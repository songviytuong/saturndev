/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$().ready(function(){
    setTimeout(function(){
        
    },100);
});

$('.saveSettingLayout').click(function(){
    $(this).addClass("disabled");
    $('.showMessages').html("<i class='fa fa-spin fa-cog'></i>")
    $.ajax({
        url: "/cpanel/stores/saveLayout",
        dataType: "JSON",
        type: "POST",
        data: $('#frmTemplate').serialize(),
        context: $(this),
        success: function(e){
//            console.log(e);
            setTimeout(function(){
                $('.saveSettingLayout').removeClass("disabled");
                if(e.status == "OK"){
                    $('.showMessages').addClass("text-success");
                    $('.showMessages').html("<i class='fa fa-check-circle'></i> " + e.message + " !");
                }else{
                    
                    $('.showMessages').addClass("text-danger");
                    $('.showMessages').html("<i class='fa fa-minus-circle'></i> " + e.message + " !");
                }
            },500);
        }
    });
});

$('.saveSettingStore').click(function(){
    $(this).addClass("disabled");
    $('.showMessages_store').html("<i class='fa fa-spin fa-cog'></i>")
    $.ajax({
        url: "/cpanel/stores/saveSettingStore",
        dataType: "JSON",
        type: "POST",
        data: $('#frmUpdateStore').serialize(),
        context: $(this),
        success: function(e){
            setTimeout(function(){
                $('.saveSettingStore').removeClass("disabled");
                if(e.status == "OK"){
                    $('.showMessages_store').addClass("text-success");
                    $('.showMessages_store').html("<i class='fa fa-check-circle'></i> " + e.message + " !");
                }else{
                    
                    $('.showMessages_store').addClass("text-danger");
                    $('.showMessages_store').html("<i class='fa fa-minus-circle'></i> " + e.message + " !");
                }
            },500);
        }
    });
});