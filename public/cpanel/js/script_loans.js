/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$().ready(function(){
    setTimeout(function(){
        loadCustomerList();
        loadMemberList();
        loadMoneyList();
        loadReturnMoneyList();
        loadTimer();
        $('#verify_by').attr("disabled",true);
        $('.addReturnAction').attr("disabled",true);
    },100);
});

$('.addMoreTimer').click(function(){
    var answer = prompt("Nhập vào thời lượng ngày?", "10");
    if(answer != null) {
        $.ajax({
            url: "/cpanel/loans/addMoreTimer",
            dataType: "HTML",
            type: "POST",
            data: "value="+answer,
            context: $(this),
            success: function(result){
                if(result == "OK"){
                   loadTimer();
                }
            }
        });
    }
});

function loadTimer(){
    $.ajax({
        url: "/cpanel/loans/loadTimer",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('#after_date').html(result);
            });
        }
    });
}

function loadCustomerList(){
    $.ajax({
        url: "/cpanel/loans/loadCustomerList",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadCustomerList').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}

function loadMemberList(){
    $.ajax({
        url: "/cpanel/loans/loadMemberList",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadMemberList').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}

function loadMoneyList(){
    $.ajax({
        url: "/cpanel/loans/loadMoneyList",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadMoneyList').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}

function loadReturnMoneyList(){
    $.ajax({
        url: "/cpanel/loans/loadReturnMoneyList",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadReturnMoneyList').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}

$('.widget-control-refresh').click(function(){
    loadCustomerList();
});

$('.btnToday').click(function(){
    $.ajax({
        url: "/cpanel/loans/setToday",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            $('.widget-control-refresh').click();
        }
    });
});

function loadLoansDetails(cID,token){
    $.ajax({
        url: "/cpanel/loans/loadLoansDetails",
        dataType: "html",
        type: "POST",
        data: "cID="+cID+"&token="+token,
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadLoansDetails').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}

$('.addLoansDetails').click(function(){
    $('.rowLoansDetails').removeClass("hidden");
});

$('.addCustomerInfo').click(function(){
    var fullname = $('input[name=fullname]').val();
    var address = $('input[name=address]').val();
    var phone = $('input[name=phone]').val();
    if(fullname == ""){
        $('input[name=fullname]').focus();
        return false;
    }else if(address == ""){
        $('input[name=address]').focus();
        return false;
    }else if(phone == ""){
        $('input[name=phone]').focus();
        return false;
    }else{
        $(this).addClass("disabled",true);
        $.ajax({
            url: "/cpanel/loans/addCustomerInfo",
            dataType: "JSON",
            type: "POST",
            data: $('#frmCustomerInfo').serialize(),
            context: $(this),
            success: function(result){
                if(result.status == "OK"){
                    $(this).addClass("disabled",true);
                    location.reload(true);
                }else{
                    console.log(result);
                }
            }
        });
    }
});

$('.saveLoansDetails').click(function(){
    var title = $('input[name=title]').val();
    if(title == ""){
        $('input[name=title]').focus();
        //$('input[name=title]').parent().closest('div').addClass("iconed-input");
        //$('input[name=title]').parent().closest('div').parent().closest('div').addClass("has-iconed has-error");
        return false;
    }
    var notes = $('input[name=notes]').val();
    if(notes == ""){
        $('input[name=notes]').focus();
        return false;
    }
    var verify_notes = $('input[name=verify_notes]').val();
    if(verify_notes == ""){
        $('input[name=verify_notes]').focus();
        return false;
    }
    var loan_money = $('input[name=loan_money]').val();
    if(loan_money == ""){
        $('input[name=loan_money]').focus();
        return false;
    }
    var fee_money = $('input[name=fee_money]').val();
    if(fee_money == ""){
        $('input[name=fee_money]').focus();
        return false;
    }
    var branchID = $('select[name=branchID]').val();
    if(branchID == -1){
        alert('Vui lòng chọn nguồn tiền từ CH !!! :(');
        $('select[name=branchID]').parent().closest('div').addClass("iconed-input");
        $('select[name=branchID]').parent().closest('div').parent().closest('div').addClass("has-iconed has-error");
        return false;
    }else{
        $('select[name=branchID]').parent().closest('div').parent().closest('div').addClass("has-iconed has-success");
    }
    $(this).addClass("disabled");
    $('.showMessages_saveLoansDetails').html("<i class='fa fa-spin fa-cog'></i>");
    $.ajax({
        url: "/cpanel/loans/saveLoansDetails",
        dataType: "JSON",
        type: "POST",
        data: $('#frmLoansDetails').serialize(),
        context: $(this),
        success: function(e){
            setTimeout(function(){
                $('.saveLoansDetails').removeClass("disabled");
                if(e.status == "OK"){
                    $('.showMessages_saveLoansDetails').addClass("text-success");
                    $('.showMessages_saveLoansDetails').html("<i class='fa fa-check-circle'></i> " + e.message + " !");
                    $("input[type=text]").val("");
                }else{
                    $('.showMessages_saveLoansDetails').addClass("text-danger");
                    $('.showMessages_saveLoansDetails').html("<i class='fa fa-minus-circle'></i> " + e.message + " !");
                }
            },1000);
        }
    });
});

$('select[name=branchID]').change(function(){
    var id = $(this).val();
    $.ajax({
        url: "/cpanel/loans/verifyMoney",
        dataType: "JSON",
        type: "POST",
        data: "id="+id,
        context: $(this),
        success: function(result){
            if(result.status == "OK" && id != -1){
                $('.loans h3').html("<i class='fa fa-money'></i>Còn: "+format(result.total)+ " -/- "+result.branchname);
                $('.saveLoansDetails').removeClass("disabled");
            }else{
                $('.loans h3').html("<i class='fa fa-money'></i>");
                $('.saveLoansDetails').addClass("disabled");
            }
        }
    });
    
});

var format = function (num) {
    var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
    if (str.indexOf(".") > 0) {
        parts = str.split(".");
        str = parts[0];
    }
    str = str.split("").reverse();
    for (var j = 0, len = str.length; j < len; j++) {
        if (str[j] != ",") {
            output.push(str[j]);
            if (i % 3 == 0 && j < (len - 1)) {
                output.push(",");
            }
            i++;
        }
    }
    formatted = output.reverse().join("");
    return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
};

$("#loan_money").keyup(function(e){
    var val = $(this).val();
    val = val.replace(/[^0-9]/g,'');
    $(this).val(format(val));
    $(this).attr("data-value",val);
});

$("#fee_money").keyup(function(e){
    var val = $(this).val();
    val = val.replace(/[^0-9]/g,'');
    $(this).val(format(val));
    $(this).attr("data-value",val);
});

$("#add_money").keyup(function(e){
    var val = $(this).val();
    val = val.replace(/[^0-9]/g,'');
    $(this).val(format(val));
});

$("#return_money").keyup(function(e){
    var val = $(this).val();
    if(val == ""){
        $('#verify_by').attr("disabled",true);
        $('.addReturnAction').attr("disabled",true);
    }else{
        val = val.replace(/[^0-9]/g,'');
        $(this).val(format(val));
        $(this).attr("data-money",val);
        $('#verify_by').attr("disabled",false);
        $('.addReturnAction').attr("disabled",false);
    }
});

$("#fee_money").blur(function(){
    calc();
});

$("#after_date").change(function(){
    calc();
});


function calc(){
    var loan_money = parseFloat($("#loan_money").attr("data-value"));
    var fee_money = parseFloat($("#fee_money").attr("data-value"));
    var luong = parseInt($("#after_date").val());
    var return_money = fee_money * luong + loan_money;
    if($("#loan_money").val() == ""){
        $("#loan_money").focus();
    }else{
        $("#need_money").val(format(return_money / luong));
        $("#return_money").val(format(return_money));
    }
}

$('.newCustomerAction').click(function(){
    $.ajax({
        url: "/cpanel/loans/resetSessionOldCustomer",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            if(result == "OK"){
                location.reload(true);
            }
        }
    });
});

$('.uploadCMND').click(function(){
    var src = $(this).attr("data-src");
    $('#modalUploadCMND').modal({show: 'true'});
    $('#modalUploadCMND .addOn #cID').val($(this).attr("data-id"));
    if(src != ""){
        $('#modalUploadCMND .dvPreview').html("<img src='public/loans/cmnd/" + src + "' width='100%'/>");
    }
});

$('.doUploadCMND').click(function(e){
    $('#frmU').submit();
});

$("#choosefile").change(function(){
    var Fileinput = document.getElementById("choosefile");
    var file = Fileinput.files[0];
    var imageType = /image.*/
    var dvPreview = $(".dvPreview");
    if(file.type.match(imageType)){
        var reader = new FileReader();
        reader.onload = function (e) {
            var img = $("<img />");
            img.attr("style", "max-height:100%;max-width: 100%");
            img.attr("src", e.target.result);
            dvPreview.html(img);
            $('.viewCMND').removeClass("hidden");
            $('.fa-camera').addClass("text-primary");
            $('#cmnd_src').val(file.name);
            console.log(file);
        }
        reader.readAsDataURL(file);
    }else{
        console.log("Not an Image");
    }
});
/*
 * Cần hỏi lại trước khi Insert
 * Hiển thị số tiền cần phải đóng
 */
$('.addReturnAction').click(function(){
    $.ajax({
        url: "/cpanel/loans/addReturn",
        dataType: "JSON",
        type: "POST",
        data: $('#frmAddReturn').serialize(),
        context: $(this),
        success: function(result){
            if(result.status == "OK"){
                $('.closeAddReturn').click();
                loadLoansDetails(result.cID,result.token);
            }else{
                console.log(result);
            }
        }
    });
});
function addMoney(){
    $('.addMoneyHeader h3').html("<i class='fa fa-plus-circle'></i>Xử lý tiền");
    $('#modalAddMoney').modal({show: 'true'});
}

function editMoney(ob){
    var id = $(ob).attr("data-id");
    var token = $(ob).attr("data-token");
    var link = "cpanel/loans/loadEditMoney/?id="+id+"&token="+token;
    $('.editMoneyHeader h3').html("<i class='fa fa-edit'></i>Sửa");
    $('#modalEditMoney').modal({show: 'true'});
    $('.editMoneyContent').load(link);
}

$('.addMoneyAction').click(function(){
    var add_money = $("input[name=add_money]").val();
    var add_source = $("input[name=add_source]").val();
    if(add_money == ""){
        $("input[name=add_money]").focus();
        return false;
    }
    if(add_source == ""){
        $("input[name=add_source]").focus();
        return false;
    }
    $(this).addClass("disabled");
    $('.showMessages_saveNewMoney').html("<i class='fa fa-spin fa-cog'></i>");
    $.ajax({
        url: "/cpanel/loans/addNewMoney",
        dataType: "JSON",
        type: "POST",
        data: $('#frmAddMoney').serialize(),
        context: $(this),
        success: function(e){
            setTimeout(function(){
                $('.addMoneyAction').removeClass("disabled");
                if(e.status == "OK"){
                    $('.showMessages_saveNewMoney').addClass("text-success");
                    $('.showMessages_saveNewMoney').html("<i class='fa fa-check-circle'></i> " + e.message + " !");
                }else{
                    $('.showMessages_saveNewMoney').addClass("text-danger");
                    $('.showMessages_saveNewMoney').html("<i class='fa fa-minus-circle'></i> " + e.error + " !");
                }
                $("input[type=text]").val("");
            },100);
            loadMoneyList();
        }
    });
});

function addMember(){
    $('.addMemberHeader h3').html("<i class='fa fa-plus-circle'></i> Thêm mới thành viên");
    $('#modalAddMember').modal({show: 'true'});
}



$('.addMemberAction').click(function(){
    var fullname = $("input[name=fullname]").val();
    var phone = $("input[name=phone]").val();
    if(fullname == ""){
        $("input[name=fullname]").focus();
        return false;
    }
    if(phone == ""){
        $("input[name=phone]").focus();
        return false;
    }
    $(this).addClass("disabled");
    $('.showMessages_saveNewMember').html("<i class='fa fa-spin fa-cog'></i>");
    $.ajax({
        url: "/cpanel/loans/addNewMember",
        dataType: "JSON",
        type: "POST",
        data: $('#frmAddMember').serialize(),
        context: $(this),
        success: function(e){
            setTimeout(function(){
                $('.addMemberAction').removeClass("disabled");
                if(e.status == "OK"){
                    $('.showMessages_saveNewMember').addClass("text-success");
                    $('.showMessages_saveNewMember').html("<i class='fa fa-check-circle'></i> " + e.message + " !");
                }else{
                    $('.showMessages_saveNewMember').addClass("text-danger");
                    $('.showMessages_saveNewMember').html("<i class='fa fa-minus-circle'></i> " + e.error + " !");
                }
                $("input[type=text], textarea").val("");
            },1000);
            loadMemberList();
        }
    });
});
