/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$().ready(function(){
    setTimeout(function(){
        loadTours();
    },100);
});

/*START TRAVEL*/
function loadTours(){
    $.ajax({
        url: "/cpanel/travel/loadToursAjax",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadTours').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}


var rowCount = 1;
function addMoreRows(frm) {
    console.log(frm);
    rowCount ++;
    var recRow = '<tr id="rowCount'+rowCount+'"><td><input name="name[]" type="text" size="17%" maxlength="120" /></td><td><input name="phone[]" type="text" size="17%" maxlength="120" /></td><td><select name="type[]"><option value="1">Type 1</option><option value="2">Type 2</option><option value="3">Type 3</option></select>\n\
</td><td> <a href="javascript:void(0);" onclick="removeRow('+rowCount+');">Delete</a></td></tr>';
    var divRow = '<div class="row">\n\
<div class="container">\n\
<div class="col-md-12">\n\
<div class="form-group">\n\
<label><i class="fa fa-times-circle"></i> Description:</label>\n\
<textarea class="summernote" name="tourDay[]" rows="6"></textarea>\n\
</div>\n\
</div>\n\
</div>\n\
</div>';
    jQuery('.divTour .row:last').after(divRow);
    $.getScript("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
}

function removeRow(removeNum) {
    jQuery('#rowCount'+removeNum).remove();
}

function saveNames(ob){
    var baselink = $("#baselink").val();
    $.ajax({
        url: baselink+"addNames",
        dataType: "html",
        type: "POST",
        data: $("form").serialize(),
        success: function(result){
            window.location = baselink + "addMoreRow" + '/'+result;
        }
    });
}

function updateNames(ob){
    var baselink = $("#baselink").val();
    $.ajax({
        url: baselink+"updateNames",
        dataType: "html",
        type: "POST",
        data: $("#frmTour").serialize(),
        success: function(result){
            if(result == "OK"){
                $('.fa-refresh').click();
            }else{
                console.log(result);
            }
        }
    });
}
/*END TRAVEL*/