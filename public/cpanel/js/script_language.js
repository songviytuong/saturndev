/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$().ready(function(){
    setTimeout(function(){
        loadLanguages();
        loadLanguagesList();
    },100);
});

function loadLanguages(){
    $.ajax({
        url: "/cpanel/languages/loadLanguages",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadLanguages').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}

function loadLanguagesList(){
    $.ajax({
        url: "/cpanel/languages/loadLanguagesList",
        dataType: "html",
        type: "POST",
        data: "",
        context: $(this),
        success: function(result){
            setTimeout(function(){
                $('.loadLanguagesList').html(result);
                $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
            });
        }
    });
}

$('.widget-control-refresh').click(function(){
    loadLanguages();
});

$('.txtLanguage').blur(function(){
    if($(this).val() == ''){
        $('.lblLanguage').removeClass("move-top-label-active");
    }else{
        $('.lblLanguage').addClass("move-top-label-active");
    }
});

$('.txtKeyword').blur(function(){
    if($(this).val() == ''){
        $('.lblKeyword').removeClass("move-top-label-active");
    }else{
        $('.lblKeyword').addClass("move-top-label-active");
    }
});

function insertLanguages(){
    if ($("#validateForm").valid()) {
        $.ajax({
            url: "/cpanel/languages/insertLanguage",
            dataType: "html",
            type: "POST",
            data: $('#validateForm').serialize(),
            context: $(this),
            success: function (result) {
                if (result == "OK") {
                    $('.txtLanguage').val("");
                    $('#txtKeyword').val("");
                    loadLanguages();
                } else {
                    alert(result);
                    $('#txtKeyword').focus();
                    $('#txtKeyword').select();
                }
            }
        });
    }
}

function showModal(ob){
    var keyword = $(ob).attr("rel");
    $(".modal-body #txtKeyword").val(keyword);
    $(".modal-body .lblKeyword").addClass("move-top-label-active");
    $('#modalForm').modal({
        show: 'true'
    });
}

function updateRow(ob){
    var id = $(ob).attr("rel");
    var translate = $(ob).val();
    $.ajax({
        url: "/cpanel/languages/updateLanguage",
        dataType: "html",
        type: "POST",
        data: "translate="+translate+"&id="+id,
        context: $(this),
        success: function(result){
            if(result == "OK"){
                $('.td_'+id).addClass("iconed-input");
                setTimeout(function(){
                    $('.td_'+id).removeClass("iconed-input");
                },2000);
            }
        }
    });
//    console.log(id,translate);
}