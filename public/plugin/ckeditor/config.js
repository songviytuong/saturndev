/**
* @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
* For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
    var link = "http://tools.dev/trantoanphat";
    config.filebrowserBrowseUrl = link+'/public/plugin/ckeditor/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = link+'/public/plugin/ckeditor/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = link+'/public/plugin/ckeditor/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl = link+'/public/plugin/ckeditor/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = link+'/public/plugin/ckeditor/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = link+'/public/plugin/ckeditor/kcfinder/upload.php?type=flash';
    config.height = '500';
    /*config.toolbar = [
    	{name : 'clipboard', items: ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','Bold','Italic','Font','Iframe','Heading']},
    	{name : 'editing', items: ['Scayt']},
    	{name : 'links', items: ['Link','Unlink','Anchor']},
    	{name : 'insert', items: ['Image','Table','HorizontalRule','SpecialChar']},
    	{name : 'tools', items: ['Maximize']},
    	{name : 'document', items: ['Source']}
    ];*/
};
