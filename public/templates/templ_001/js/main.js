$(document).ready(function() {
 
  var owl = $("#owl-demo");
 
  owl.owlCarousel({
     
      itemsCustom : [
        [0, 1],
        [500, 2],
        [750, 3],
        [992,1],
        [1600, 1]
      ],
    lazyLoad : true,
	autoPlay : true,
	stopOnHover : true,
    navigation : false
 
  });
$('#style-switcher').styleSwitcher(); 
});

$('input[name=Subscribe]').keyup(function(e) {
    if (e.keyCode == 13) {
        ajaxSubScribe();
    }
});

function ajaxSubScribe() {
    var email = $('input[name=Subscribe]').val();
    if (email == "") {
        $('input[name=Subscribe]').focus();
        $('input[name=Subscribe]').attr("placeholder", "Enter your email");
        $("#result").text(email + " is not empty :(");
        $("#result").css("color", "red");
    } else {
        $('.fa-icon').removeClass("fa-paper-plane-o").addClass("fa-spin fa-refresh");
        if (validateEmail(email)) {
            $("#result").text(email + " is valid :)");
            $("#result").css("color", "green");
            /*Ajax*/
            $.ajax({
                url: "ajaxNewsLetter",
                dataType: "html",
                type: "POST",
                data: "email=" + email,
                success: function(result){
                    if(result == "OK"){
                        $(".Subscribe-btn").css({
                            backgroundColor: 'green'
                        });
                        $("#result").text("Đăng ký thành công!");
                        $("input[name=Subscribe]").val("");
                        $('.fa-icon').removeClass("fa-spin fa-refresh").addClass("fa-check");
                        setTimeout(function(){
                            $('.fa-icon').removeClass("fa-check").addClass("fa-paper-plane-o");
                            $(".Subscribe-btn").css({
                                backgroundColor: 'red'
                            });
                        },1000);
                    }
                }
            });
        } else {
            $("#result").text(email + " is not valid :(");
            $("#result").css("color", "red");
            setTimeout(function(){
                $('.fa-icon').removeClass("fa-spin fa-refresh fa-check").addClass("fa-paper-plane-o");
            },500);
        }
        return false;
    }
    $("#result").css({
        fontSize: '12px'
    });
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(email);
}




