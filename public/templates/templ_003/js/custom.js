// JavaScript Document
$ = jQuery.noConflict();

var stGlobals = {},
    loaded    = 0;
stGlobals.isMobile       = (/(Android|BlackBerry|iPhone|iPod|iPad|Palm|Symbian)/.test(navigator.userAgent));
stGlobals.isMobileWebkit = /WebKit/.test(navigator.userAgent) && /Mobile/.test(navigator.userAgent);
stGlobals.isIOS          = (/iphone|ipad|ipod/gi).test(navigator.appVersion);

/* jQuery set same height plugin */
$.fn.setSameHeight=function(){
    var max_height = 0;
    $(this).height('auto');
    $(this).each(function(index){
        if ($(this).outerHeight() > max_height) { max_height = $(this).outerHeight(); }
    });
    //if( $(this).hasClass("description") )
    //    max_height = max_height - 18;
    $(this).css('min-height', max_height);
    return this;
}
function setSameHeight_listing_style1() {
    if ($('.listing-style1').length > 0) {
        $('.listing-style1').each(function(index){
            $(this).find('article.box .details .description').setSameHeight();
            $(this).find('article.box .details .box-title').setSameHeight();
            //$(this).find('article.box').setSameHeight();
        });
    }
}

function changeTraveloElementUI() {
    // change UI of select box
    $(".selector select").each(function() {
        var obj = $(this);
        if (obj.parent().children(".custom-select").length < 1) {
            obj.after("<span class='custom-select'>" + obj.children("option:selected").html() + "</span>");

            if (obj.hasClass("white-bg")) {
                obj.next("span.custom-select").addClass("white-bg");
            }
            if (obj.hasClass("full-width")) {
                //obj.removeClass("full-width");
                //obj.css("width", obj.parent().width() + "px");
                //obj.next("span.custom-select").css("width", obj.parent().width() + "px");
                obj.next("span.custom-select").addClass("full-width");
            }
        }
    });
    $("body").on("change", ".selector select", function() {
        if ($(this).next("span.custom-select").length > 0) {
            $(this).next("span.custom-select").text($(this).children("option:selected").text());
        }
    });

    $("body").on("keydown", ".selector select", function() {
        if ($(this).next("span.custom-select").length > 0) {
            $(this).next("span.custom-select").text($(this).children("option:selected").text());
        }
    });

    // change UI of file input
    $(".fileinput input[type=file]").each(function() {
        var obj = $(this);
        if (obj.parent().children(".custom-fileinput").length < 1) {
            obj.after('<input type="text" class="custom-fileinput" />');
            if (typeof obj.data("placeholder") != "undefined") {
                obj.next(".custom-fileinput").attr("placeholder", obj.data("placeholder"));
            }
            if (typeof obj.prop("class") != "undefined") {
                obj.next(".custom-fileinput").addClass(obj.prop("class"));
            }
            obj.parent().css("line-height", obj.outerHeight() + "px");
        }
    });

    $(".fileinput input[type=file]").on("change", function() {
        var fileName = this.value;
        var slashIndex = fileName.lastIndexOf("\\");
        if (slashIndex == -1) {
            slashIndex = fileName.lastIndexOf("/");
        }
        if (slashIndex != -1) {
            fileName = fileName.substring(slashIndex + 1);
        }
        $(this).next(".custom-fileinput").val(fileName);
    });
    // checkbox
    $(".checkbox input[type='checkbox'], .radio input[type='radio']").each(function() {
        if ($(this).is(":checked")) {
            $(this).closest(".checkbox").addClass("checked");
            $(this).closest(".radio").addClass("checked");
        }
    });
    $(".checkbox input[type='checkbox']").bind("change", function() {
        if ($(this).is(":checked")) {
            $(this).closest(".checkbox").addClass("checked");
        } else {
            $(this).closest(".checkbox").removeClass("checked");
        }
    });
    //radio
    $(".radio input[type='radio']").bind("change", function(event, ui) {
        if ($(this).is(":checked")) {
            var name = $(this).prop("name");
            if (typeof name != "undefined") {
                $(".radio input[name='" + name + "']").closest('.radio').removeClass("checked");
            }
            $(this).closest(".radio").addClass("checked");
        }
    });

    // datepicker
    $('.datepicker-wrap input').each(function() {
        var minDate = $(this).data("min-date");
        var maxDate = $(this).data("max-date");
        if (typeof minDate == "undefined") {
            minDate = 0;
        }
        if (typeof maxDate == "undefined") {
            maxDate = null;
        }
        if ($(this).closest('.datepicker-wrap').hasClass('to-today')) { minDate = null; maxDate = 0; }

        $(this).datepicker({
            showOn: 'button',
            buttonImage: themeurl + '/images/blank.png',
            buttonText: '',
            buttonImageOnly: true,
            changeYear: false,
            minDate: minDate,
            maxDate: maxDate,
            dateFormat: 'dd/mm/yy',
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            beforeShow: function(input, inst) {
                var themeClass = $(input).parent().attr("class").replace("datepicker-wrap", "");
                $('#ui-datepicker-div').attr("class", "");
                $('#ui-datepicker-div').addClass("ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all");
                $('#ui-datepicker-div').addClass(themeClass);
            },
            onClose: function(selectedDate) {
                if ( $(this).attr('name') == 'date_from' ) {
                    if ( $(this).closest('form').find('input[name="date_to"]').length > 0 ) {
                        $(this).closest('form').find('input[name="date_to"]').datepicker("option", "minDate", selectedDate);
                    }
                }
                if ( $(this).attr('name') == 'date_to' ) {
                    if ( $(this).closest('form').find('input[name="date_from"]').length > 0 ) {
                        $(this).closest('form').find('input[name="date_from"]').datepicker("option", "maxDate", selectedDate);
                    }
                }
            }
        });
    });

    // placeholder for ie8, 9
    try {
        $('input, textarea').placeholder();
    } catch (e) {}
}

/* display photo gallery */
function displayPhotoGallery($item) {
    if (! $.fn.flexslider || $item.length < 1 || $item.is(":hidden")) {
        return;
    }
    var dataAnimation = $item.data("animation");
    var dataSync = $item.data("sync");
    if (typeof dataAnimation == "undefined") {
        dataAnimation = "slide";
    }
    var dataFixPos = $item.data("fix-control-nav-pos");

    $item.flexslider({
        animation: dataAnimation,
        controlNav: true,
        animationLoop: true,
        slideshow: true,
        pauseOnHover: true,
        sync: dataSync,
        start: function(slider) {
            if (typeof dataFixPos != "undefined" && dataFixPos == "1") {
                var height = $(slider).find(".slides img").height();
                $(slider).find(".flex-control-nav").css("top", (height - 44) + "px");
            }
        },
    });
}

/* display image carousel */
function displayImageCarousel($item) {
    if (! $.fn.flexslider || $item.length < 1 || $item.is(":hidden")) {
        return;
    }
    var dataAnimation = $item.data("animation");
    var dataItemWidth = $item.data("item-width");
    var dataItemMargin = $item.data("item-margin");
    var dataSync = $item.data("sync");
    if (typeof dataAnimation == "undefined") {
        dataAnimation = "slide";
    }
    if (typeof dataItemWidth == "undefined") {
        dataItemWidth = 70;
    }
    if (typeof dataItemMargin == "undefined") {
        dataItemMargin = 10;
    }
    dataItemWidth = parseInt(dataItemWidth, 10);
    dataItemMargin = parseInt(dataItemMargin, 10);

    var dataAnimationLoop = true;
    var dataSlideshow = false;
    if (typeof dataSync == "undefined") {
        dataSync = "";
        //dataAnimationLoop = true;
        dataSlideshow = true;
    }

    $item.flexslider({
        animation: dataAnimation,
        controlNav: true,
        animationLoop: dataAnimationLoop,
        slideshow: dataSlideshow,
        itemWidth: dataItemWidth,
        itemMargin: dataItemMargin,
        minItems: 1,
        maxItems: 4,
        pauseOnHover: true,
        asNavFor: dataSync,
        start: function(slider) {
            if (dataSync == "") {
                $(slider).find(".middle-block img, .middle-block .middle-item").each(function() {
                    if($(this).width() < 1) {
                        $(this).load(function() {
                            $(this).closest(".middle-block").middleblock();
                        });
                    } else {
                        $(this).closest(".middle-block").middleblock();
                    }
                });
            }
            /*if ($('.listing-style2').length > 0) {
                $('.listing-style2').each(function(index){
                    $(this).find('article.box .details .description').setSameHeight();
                    $(this).find('article.box .details .box-title').setSameHeight();
                    $(this).find('article.box').setSameHeight();
                });
            }*/
        },
        after: function(slider) {
            if (slider.currentItem == 0) {
                var target = 0;
                if (slider.transitions) {
                    target = (slider.vars.direction === "vertical") ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
                    slider.container.css("-" + slider.pfx + "-transition-duration", "0s");
                    slider.container.css("transition-duration", "0s");
                }
                slider.args[slider.prop] = target;
                slider.container.css(slider.args);
                slider.container.css('transform',target);
            }
        }

    });
}

var loaded = 0;
function tnktravel_ready() {
    /* change UI of input, select, option, checkbox, datepicker */
    changeTraveloElementUI();
    if ( stGlobals.isMobile ) {
        $("body").addClass("is-mobile");
    }
    // parallax for wekbit mobile
    if (stGlobals.isMobileWebkit) {
        $(".parallax").css("background-attachment", "scroll");
    }

    // tooltip
    $("[data-toggle=tooltip]").tooltip();
    $('.unsubscribe-tooltip').popover({
        trigger: 'click hover',
        placement: 'bottom',
        delay: {show: 50, hide: 400},
        html: true
    });

    // price range
    $(".range-slide").each(function() {
        var target = $(this),
        this_parent = target.parent(),
        range_slide_min_val = 0,
        range_slide_step = $(this).data('slide-step'),
        range_slide_last_val = $(this).data('slide-last-val'),
        range_slide_max_val = range_slide_last_val + range_slide_step,
        range_slide_def = $(this).data('slide-def'),
        position_def = $(this).data('slide-def-position'),
        min_slide = $(this).data('min-slide'),
        max_slide = $(this).data('max-slide');

        //console.log(target.parent().attr('id'));
        if (max_slide == "no_max") { max_slide = range_slide_max_val; }
        $(this).slider({
            range: true,
            min: range_slide_min_val,
            max: range_slide_max_val,
            step: range_slide_step,
            values: [ min_slide, max_slide ],
            slide: function(event, ui) {
                // make handles uncollapse
                if ((ui.values[0] + 1) >= ui.values[1]) {
                    return false;
                }

                // min price text
                this_parent.find(".min-range-label").text(position_def == 'before' ? range_slide_def + ui.values[0] : ui.values[0] + range_slide_def);

                // max price text
                max_slide = ui.values[1];
                if (max_slide == range_slide_max_val) {
                    max_slide = range_slide_last_val + '+';
                }
                this_parent.find(".max-range-label").text(position_def == 'before' ? range_slide_def + max_slide : max_slide + range_slide_def);
            },
            change: function(event, ui) {
                this_parent.find("input[name*='mi']").val(ui.values[0])
                this_parent.find("input[name*='ma']").val(ui.values[1])
            }
        });

        this_parent.find(".min-range-label").text(position_def == 'before' ? range_slide_def + $(this).slider("values", 0) : $(this).slider("values", 0) + range_slide_def);
        if ($(this).slider("values", 1) == range_slide_max_val) {
            this_parent.find(".max-range-label").text((position_def == 'before' ? range_slide_def + range_slide_last_val : range_slide_last_val + range_slide_def) + "+");
        } else {
            this_parent.find(".max-range-label").text(position_def == 'before' ? range_slide_def + $(this).slider("values", 1) : $(this).slider("values", 1) + range_slide_def);
        }
    });

    // rating
    if ( $("#rating").length ) {
        //var url_norating = $("#rating").data('url-norating').replace(/&amp;/g, '&');
        var rating = $("#rating").data('rating');
        $("#rating").slider({
            range: "min",
            value: rating,
            min: 0,
            max: 5,
            step: 1,
            slide: function(event, ui) {
                var label_rating = '';
                if (ui.value == 0) {
                    label_rating = $("#rating").data('label-norating');
                } else if (ui.value == 5) {
                    label_rating = $("#rating").data('label-fullrating');
                } else {
                    label_rating = ui.value + ' ' + $("#rating").data('label-rating');
                }
                $("#rating-filter .panel-content > span").text(label_rating);
            },
            change: function(event, ui) {
                $("#rating-filter .panel-content > input[name='stars']").val(ui.value);
                /*if (ui.value != 0) {
                    url_norating += '&rating=' + ui.value;
                }
                if (url_norating.indexOf("?") < 0) { url_norating = url_norating.replace(/&/, '?'); }*/
                //window.location.href = url_norating;
            }
        });

        var label_rating = '';
        if (rating == 0) {
            label_rating = $("#rating").data('label-norating');
        } else if (rating == 5) {
            label_rating = $("#rating").data('label-fullrating');
        } else {
            label_rating = rating + ' ' + $("#rating").data('label-rating');
        }
        $("#rating-filter .panel-content > span").text(label_rating);
    }

    // fit video
    if ($('.full-video').length > 0 && $.fn.fitVids) {
        $('.full-video').fitVids();
    }

    //waypoint
    if($().waypoint) {
        // animation effect
        $('.animated').waypoint(function() {
            var type = $(this).data("animation-type");
            if (typeof type == "undefined" || type == false) {
                type = "fadeIn";
            }
            $(this).addClass(type);

            var duration = $(this).data("animation-duration");
            if (typeof duration == "undefined" || duration == false) {
                duration = "1";
            }
            $(this).css("animation-duration", duration + "s");

            var delay = $(this).data("animation-delay");
            if (typeof delay != "undefined" && delay != false) {
                $(this).css("animation-delay", delay + "s");
            }

            $(this).css("visibility", "visible");

            setTimeout(function() { $.waypoints('refresh'); }, 1000);
        }, {
            triggerOnce: true,
            offset: 'bottom-in-view'
        });
    }

    // Mobile search
    if ($('#mobile-search-tabs').length > 0) {
        var mobile_search_tabs_slider = $('#mobile-search-tabs').bxSlider({
            mode: 'fade',
            infiniteLoop: false,
            hideControlOnEnd: true,
            touchEnabled: true,
            pager: false,
            onSlideAfter: function($slideElement, oldIndex, newIndex) {
                $('a[href="' + $($slideElement).children("a").attr("href") + '"]').tab('show');
            }
        });
    }

    // search box
    $("#frm_tour select[name='cat']").change(function() {
        $("#frm_tour").addClass('loading');
        var catID = $(this).val();
        $.ajax({
            type: "POST",
            url: themeurl + '/actions.php',
            dataType: 'json',
            data: 'url=loadsubT,'+ catID,
            cache: false,
            success: function(data){
                $("#frm_tour").attr("action", data.formURL);
                $("#frm_tour select[name='tourtype']").html(data.slsub).next("span.custom-select").text($("#frm_tour select[name='tourtype']").children("option:selected").text());
                setTimeout(function(){
                    $("#frm_tour").removeClass('loading');
                }, 300);
            }
        });
    });
    $("#frm_tour").delegate("select[name='tourtype']", "change", function() {
        var catID = parseInt($(this).val());
        if( catID ) {
            $("#frm_tour").addClass('loading');
            $.ajax({
                type: "POST",
                url: themeurl + '/actions.php',
                data: 'url=loadlinksubT,'+ catID,
                cache: false,
                success: function(data){
                    $("#frm_tour").attr("action", data);
                    setTimeout(function(){
                        $("#frm_tour").removeClass('loading');
                    }, 300);
                }
            });
        }
    });
    /*$('#tour-search').typeahead({
        minLength: 2,
        order: "asc",
        offset: false,
        hint: true,
        cache: true,
        ttl: 86400000, // 1day
        compression: true,
        maxItem: 10,
        dynamic: true, //
        //delay: 500, // delay to query database
        //emptyTemplate: 'No result for "{{query}}"',
        backdrop: {
            "opacity": 0.45,
            "filter": "alpha(opacity=45)",
            "background-color": "#fff"
        },
        source: {
            tour: {
                url: {
                    type: "POST",
                    url: themeurl + '/actions.php?catID='+ parseInt($("#frm_tour select[name='cat']").val()) +'&<subID></subID>='+ parseInt($("#frm_tour select[name='tourtype'] option:selected").val()),
                    data: {
                        url     : 'loadTourDetail',
                        q       : "{{query}}",
                        //catID   : $("#frm_tour select[name='cat']").val(),
                        //subID   : $("#frm_tour select[name='tourtype'] option:selected").val()
                    },
                    //callback: {
                    //    done: function (data) {
                    //        alert( $("#frm_tour select[name='cat']").val() );
                    //    }
                    //}
                }
            }
        },
        callback: {
            onClick: function (node, a, item, event) {
                alert(JSON.stringify(item));
            },
            onSubmit: function (node, form, item, event) {
                alert(JSON.stringify(item));
            }
        },
        //debug: true
    });*/
    $("#frm_hotel select[name='des']").change(function() {
        $("#frm_hotel").addClass('loading');
        var catID = $(this).val();
        $.ajax({
            type: "POST",
            url: themeurl + '/actions.php',
            dataType: 'json',
            data: 'url=loadsubH,'+ catID,
            cache: false,
            success: function(data){
                $("#frm_hotel").attr("action", data.formURL);
                $("#frm_hotel select[name='sub']").html(data.slsub).next("span.custom-select").text($("#frm_hotel select[name='sub']").children("option:selected").text());
                setTimeout(function(){
                    $("#frm_hotel").removeClass('loading');
                }, 300);
            }
        });
    });
    $("#frm_hotel select[name='sub']").change(function() {
        var subID = $(this).val();
        if( !parseInt(subID) )
            subID = $("#frm_hotel select[name='des']").find("option:selected").val();
        $.ajax({
            type: "POST",
            url: themeurl + '/actions.php',
            dataType: 'json',
            data: 'url=loadlinksubH,'+ subID,
            cache: false,
            success: function(data){
                $("#frm_hotel").attr("action", data.formURL);
            }
        });
    });


    $('#quickmenu').dropit({
        action: 'mouseenter',
        triggerParentEl: 'li'
    });

    $('#scrollBooking').click(function() {
        $('html,body').animate({
            scrollTop: $("#hotel-features").offset().top
        }, 'slow');
        var target_tabs = $("#hotel-features > ul.tabs > li");
        target_tabs.removeClass('active');
        target_tabs.find("a[href='#tab_price']").parent().addClass('active')
    });
}

/*var t = $(".ct-preloader"),
    a = $(".ct-preloader-content");
setTimeout(function() {
    $(t).addClass("animated").addClass("fadeOut"), $(a).addClass("animated").addClass("fadeOut")
}, 0), setTimeout(function() {
    $(t).css("display", "none").css("z-index", "-9999")
}, 500)*/

// back to top
$("body").on("click", "#back-to-top", function(e) {
    e.preventDefault();
    $("html,body").animate({scrollTop: 0}, 10000);
});

//UI TO TOP PLUGIN...
$().UItoTop();

// login box
$("body").on("click", ".soap-popupbox", function(e) {
    e.preventDefault();
    var sourceId = $(this).attr("href");
    if (typeof sourceId == "undefined") {
        sourceId = $(this).data("target");
    }
    if (typeof sourceId == "undefined") {
        return;
    }
    if ($(sourceId).length < 1) {
        return;
    }
    $(this).soapPopup({
        wrapId: "soap-popupbox",
    });
});

// login box
$("body").on("click", ".travelo-modal-box .signup-email", function(e) {
    e.preventDefault();
    $(this).closest(".travelo-modal-box").find(".simple-signup").hide();
    $(this).closest(".travelo-modal-box").find(".email-signup").show();
    $(this).closest(".travelo-modal-box").find(".email-signup").find(".input-text").eq(0).focus();
});

// go back
$(".go-back").click(function(e) {
    e.preventDefault();
    window.history.go(-1);
});

// alert, info box
$("body").on("click", ".alert > .close, .info-box > .close", function() {
    $(this).parent().fadeOut(300);
});

$(document).ready(function() {
    tnktravel_ready()

    var selectFormGroup = function (event) {
        event.preventDefault();

        var $selectGroup = $(this).closest('.input-group-select');
        var param = $(this).attr("href").replace("#","");
        var concept = $(this).text();

        $selectGroup.find('.concept').text(concept);
        $selectGroup.find('.input-group-select-val').val(param);

    }
    $(document).on('click', '.dropdown-menu a', selectFormGroup);

    $('.required-icon').tooltip({
        placement: 'left',
        title: 'Required field'
    });
});

$(window).load(function() {
    /*$('.image-carousel').each(function() {
        displayImageCarousel($(this));
    });*/

    if($(".dt_carousel").length) {
        $(".dt_carousel").each(function(){
            var slider = $(this).lightSlider({
            //$(this).lightSlider({
                item:4,
                slideMove:1,
                auto:true,
                loop:true,
                cssEasing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                speed:600,
                pause:3000,
                slideMargin:30,
                pauseOnHover: true,
                controls:false,
                pager:false,
                responsive : [
                    {
                        breakpoint:991,
                        settings: {
                            item:3,
                            slideMove:1
                          }
                    },
                    {
                        breakpoint:640,
                        settings: {
                            item:2,
                            slideMove:1
                          }
                    },
                    {
                        breakpoint:481,
                        settings: {
                            item:1,
                            slideMove:1
                          }
                    }
                ],
                onSliderLoad: function() {
                    $(this).removeClass('cS-hidden');
                }
            });
            $('#goToPrevSlide').click(function(){
                slider.goToPrevSlide();
            });
            $('#goToNextSlide').click(function(){
                slider.goToNextSlide();
            });
            if ($('.listing-style2').length > 0) {
                $('.listing-style2').each(function(index){
                    $(this).find('article.box .details .description').setSameHeight();
                    $(this).find('article.box .details .box-title').setSameHeight();
                    $(this).find('article.box').setSameHeight();
                });
            }
        });
    }


    if( ( ! stGlobals.isMobileWebkit && $(".parallax").length > 0 ) ) {
        $.stellar({
            responsive: true,
            horizontalScrolling: false
        });
    }

    //$(window).trigger('resize');
    setTimeout(function() {
        $(window).trigger('resize');
     }, 500);
    loaded = 1;
});

$(window).resize(function() {
    // make listing same height
    setSameHeight_listing_style1();
});

// override jquery validate plugin defaults
$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$('#booking-form').validate({
    rules: {
        email: 'required',
        cemail: {
            equalTo: '#txtEmail'
        }
    }/*,
    submitHandler: function(form) { // for demo
        alert('valid form');
        return false;
    }*/
});

$(document).on('click', '.number-spinner button', function () {
    var btn = $(this),
        oldValue = btn.closest('.number-spinner').find('input').val().trim(),
        minValue = parseInt(btn.closest('.number-spinner').find('input').data('min')),
        newVal = 0;

    if (btn.attr('data-dir') == 'up') {
        newVal = parseInt(oldValue) + 1;
    } else {
        if (oldValue > 1) {
            newVal = parseInt(oldValue) - 1;
        } else {
            if( minValue )
                newVal = 1;
        }
    }
    btn.closest('.number-spinner').find('input').val(newVal);
    return false;
});

var loading = '<div style="text-align:center;"><img src="'+ themeurl +'/images/loading/loading%s.gif" alt="" /></div>';
function sprintf(format, etc) {
    var arg = arguments;
    var i = 1;
    return format.replace(/%((%)|s)/g, function (m) { return m[2] || arg[i++] })
}

function textCounter(field, countfield, maxlimit) {
    if (field.value.length > maxlimit) {
        alert("Your message exceeds " + maxlimit + " characters. Please shorten it");
        field.value = field.value.substring(0, maxlimit);
    } else {
        countfield.value = maxlimit - field.value.length;
    }
}

$('input[name="Transfer"]').click(function(event) {
    if( parseInt($(this).val()) )
        $('#isPickup').slideDown('fast');
    else $('#isPickup').slideUp('fast');
});

// Live chat
var __lc = {};
__lc.license = 4197571;

(function() {
    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
