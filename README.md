<h1>Multiple store website in Codeigniter</h1>
-----------------------------------------
23/08/2016
- Loans Module (updating...)
    + Menu Money
-----------------------------------------
19/08/2016
- Loans Module (updating...)
    + Add Return
    + Add Money Package
    + Update Database to 2.1

-----------------------------------------
18/07/2016
- Add a Permission Module

-----------------------------------------
16/07/2016
- Add CI-Smarty

-----------------------------------------
08/07/2016
- Update Layout store
- Update Template store

-----------------------------------------
05/06/2016
- Configure a Router to run Subdomain (~ your store)
+ user1.domain.com -> to your store website
+ user2.domain.com -> to your store website
+ yourdomain.com -> to your store website

### Contact
You can contact us at songviytuong@gmail.com.
