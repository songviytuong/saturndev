<!DOCTYPE html>
<html>
<head>
    <?php echo $_title."\n"; ?>
    <meta charset="utf-8">
    <base href="<?php echo base_url(). ADMINROOT ;?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="author" content="<?=CMS_VERSION_NAME;?>"/>
    <meta name="version" content="<?=CMS_VERSION;?>"/>
    <link rel="stylesheet" href="public/authen/03a03bbe34da26df16eb239ba68ecc0a.css"/>
    <link rel="stylesheet" href="public/cpanel/css/flag-icon.min.css"/>
    <?php echo $_styles."\n"; ?>
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,700|Roboto+Condensed:300,400,700' rel='stylesheet' type='text/css'>
    <link href="public/authen/assets/favicon.ico" rel="shortcut icon"/>
    <link href="public/authen/assets/apple-touch-icon.png" rel="apple-touch-icon"/>
</head>
<body class="glossed">
<div class="all-wrapper fixed-header left-menu <?=$this->session->userdata('submenu');?>">
    <div class="page-header">
        <?php echo $header; ?>
    </div>
    <div class="side">
        <?php echo $sitebar; ?>
        <?php echo $subsitebar; ?>
    </div>
    <div class="main-content">
    <?php echo $content;?>
    </div>
    <?php echo $footer; ?>
</div>
<?php
    $this->load->view('common/modal');
?>
<script src="public/authen/assets/js/jquery.min.js"></script>
<script src="public/authen/assets/js/jquery-ui.min.js"></script>
<?php echo $_scripts; ?>
</body>
</html>