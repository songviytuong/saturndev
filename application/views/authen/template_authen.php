<!DOCTYPE html>
<html>
<head>
  <title>Responsive Admin template based on Bootstrap 3</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel='stylesheet' href='../public/authen/03a03bbe34da26df16eb239ba68ecc0a.css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,700|Roboto+Condensed:300,400,700' rel='stylesheet' type='text/css'>
  <link href="../public/authen/assets/favicon.ico" rel="shortcut icon">
  <link href="../public/authen/assets/apple-touch-icon.png" rel="apple-touch-icon">
</head>

<body class="glossed">
<div class="all-wrapper no-menu-wrapper light-bg">
  <div class="login-logo-w">
      <a href="<?=base_url().ADMINPATH;?>" class="logo">
      <i class="fa fa-rocket"></i>
    </a>
  </div>
  <div class="row">
    <div class="col-md-4 col-md-offset-4">

      <div class="widget widget-blue">
        <div class="widget-title">
          <h3 class="text-center"><i class="fa fa-lock"></i><?php echo $this->lang->line('login_system'); ?></h3>
        </div>
        <div class="widget-content">
          <?php echo $content; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="../public/authen/assets/js/jquery.min.js"></script>
<script src="../public/authen/assets/js/jquery-ui.min.js"></script>
<script src='../public/authen/ad67372f4b8b70896e8a596720082ac6.js'></script>
</body>
</html>
