<?php
/*
 * Payment Method
 */
if (!function_exists('PaymentMethod')) {
    function PaymentMethod($id=null) {
        $payment = array(
            0   => 'COD',
            1   => 'Chuyển khoản'
        );

        if ($id === null) {
            return $payment;
        }
        return $payment[$id];
    }
}