<?php
/*
 * Debug
 */
if (!function_exists('pre')) {
    function pre($var) {
        echo '<style>pre {display: block;padding: 9.5px;margin: 0 0 10px;font-size: 13px;line-height: 1.42857143;color: #333;word-break: break-all;word-wrap: break-word;background-color: #f5f5f5;border: 1px solid #ccc;border-radius: 4px;</style>';
        echo '<pre>';
        if (is_array($var)) {
            print_r($var);
        } else {
            var_dump($var);
        }
        echo '</pre>';
    }
}

if (!function_exists('ip')) {
    function ip() {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if ($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        echo $ipaddress;
    }
}

if (!function_exists('blacklist')) {
    function blacklist() {
        $whitelist = array();
        $CI =& get_instance();
        $CI->load->model("ipaccept","ipa");
        $strdata = $CI->ipa->getIPs(1);
        foreach($strdata as $item){
            $str2[] = $item->ip;
        }
        $file = "ipdeny.txt";
        if(file_exists($file)){
            $str1 = file_get_contents($file);
            $str1 = explode('|',$str1);
            $blacklist = array_unique(array_merge($str1,isset($str2) ? $str2 : array()));
        }
        else{
            foreach($str2 as $item){
                $blacklist[] = $item->ip;
            }
        }
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if (in_array ($ip, $blacklist)) {
           header("location: ".RETURN_IP);
           exit();
        }
    }
}

if (!function_exists('whitelist')) {
    function whitelist() {
        $whitelist = array();
        $CI =& get_instance();
        $CI->load->model("ipaccept","ipa");
        $strdata = $CI->ipa->getIPs();
        foreach($strdata as $item){
            $str2[] = $item->ip;
        }
        $file = "ipaccept.txt";
        if(file_exists($file)){
            $str1 = file_get_contents($file);
            $str1 = explode('|',$str1);
            $whitelist = array_unique(array_merge($str1,isset($str2) ? $str2 : array()));
        }
        else{
            foreach($str2 as $item){
                $whitelist[] = $item->ip;
            }
        }
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if (!in_array ($ip, $whitelist)) {
           header("location: ".RETURN_IP);
           exit();
        }
    }
}
