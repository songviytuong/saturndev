<?php
class Store_model extends CI_Model {

    const _tablename_store   = 'ttp_store';
    const _tablename_user    = 'ttp_user';
    const _tablename_store_mainmenu    = 'ttp_store_main_menu';
    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getStoreInfo($domain) {
        $this->db->select('*');
        $this->db->where('Domain', $domain);
        $result = $this->db->get(self::_tablename_store)->row();
        return $result;
    }

    function getUserInfo($uid) {
        $this->db->select('*');
        $this->db->where('ID', $uid);
        $result = $this->db->get(self::_tablename_user)->row();
        return $result;
    }
    
    function getUserMainMenu($uid) {
        $this->db->select('*');
        $this->db->where('UserID', $uid);
        $result = $this->db->get(self::_tablename_store_mainmenu)->result();
        if($result === null){
            return false;
        }
        return $result;
    }
}