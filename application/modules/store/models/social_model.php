<?php
class Social_model extends CI_Model {

    const _tablename        = 'ttp_social';
    const _tablename_data   = 'ttp_social_data';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getSocialList($uid = null){
        if($uid){
            $this->db->select('*');
            $this->db->from(self::_tablename_data);
            $this->db->join('ttp_social', 'ttp_social_data.SocialID = ttp_social.ID');
            $this->db->where('ttp_social_data.UserID',$uid);
            $this->db->where('ttp_social_data.Active',0);
            $result = $this->db->get()->result();
        }else{
            $this->db->select('*');
            $result = $this->db->get(self::_tablename)->result();
        }
        return $result;
    }
   
}