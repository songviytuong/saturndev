<?php
class Languages_model extends CI_Model {

    const _tablename        = 'ttp_languages';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getLanguages($Active = "")
    {
        $this->db->select('*');
        if($Active != ""){
            $this->db->where('LangActive',1);
        }
        $this->db->order_by('LangPosition',"ASC");
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
    
    public function getLangIDByName($LangName = "Vietnamese")
    {
        $this->db->select('LangID');
        $this->db->where('LangName',$LangName);
        $result = $this->db->get(self::_tablename)->row();
        return $result->LangID;
    }
    
    public function getLangAliasByName($LangName = "Vietnamese")
    {
        $this->db->select('LangAlias');
        $this->db->where('LangName',$LangName);
        $result = $this->db->get(self::_tablename)->row();
        return $result->LangAlias;
    }
    
    public function getLangFlagByName($LangName)
    {
        $this->db->select('LangFlag');
        $this->db->where_in('LangName',$LangName);
        $this->db->order_by('LangPosition',"ASC");
        $result = $this->db->get(self::_tablename)->row();
        return $result->LangFlag;
    }
    
    public function getLangIconByStore($ids)
    {
        $this->db->select('LangID,LangIcon');
        $this->db->where_in('LangID',$ids);
        $this->db->order_by('LangPosition',"ASC");
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
    
    public function getLanguagesIn($ids)
    {
        $this->db->select('*');
        $this->db->where_in('LangID',$ids);
        $this->db->order_by('LangPosition',"ASC");
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }

}