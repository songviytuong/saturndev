<?php
class Templates_model extends CI_Model {

    const _tablename        = 'ttp_templates';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getTemplateInfo($TempID){
        $this->db->select('TempName');
        $this->db->where('ID',$TempID);
        $row = $this->db->get(self::_tablename)->row();
        return $row;
    }
}