<!DOCTYPE html>
<html lang="<?=$LangAlias;?>">
    <?php
        $this->load->view('layout/header');
    ?>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head">
                        <header><a href="index.html"><img src="<?=$TEMPLATE_SRC;?>/images/logo.png" width="157" height="26" class="logo-img" alt="Inkwell logo"></a></header>
                        <nav>
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav-menu">
                                    <li>
                                        <a href="index.html">
                                            <h2>Home</h2>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="contact.html">
                                            <h2>Contact</h2>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="category.html">
                                            <h2>Category</h2>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="post.html">
                                            <h2>Post</h2>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="404.html">
                                            <h2>404 page</h2>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="../inkwell-rtl/index.html">
                                            <h2  style="color:red">RTL Version</h2>
                                        </a>
                                    </li>
                                </ul>
                                <!-- Search for mobile -->
                                <div class="search  hidden-md hidden-lg">
                                    <input type="text" class="search-input" placeholder="SEARCH">
                                    <a href="#">
                                        <div class="search-btn"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                                <!-- /Search for mobile -->
                                <ul class="nav-social-icons">
                                    <?php
                                        foreach($social_active_data as $key=>$row) {
                                    ?>
                                    <li><a href="<?=$row->Link;?>"><i class="fa <?=$row->Class."-".$row->Alias?>"></i></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <!-- Menu for mobile -->
                    <div class="select-cat-sm hidden-lg hidden-md">
                        <select>
                            <option value="select-cat">Select Category...</option>
                            <option value="">Design</option>
                            <option value="">Web Design</option>
                            <option value="">Responsive</option>
                            <option value="">Typography</option>
                            <option value="">Inspiration</option>
                            <option value="">Mobile</option>
                            <option value="">iPhone & iPad</option>
                            <option value="">Android</option>
                            <option value="">Design Patterns</option>
                            <option value="">Coding</option>
                            <option value="">CSS</option>
                            <option value="">HTML</option>
                            <option value="">JavaScript</option>
                            <option value="">Techniques</option>
                        </select>
                    </div>
                    <!-- End menu for mobile -->
                </div>
                <!--Content-->
                <div class="col-md-12">
                    <div class="content">
                        <div class="content-box">
                            <div class="content-box-head">
                                <h3>PAGE NOT FOUND</h3>
                                <i class="fa fa-info content-box-head-icon yellow-color"></i>
                            </div>
                            <div class="error-page content-box-body">
                                <h2>Oops!</h2>
                                <h3>Looks like this page doesn’t exist.</h3>
                                <h4>Can’t find what you need? Take a moment and do a search below!</h4>
                                <div class="error-search">
                                    <input type="text" placeholder="Search...">
                                    <a href="#">
                                        <div class="error-search-btn Subscribe-btn"><i class="fa fa-paper-plane-o"></i></div>
                                    </a>
                                </div>
                                <div class="back-home-section"><i class="fa fa-reply send-comment-btn blue-color"></i><a href="index.html" class="back-home-a">Back to home</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Content-->
                <div class="col-md-12">
                    <footer>
                        <div class="col-md-6">
                            <div class="footer-about">
                                <h3>About Us</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris. tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.tempor incididunt ut labore et dolore magna aliqua.incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="footer-links">
                                <h3>Links</h3>
                                <ul>
                                    <li><a href="#">First link</a></li>
                                    <li><a href="#">Second link</a></li>
                                    <li><a href="#">Third link</a></li>
                                    <li><a href="#">Fourth link</a></li>
                                    <li><a href="#">Fifth link</a></li>
                                    <li><a href="#">Sixth link</a></li>
                                    <li><a href="#">Seventh link</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-newsletter">
                                <h3>Inkwell Newsletter</h3>
                                <div class="email-flat-icon"></div>
                                <p>Subscribe to our email newsletter for useful tips and valuable resources.</p>
                                <div class="Subscribe-form">
                                    <input type="text" name="Subscribe" value="" placeholder="Enter your email...">
                                    <div id="result"></div>
                                    <a onclick="ajaxSubScribe(this)" class="btn">
                                        <div class="Subscribe-btn"><i class="fa-icon fa fa-paper-plane-o"></i></div>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                    </footer>
                    <p class="footer-copyright">© Copyright 2016 -  <?=$store_name;?> Được thiết kế bởi <a href="#">iSTORE</a></p>
                </div>
                <script src="<?=$TEMPLATE_SRC;?>/js/jquery.min.js"></script>
                <script src="<?=$TEMPLATE_SRC;?>/js/bootstrap.min.js"></script>
                <script src="<?=$TEMPLATE_SRC;?>/js/owl-carousel/owl.carousel.min.js"></script>
                <script src="<?=$TEMPLATE_SRC;?>/js/jQuery.style.switcher.js"></script>
                <script src="<?=$TEMPLATE_SRC;?>/js/main.js"></script>
            </div>
        </div>
        <div class="footer-bttom-line"></div>
        <ul id="style-switcher">
            <li style="background-color:#ff4e00;"><a href="javascript: void(0)" data-theme="orange"></a></li>
            <li style="background-color:#EC4134;"><a href="javascript: void(0)" data-theme="red"></a></li>
            <li style="background-color:#e93170;"><a href="javascript: void(0)" data-theme="pink"></a></li>
            <li style="background-color:#4CAF50;"><a href="javascript: void(0)" data-theme="green"></a></li>
            <li style="background-color:#009587;"><a href="javascript: void(0)" data-theme="teal"></a></li>
            <li style="background-color:#00aff0;"><a href="javascript: void(0)" data-theme="cyan"></a></li>
            <li style="background-color:#2092EC;"><a href="javascript: void(0)" data-theme="blue"></a></li>
            <li style="background-color:#9926AC;"><a href="javascript: void(0)" data-theme="purple"></a></li>
            <li style="background-color:#3D4EAE;"><a href="javascript: void(0)" data-theme="indigo"></a></li>
        </ul>
    </body>
</html>
