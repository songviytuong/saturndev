<!DOCTYPE html>
<html lang="<?= $LangAlias; ?>">
    <?php
        $this->load->view('layout/header');
    ?>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head">
                        <header><a href="/"><img src="<?= $TEMPLATE_SRC; ?>/images/logo.png" width="157" height="26" class="logo-img" alt="<?=$STORE_NAME;?>"></a></header>
                        <nav>
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <?php
                                $this->load->view('layout/nav-menu');
                                ?>
                                <!-- Search for mobile -->
                                <div class="search  hidden-md hidden-lg">
                                    <input type="text" class="search-input" placeholder="SEARCH">
                                    <a href="#">
                                        <div class="search-btn"><i class="fa fa-search"></i></div>
                                    </a>
                                </div>
                                <!-- /Search for mobile -->
                                <ul class="nav-social-icons">
                                    <?php
                                    foreach ($social_active_data as $key => $row) {
                                        ?>
                                        <li><a href="<?= $row->Link; ?>"><i class="fa <?= $row->Class . "-" . $row->Alias ?>"></i></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <!-- Menu for mobile -->
                    <div class="select-cat-sm hidden-lg hidden-md">
                        <select>
                            <option value="select-cat">Select Category...</option>
                            <option value="">Design</option>
                            <option value="">Web Design</option>
                            <option value="">Responsive</option>
                            <option value="">Typography</option>
                            <option value="">Inspiration</option>
                            <option value="">Mobile</option>
                            <option value="">iPhone & iPad</option>
                            <option value="">Android</option>
                            <option value="">Design Patterns</option>
                            <option value="">Coding</option>
                            <option value="">CSS</option>
                            <option value="">HTML</option>
                            <option value="">JavaScript</option>
                            <option value="">Techniques</option>
                        </select>
                    </div>
                    <!-- End menu for mobile -->
                </div>
                <div class="col-md-8">
                    <div class="content">
                        <div class="index-slider">
                            <div class="col-md-7">
                                <a href="post.html">
                                    <div class="index-slider-left" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/woman-1129248.jpg')">
                                        <div class="index-slider-left-text">
                                            <h3>Nulla pariatur ex cepteur sint occa cupidatat non proi dent sunt in</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-5">
                                <div class="index-slider-right">
                                    <a href="post.html">
                                        <div class="slider-right-one" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/almond-blossom-1229138.jpg')">
                                            <div class="slider-right-one-text">
                                                <h3>Sed do eiusmod tempor incidi dunt ut labore et dolore magna.</h3>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="post.html">
                                        <div class="slider-right-two" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/alcedo-atthis-881594.jpg')">
                                            <div class="slider-right-two-text">
                                                <h3>Sed do eiusmod tempor incidi dunt ut labore et dolore magna.</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <article class="list-article" itemscope itemtype="http://schema.org/Article">
                            <a href="post.html">
                                <div class="article-image" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/girl-1262801.jpg')"></div>
                            </a>
                            <div class="article-text">
                                <a href="post.html">
                                    <h2 itemprop="name">Nulla pariatur ex cepteur sint occa cupidatat non proi dent sunt in</h2>
                                </a>
                                <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <a href="post.html" class="read-more-a" itemprop="url"><i class="fa fa-file-text-o read-more-btn"></i>Read More...</a>
                            </div>
                        </article>
                        <article class="list-article" itemscope itemtype="http://schema.org/Article">
                            <a href="post.html">
                                <div class="article-image" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/sunglasses-635269.jpg')"></div>
                            </a>
                            <div class="article-text">
                                <a href="post.html">
                                    <h2 itemprop="name">Nulla pariatur ex cepteur sint occa cupidatat non proi dentar sus cipit</h2>
                                </a>
                                <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <a href="post.html" class="read-more-a" itemprop="url"><i class="fa fa-file-text-o read-more-btn"></i>Read More...</a>
                            </div>
                        </article>
                        <article class="list-article" itemscope itemtype="http://schema.org/Article">
                            <a href="post.html">
                                <div class="article-image" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/apples-635239.jpg')"></div>
                            </a>
                            <div class="article-text">
                                <a href="post.html">
                                    <h2 itemprop="name">Nulla pariatur ex cepteur sint occa cupidatat non proi dentar sus cipit</h2>
                                </a>
                                <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <a href="post.html" class="read-more-a" itemprop="url"><i class="fa fa-file-text-o read-more-btn"></i>Read More...</a>
                            </div>
                        </article>
                        <article class="list-article" itemscope itemtype="http://schema.org/Article">
                            <a href="post.html">
                                <div class="article-image" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/girl-711087.jpg')"></div>
                            </a>
                            <div class="article-text">
                                <a href="post.html">
                                    <h2 itemprop="name">Nulla pariatur ex cepteur sint occa cupidatat non proi dentar sus cipit</h2>
                                </a>
                                <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <a href="post.html" class="read-more-a" itemprop="url"><i class="fa fa-file-text-o read-more-btn"></i>Read More...</a>
                            </div>
                        </article>
                        <article class="list-article" itemscope itemtype="http://schema.org/Article">
                            <a href="post.html">
                                <div class="article-image" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/girl-1219339.jpg')"></div>
                            </a>
                            <div class="article-text">
                                <a href="post.html">
                                    <h2 itemprop="name">Nulla pariatur ex cepteur sint occa cupidatat non proi dentar sus cipit</h2>
                                </a>
                                <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <a href="post.html" class="read-more-a" itemprop="url"><i class="fa fa-file-text-o read-more-btn"></i>Read More...</a>
                            </div>
                        </article>
                        <article class="list-article" itemscope itemtype="http://schema.org/Article">
                            <a href="post.html">
                                <div class="article-image" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/young-woman-635249.jpg')"></div>
                            </a>
                            <div class="article-text">
                                <a href="post.html">
                                    <h2 itemprop="name">Nulla pariatur ex cepteur sint occa cupidatat non proi dentar sus cipit</h2>
                                </a>
                                <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                <a href="post.html" class="read-more-a" itemprop="url"><i class="fa fa-file-text-o read-more-btn"></i>Read More...</a>
                            </div>
                        </article>
                        <div class="pagination">
                            <ul>
                                <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <aside>
                        <div class="search hidden-xs hidden-sm">
                            <input type="text" class="search-input" placeholder="SEARCH">
                            <a href="#">
                                <div class="search-btn"><i class="fa fa-search"></i></div>
                            </a>
                        </div>
                        <div class="about-me">
                            <div class="about-me-content">
                                <div class="about-me-avatar" style="background-image:url('<?= $LOGO_SRC; ?>');"></div>
                                <h2><?= $StoreName[$LangID]; ?></h2>
                                <h5><i><?= $Slogan[$LangID]; ?></i></h5>
                                <p class="blockquote-reverse">"<?= $Intro[$LangID]; ?>"</p>
                                <ul class="about-me-social">
                                    <?php
                                    foreach ($social_active_data as $key => $row) {
                                        ?>
                                        <li><a href="<?= $row->Link; ?>"><i class="fa <?= $row->Class . "-" . $row->Alias ?>"></i></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="content-box hidden-sm hidden-xs">
                            <div class="content-box-head">
                                <h3>CATEGORY</h3>
                                <i class="fa fa-bars content-box-head-icon orange-color"></i>
                            </div>
                            <div class="categories content-box-body">
                                <ul>
                                    <li>
                                        <a href="category.html">
                                            <div class="rectangle-list-style"></div>
                                            Design
                                        </a>
                                        <ul>
                                            <li><a href="category.html">Web Design</a></li>
                                            <li><a href="category.html">Responsive</a></li>
                                            <li><a href="category.html">Typography</a></li>
                                            <li><a href="category.html">Inspiration</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="category.html">
                                            <div class="rectangle-list-style"></div>
                                            Mobile
                                        </a>
                                        <ul>
                                            <li><a href="category.html">iPhone & iPad</a></li>
                                            <li><a href="category.html">Android</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="category.html">
                                            <div class="rectangle-list-style"></div>
                                            Coding
                                        </a>
                                        <ul>
                                            <li><a href="category.html">CSS</a></li>
                                            <li><a href="category.html">HTML</a></li>
                                            <li><a href="category.html">JavaScript</a></li>
                                            <li><a href="category.html">PHP</a></li>
                                            <li><a href="category.html">Techniques</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-head">
                                <h3>POPULAR</h3>
                                <i class="fa fa-star content-box-head-icon yellow-color"></i>
                            </div>
                            <div class="popular content-box-body">
                                <ul>
                                    <li>
                                        <a href="post.html">
                                            <div class="rectangle-list-style"></div>
                                            <p>Quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo con.</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="post.html">
                                            <div class="rectangle-list-style"></div>
                                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="post.html">
                                            <div class="rectangle-list-style"></div>
                                            <p>Lobortis nisl ut aliquip ex ea commodo vulputate suscipit lobortis nisl ut.</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="post.html">
                                            <div class="rectangle-list-style"></div>
                                            <p>Vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio .</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="post.html">
                                            <div class="rectangle-list-style"></div>
                                            <p>Blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-head">
                                <h3>TWITTER</h3>
                                <i class="fa fa-twitter content-box-head-icon blue-color"></i>
                            </div>
                            <div class="twitter-box content-box-body">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-twitter gray-color"></i>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-twitter gray-color"></i>
                                            <p>nostrud exerci tation ullamcor per suscipit lobortis nisl ut aliquip ex ea commodo cons.</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-twitter gray-color"></i>
                                            <p>dolor sit amet, consectetur adipisicing elit, sed do eius mod tempor incididunt.</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="content-box">
                            <div class="content-box-head">
                                <h3>INSTAGRAM</h3>
                                <i class="fa fa-instagram content-box-head-icon darkblue-color"></i>
                            </div>
                            <div class="instagram-box content-box-body">
                                <div id="owl-demo" class="owl-carousel">
                                    <div class="item"><img class="lazyOwl" data-src="<?= $TEMPLATE_SRC; ?>/images/woman-1369253.jpg" alt="Lazy Owl Image"></div>
                                    <div class="item"><img class="lazyOwl" data-src="<?= $TEMPLATE_SRC; ?>/images/girl-with-flowers-1374221.jpg" alt="Lazy Owl Image"></div>
                                    <div class="item"><img class="lazyOwl" data-src="<?= $TEMPLATE_SRC; ?>/images/ladybug-1324398.jpg" alt="Lazy Owl Image"></div>
                                    <div class="item"><img class="lazyOwl" data-src="<?= $TEMPLATE_SRC; ?>/images/fashion-1063100.jpg" alt="Lazy Owl Image"></div>
                                </div>
                                <div class="instagram-avatar" style="background-image:url('<?= $TEMPLATE_SRC; ?>/images/instagram-avatar.jpg');"></div>
                                <a href="#" class="instagram-user-link">PhotoInsta</a>
                            </div>
                        </div>
                    </aside>
                </div>
                <div class="col-md-12">
                    <footer>
                        <div class="col-md-6">
                            <div class="footer-about">
                                <h3>About Us</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris. tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.tempor incididunt ut labore et dolore magna aliqua.incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="footer-links">
                                <h3>Links</h3>
                                <ul>
                                    <li><a href="#">First link</a></li>
                                    <li><a href="#">Second link</a></li>
                                    <li><a href="#">Third link</a></li>
                                    <li><a href="#">Fourth link</a></li>
                                    <li><a href="#">Fifth link</a></li>
                                    <li><a href="#">Sixth link</a></li>
                                    <li><a href="#">Seventh link</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-newsletter">
                                <h3>Inkwell Newsletter</h3>
                                <div class="email-flat-icon"></div>
                                <p>Subscribe to our email newsletter for useful tips and valuable resources.</p>
                                <div class="Subscribe-form">
                                    <input type="text" name="Subscribe" value="" placeholder="Enter your email...">
                                    <div id="result"></div>
                                    <a onclick="ajaxSubScribe(this)" class="btn">
                                        <div class="Subscribe-btn"><i class="fa-icon fa fa-paper-plane-o"></i></div>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </footer>
                    <p class="footer-copyright">© Copyright 2016 - <?= $STORE_NAME; ?> Được thiết kế bởi <a href="#">iSTORE</a></p>
                </div>
                <script src="<?= $TEMPLATE_SRC; ?>/js/jquery.min.js"></script>
                <script src="<?= $TEMPLATE_SRC; ?>/js/bootstrap.min.js"></script>
                <script src="<?= $TEMPLATE_SRC; ?>/js/owl-carousel/owl.carousel.min.js"></script>
                <script src="<?= $TEMPLATE_SRC; ?>/js/jQuery.style.switcher.js"></script>
                <script src="<?= $TEMPLATE_SRC; ?>/js/main.js"></script>
            </div>
        </div>
        <div class="footer-bttom-line"></div>
        <ul id="style-switcher">
            <li style="background-color:#ff4e00;"><a href="javascript: void(0)" data-theme="orange"></a></li>
            <li style="background-color:#EC4134;"><a href="javascript: void(0)" data-theme="red"></a></li>
            <li style="background-color:#e93170;"><a href="javascript: void(0)" data-theme="pink"></a></li>
            <li style="background-color:#4CAF50;"><a href="javascript: void(0)" data-theme="green"></a></li>
            <li style="background-color:#009587;"><a href="javascript: void(0)" data-theme="teal"></a></li>
            <li style="background-color:#00aff0;"><a href="javascript: void(0)" data-theme="cyan"></a></li>
            <li style="background-color:#2092EC;"><a href="javascript: void(0)" data-theme="blue"></a></li>
            <li style="background-color:#9926AC;"><a href="javascript: void(0)" data-theme="purple"></a></li>
            <li style="background-color:#3D4EAE;"><a href="javascript: void(0)" data-theme="indigo"></a></li>
        </ul>
    </body>
</html>