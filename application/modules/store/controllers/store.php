<?php 
class Store extends CI_Controller {
    public $domain;
    public $subdomain;
    public $classname="cpanel";
    
    public function __construct() {
        parent::__construct();
        
        #BEGIN: ACTIVE SMARTY
        $this->load->library('parser');
        #$this->parser->parse("demo.tpl", $data);
        
        $this->domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
        $this->subdomain = explode('.', $this->domain);
        if (count($this->subdomain) == 3) {
            $this->session->set_userdata('subdomain', $this->subdomain[0]);
        } else {
            $this->session->set_userdata('domain', $this->domain);
        }
        
        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
    }

    public function index() {
        #BEGIN: DEFINE LANGUAGE
        $this->load->model("languages_model","_lang");
        $LangID = $this->_lang->getLangIDByName($this->cur_lang);
        $LangAlias = $this->_lang->getLangAliasByName($this->cur_lang);
        $LangAlias = explode("_", $LangAlias);
        $data["LangID"] = $LangID;
        $data["LangAlias"] = $LangAlias[0];
        
        #SHOW STORE
        if (count($this->subdomain) >= 3) {
            $domain = $this->session->userdata('subdomain');
            $domain = $domain . "." . HACKER;
        } else {
            $domain = $this->session->userdata('domain');
        }
        
        #GET STORE INFO
        $this->load->model("store_model","store");
        $store = $this->store->getStoreInfo($domain);
        if($store){
            $data["StoreName"]  = unserialize($store->StoreName);
            $data["Slogan"]     = unserialize($store->Slogan);
            $data["Intro"]      = unserialize($store->Intro);
        }
        
        #GET USER INFO
        $userinfo = $this->store->getUserInfo($store->UserID);
        $data["userGlobal"] = $userinfo;
        
        #GET TEMPLATES INFO
        $this->load->model("templates_model","temp");
        $temp = $this->temp->getTemplateInfo($store->TempID);
        $data["tempGlobal"] = $temp;

        #DEFINE SRC TEMPLATE
        $data["TEMPLATE_SRC"] = DYNAMIC_TEMPLATE_SRC . $temp->TempName;
        
        #SHOW SITENAME WITH SLOGAN
        if($store->SloganActive == 1){
            $site_name = $data["StoreName"][$LangID] . " | " . $data["Slogan"][$LangID];
        }else{
            $site_name = $data["StoreName"][$LangID];
        }
        
        #SHOW SOCIAL
        $this->load->model("social_model","social");
        $social = $this->social->getSocialList();
        $data["social"] = $social;
        
        #SOCIAL DATA
        $social_active_data = $this->social->getSocialList($store->UserID);
        $data["social_active_data"] = $social_active_data;
        
        #SHOW META KEYWORDS & DESCRIPTION
        $data["LOGO_SRC"] = PUBLIC_MEDIA_SRC."user".$store->UserID."/".$store->LogoSrc;
        $data["SITE_NAME"] = $site_name;
        $data["STORE_NAME"] = $data["StoreName"][$LangID];
        $data["META_KEYWORDS"] = $store->MetaKeywords;
        $data["META_DESCRIPTION"] = $store->MetaDescription;
        
        #BEGIN: LOAD MAIN MENU
        $mainMenu = $this->store->getUserMainMenu($store->UserID);
        $data["MainMenu"] = $mainMenu;
        
        $current = str_replace(PAGE_EXTENSION, "", uri_string());
        $data["menu_active"] = ($current) ? $current : 'index';
        switch ($current){
            case 'index':
                $data["SITE_NAME"] = "Trang chủ";
                $this->load->view('store/' . $temp->TempName . '/templates', $data);
                break;
            case 'notFound':
                $this->load->view('store/' . $temp->TempName . '/templates_404', $data);
                break;
            default:
                $this->load->view('store/' . $temp->TempName . '/templates', $data);
                break;
        }
    }
    
    #BEGIN: MENU MAIN
    public function mainMenu(){
//        session_start();
//        $this->session->unset_userdata('session_id');
//        echo "<h3> PHP List All Session Variables</h3>";
//        foreach ($this->session->userdata as $key=>$val)
//        echo $key." ".$val."<br/>";
//        $data["url"] = current_url();
        pre(uri_string());     
    }
    
    #BEGIN: MENU NEWS
    public function newsMenu(){
        
    }
    
    #BEGIN: MENU PRODUCTS
    public function productsMenu(){
        
    }

    public function test() {
        echo "test Store";
    }

    public function my404() {
        redirect('/notFound.html');
        die("404 Page");
    }

    public function detail($slug = null, $id = null) {
        echo "Slug: " . $slug;
        echo "<br/>";
        echo "Trang ID: " . $id;
    }
    
    /* Ajax
     * Action Newsletter
     */
    function ajaxNewsLetter(){
        echo "OK";
    }
}
?>