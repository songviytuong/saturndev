<?php 
class Home extends CI_Controller { 
    public function __construct() { 
        parent::__construct();
    }
    public function index()
    {
        echo "index";
        //$this->template->render();
        pre(PaymentMethod());
        //print_r($this->fetch_fb_fans('285086208340955', 2, 400000));
    }
    
    function fetch_fb_fans($fanpage_name, $no_of_retries = 10, $pause = 500000 /* 500ms */) {
        $ret = array();
        // prepare real like user agent and accept headers
        $context = stream_context_create(array('http' => array('header' => 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/49.0.2623.108 Chrome/49.0.2623.108 Safari/537.36\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\nAccept-encoding: gzip, deflate, sdch\r\nAccept-language: en-US,en;q=0.8,pl;q=0.6\r\n')));
        // get page id from facebook html og tags for mobile apps
        $fanpage_html = file_get_contents('https://www.facebook.com/' . $fanpage_name, false, $context);
        if (!preg_match('{fb://page/(\d+)}', $fanpage_html, $id_matches)) {
            // invalid fanpage name
            return $ret;
        }
        pre($fanpage_html); exit();
        $url = 'http://www.facebook.com/plugins/fan.php?connections=100&id=' . $id_matches[1];
        for ($a = 0; $a < $no_of_retries; $a++) {
            $like_html = file_get_contents($url, false, $context);
            preg_match_all('{href="https?://www\.facebook\.com/([a-zA-Z0-9\._-]+)" data-jsid="anchor" target="_blank"}', $like_html, $matches);
            if (empty($matches[1])) {
                // failed to fetch any fans - convert returning array, cause it might be not empty
                return array_keys($ret);
            } else {
                // merge profiles as array keys so they will stay unique
                $ret = array_merge($ret, array_flip($matches[1]));
            }
            // don't get banned as flooder
            usleep($pause);
        }
        return array_keys($ret);
    }

    public function test()
    {
        echo "test";
        //$this->template->render();
    }
    
    public function my404(){
        echo "Home 404";
    }
    public function decline(){
        echo "decline";
    }
    
    
}
?>