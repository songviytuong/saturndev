<?php

class Authentication extends CI_Controller
{
    public $classname = 'cpanel';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->template->set_template('authen');
        $this->template->add_doctype();

        //BEGIN: LOAD LANGUAGE
        $this->cur_lang = 'english';
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== '') {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        //BEGIN: END LANGUAGE
    }

    public function index()
    {
        $this->is_login();
        redirect('/cpanel');
        $this->template->render();
    }

    public function is_login()
    {
        $user = $this->session->userdata('usercp');
        if ($user == '') {
            redirect(ADMINPATH.'/login');
        }
    }

    public function logout()
    {
        $this->load->library('facebook');
        // Logs off session from website
        $this->facebook->destroy_session();
        // Make sure you destory website session as well.

        $this->session->unset_userdata('usercp');
        $this->session->sess_destroy();
        redirect(ADMINPATH);
    }

    public function login()
    {
        //BEGIN: BLACK & WHITE LIST
        blacklist();
        whitelist();
        //BEGIN: LOGIN USING FACEBOOK
        $this->load->library('facebook');
        $data['user'] = array();
        // Check if user is logged in
        if ($this->facebook->is_authenticated()) {
            // User logged in, get user details
            $user = $this->facebook->request('get', '/me?fields=id,name,email,gender');
            if (!isset($user['error'])) {
                $data['user'] = $user;
                $FacebookID = $user['id'];
                $result = $this->db->query("SELECT *,count(*) as count FROM ttp_user WHERE FacebookID = $FacebookID")->row();
                if ($result->count > 0) {
                    $this->session->set_userdata('usercp', $result->UserName);
                } else {
                    $dataInsert = array();

                    $img = file_get_contents('https://graph.facebook.com/'.$user['id'].'/picture?type=large');
                    $path = 'public/authen/avatar/';
                    $file = $path.$user['id'].'.jpg';
                    if (!file_exists($file)) {
                        file_put_contents($file, $img);
                    }

                    $fullname = explode(' ', $user['name']);

                    $dataInsert['FacebookID'] = $user['id'];
                    $dataInsert['UserName'] = strtolower($fullname[0].$fullname[1]);
                    $dataInsert['FirstName'] = $fullname[0];
                    $dataInsert['LastName'] = $fullname[1];
                    $dataInsert['Email'] = $user['email'];
                    $dataInsert['Thumb'] = $file;
                    $dataInsert['Gender'] = ($user['gender'] == 'male') ? 1 : 0;

                    $_existCus = $this->db->query("SELECT ID,RoleID,DetailRole,UserType,DepartmentID FROM ttp_user WHERE ROLEID = '8' ORDER BY ID DESC LIMIT 0,1")->row();
                    $dataInsert['RoleID'] = $_existCus->RoleID;
                    $dataInsert['DetailRole'] = $_existCus->DetailRole;
                    $dataInsert['UserType'] = $_existCus->UserType;
                    $dataInsert['DepartmentID'] = $_existCus->DepartmentID;
                    $dataInsert['Published'] = (PUBLISHED == 1) ? 1 : 0;
                    $dataInsert['Created'] = date('Y-m-d h:m:s', time());
                    $dataInsert['LastEdited'] = date('Y-m-d h:m:s', time());
                    $this->db->insert('ttp_user', $dataInsert);
                    //Insert new User
                }
            }
        }
        //BEGIN: RENDER VIEW
        $view = 'login';
        $this->template->write_view('content', $view, $data);
        $this->template->render();
    }

    //BEGIN: REGISTER

    public function register()
    {
        if (REGISTER == 'false') {
            redirect('/cpanel');
        }
        $view = 'register';
        $this->template->write_view('content', $view);
        $this->template->render();
    }

    //BEGIN: LOGIN

    public function login_now()
    {
        if (isset($_POST['username']) && isset($_POST['password'])) {
            $username = $this->security->xss_clean($_POST['username']);
            $password = $this->security->xss_clean($_POST['password']);
            //$token = $this->security->xss_clean($_POST['ttp']);
            $temp = $password;
            // $username = mysql_real_escape_string($username);
            // $password = mysql_real_escape_string($password);
            $password = sha1($password);
            $result = $this->db->query("select a.ID,a.UserType,a.PageDefault from ttp_user a,ttp_role b where a.UserName = '$username' and a.Password='$password' and a.Published=1 and b.Published=1 and a.RoleID=b.ID")->row();
            if ($result) {
                //$this->accept_ip($result->IsAdmin,$token);
                $this->session->set_userdata('usercp', $username);
                $this->session->set_userdata('_token', time().MYKEY);

                $_token = substr(sha1($this->session->userdata('_token')), TOKENF, TOKENT);
                if ($result->PageDefault) {
                    redirect($result->PageDefault.'/?_token='.$_token);
                } else {
                    redirect(ADMINPATH);
                }
            }
            redirect(ADMINPATH.'/login/error');
        }
    }
}
