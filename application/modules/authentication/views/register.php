<form action="" role="form">
    <div class="lined-separator">Or register using email</div>
    <div class="form-group relative-w">
        <input type="text" class="form-control" placeholder="Full Name">
        <i class="fa fa-user input-abs-icon"></i>
    </div>
    <div class="form-group relative-w">
        <input type="email" class="form-control" placeholder="Enter email">
        <i class="fa fa-envelope-alt input-abs-icon"></i>
    </div>
    <div class="form-group relative-w">
        <input type="password" class="form-control" placeholder="Password">
        <i class="fa fa-lock input-abs-icon"></i>
    </div>
    <div class="form-group relative-w">
        <input type="password" class="form-control" placeholder="Confirm Password">
        <i class="fa fa-lock input-abs-icon"></i>
    </div>
    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="checkbox"> I agree to <a href="#">terms of use</a> and <a href="#">policy</a>.
            </label>
        </div>
    </div>
    <a href="index.html" class="btn btn-success btn-rounded btn-iconed">
        <span>Register Now</span>
        <i class="fa fa-arrow-right"></i>
    </a>
    <div class="no-account-yet">
        Already have an account? <a href="<?php echo base_url() . ADMINPATH . "/login"; ?>">Login</a>
    </div>
</form>