<form method="post" action="<?php echo base_url() . ADMINPATH . "/check-login" ?>">
    <?php if ( $this->facebook->is_authenticated()):?>
    <div class="row">
        <div class="col-lg-12 text-center">
            <img class="img-thumbnail" data-src="holder.js/140x140" alt="140x140" src="https://graph.facebook.com/<?= $user['id'] ?>/picture?type=large" style="width: 140px; height: 140px;">
            <h2><?= $user['name'] ?></h2>
            <div class="">
                <a href="<?php echo base_url() . ADMINROOT .'/profiles' ?>" class="btn btn-default btn-xs"><i class="fa fa-user"></i><?php echo $this->lang->line('profiles'); ?></a>
                <a href="<?php echo base_url() . ADMINROOT ?>" class="btn btn-success btn-xs"><i class="fa fa-sign-in"></i><?php echo $this->lang->line('cpanel'); ?></a>
                <a href="<?php echo $this->facebook->logout_url(); ?>" class="btn btn-primary btn-xs"><i class="fa fa-sign-out"></i><?php echo $this->lang->line('logout'); ?></a>
            </div>
        </div>
    </div>
    <?php else: ?>
    <a href="<?php echo $this->facebook->login_url(); ?>" class="facebook-connect">
        <i class="fa fa-facebook"></i>
        <?php echo $this->lang->line('connect_using_facebook'); ?>
    </a>
    <div class="lined-separator"><?php echo $this->lang->line('or_login_using_username'); ?></div>
    <div class="form-group relative-w">
        <input type="text" name="username" class="form-control" placeholder="<?php echo $this->lang->line('Username'); ?>" id="username" autocomplete="off" />
        <i class="fa fa-user input-abs-icon"></i>
    </div>
    <div class="form-group relative-w">
        <input type="password" name="password" class="form-control" placeholder="<?php echo $this->lang->line('Password'); ?>" id="password"  autocomplete="off" />
        <i class="fa fa-lock input-abs-icon"></i>
    </div>
    <div class="form-group">
        <div class="checkbox">
            <label for="remember_login">
                <input type="checkbox" name="remember_login"/><?php echo $this->lang->line('remember'); ?>
            </label>
        </div>
    </div>
    <button class="btn btn-primary btn-rounded btn-iconed">
        <span><?php echo $this->lang->line('login'); ?></span>
        <i class="fa fa-arrow-right"></i>
    </button>
    <div class="no-account-yet">
        <?php echo $this->lang->line('dont_have_an_account_yet'); ?> <a href="<?php echo base_url() . ADMINPATH . "/register"; ?>"><?php echo $this->lang->line('register'); ?></a>
    </div>
    <?php endif; ?>
</form>         