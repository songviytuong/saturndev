<?php
class Loans_model extends CI_Model {

    const _tablename_customers      = 'm_customers';
    const _tablename_loans          = 'm_loans';
    const _tablename_members        = 'm_members';
    const _tablename_members_ns        = 'm_members_ns';
    const _tablename_loans_return  = 'm_loans_details';
    const _tablename_branch  = 'm_branch';
    const _tablename_money  = 'm_money_details';
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getMembers()
    {
        $userID = $this->user->ID;
        $this->db->select('*');
        $this->db->where('userID',$userID);
        $result = $this->db->get(self::_tablename_members)->result();
        return $result;
    }
    
    public function getMembersDetail()
    {
        $this->db->select('*,br.name as branchname,br.isdefault as isdefault,br.userID as userID');
        $this->db->from('m_members m');
        $this->db->join('m_branch br','m.branchID = br.id','inner');
        $result = $this->db->get()->result();
        return $result;
    }
    
    public function getPersonalList($mID = null){
        $this->db->select('*');
        if($mID){
            $this->db->where('memID',$mID);
        }
        $result = $this->db->get(self::_tablename_members_ns)->result();
        return $result;
    }
    
    public function getMoneyList($userID = null,$id = null){
        $this->db->select('*');
        $this->db->where('del_flag',0);
        $this->db->where('userID',$userID);
        if($id){
            $this->db->where('id',$id);
            $result = $this->db->get(self::_tablename_money)->row();
        }else{
            $result = $this->db->get(self::_tablename_money)->result();
        }
        return $result;
    }
    
    public function getReturnMoneyList($userID = null){
        $today = date('Y-m-d');
        $this->db->distinct();
        $this->db->group_by('d.loanID');
        $this->db->select('*,d.loanID as ordercode,b.name as branchname,l.created as created');
        $this->db->from('m_loans_details d');
        $this->db->join('m_customers c','d.cID = c.id','inner');
        $this->db->join('m_loans l','l.id = d.loanID','inner');
        $this->db->join('m_branch b','b.id = l.branchID','inner');
        $this->db->where('d.userID',$userID);
        $this->db->not_like('d.return_date',"%".$today."%");
        $this->db->order_by('ordercode','ASC');
        $result = $this->db->get()->result();
        return $result;
    }
    
    public function getCustomers()
    {
        $userID = $this->user->ID;
        $this->db->select('*');
        $this->db->where('userID',$userID);
        $result = $this->db->get(self::_tablename_customers)->result();
        return $result;
    }
    
    public function getCMNDCustByID($id){
        $this->db->select('cmnd_src');
        $this->db->where('id',$id);
        $result = $this->db->get(self::_tablename_customers)->row();
        return $result->cmnd_src;
    }
    
    public function updateCustomer($id,$data){
        $this->db->where("id", $id);
        return $this->db->update(self::_tablename_customers, $data);
    }
    
    public function updateMoney($id,$data){
        $this->db->where("id", $id);
        return $this->db->update(self::_tablename_money, $data);
    }
    
    public function insertCustomerInfo($data){
        $this->db->insert(self::_tablename_customers,$data);
        return $this->db->insert_id();
    }
    
    public function insertLoans($data){
        $this->db->insert(self::_tablename_loans,$data);
        return $this->db->insert_id();
    }
    
    public function insertReturn($data){
        $this->db->insert(self::_tablename_loans_return,$data);
        return $this->db->insert_id();
    }
    
    public function updateMoneyStatus($id,$data){
        $this->db->where("id", $id);
        return $this->db->update(self::_tablename_money, $data);
    }
    
    public function deleteMoney($id){
        $this->db->where("id", $id);
        return $this->db->delete(self::_tablename_money);
    }
    
    public function insertBranch($data){
        $this->db->insert(self::_tablename_branch,$data);
        return $this->db->insert_id();
    }
    
    public function updateBranch($id,$data){
        $this->db->where("id", $id);
        return $this->db->update(self::_tablename_branch, $data);
    }
    
    public function insertMoney($data){
        $this->db->insert(self::_tablename_money,$data);
        return $this->db->insert_id();
    }
    
    public function insertMember($data){
        $this->db->insert(self::_tablename_members,$data);
        return $this->db->insert_id();
    }
    
    public function getLoansBycID($cID = null){
        $this->db->select('*');
        if($cID){
            $this->db->where('cID',$cID);
        }
        $result = $this->db->get(self::_tablename_loans)->result();
        return $result;
    }
    
    public function getHistory($loanid = null,$customerid = null){
        $this->db->select('*');
        $this->db->where('loanID',$loanid);
        $this->db->where('cID',$customerid);
        $this->db->where('return_money != ',0,false);
        $result = $this->db->get(self::_tablename_loans_return)->result();
        return $result;
    }
    
    public function getAllListBranch(){
        $this->db->select('*');
        $result = $this->db->get(self::_tablename_branch)->result();
        return $result;
    }
    
    public function getListBranchByUserID($userID = null){
        $this->db->select('*');
        if($userID){
            $this->db->where('userID',$userID);
        }
        $result = $this->db->get(self::_tablename_branch)->result();
        return $result;
    }
    
    public function getBranchDefaultByUserID($userID = null,$isdefault = 1){
        $this->db->select('*');
        if($userID){
            $this->db->where('userID',$userID);
            $this->db->where('isdefault',$isdefault);
        }
        $result = $this->db->get(self::_tablename_branch)->result();
        return $result;
    }
}