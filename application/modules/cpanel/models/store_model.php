<?php
class Store_model extends CI_Model {

    const _tablename        = 'ttp_store';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getStoreInfo($id = ""){
        $this->db->select('*');
        if($id != ""){
            $this->db->where("UserID", $id);
        }
        $result = $this->db->get(self::_tablename)->row();
        return $result;
    }
        
    public function update($id,$data){
        $this->db->where("UserID", $id);
        return $this->db->update(self::_tablename, $data);
    }

}