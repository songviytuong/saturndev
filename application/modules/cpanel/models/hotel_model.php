<?php
class Hotel_model extends CI_Model {

    const _tablename        = 'tbl_hotels';
    const _tablename_city   = 'tbl_city';
    const _tablename_star   = 'tbl_star';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getHotels()
    {
        $this->db->select('*,hotel.title as title,hotel.address as address,city.name as cityName,star.star as starIcon');
        $this->db->from('tbl_hotels hotel');
        $this->db->join('tbl_city city', 'hotel.city_code = city.code', 'inner');
        $this->db->join('tbl_star star', 'hotel.star_code = star.code', 'inner');
        $res = $this->db->get()->result();
        return $res;
    }
    
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update(self::_tablename, $data);
    }
    
    public function insert($data){
        $this->db->insert(self::_tablename,$data);
        return $this->db->insert_id();
    }

}