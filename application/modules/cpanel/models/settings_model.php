<?php
class Settings_model extends CI_Model {

    const _tablename        = 'ttp_setting';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getSettingInfo($id = "", $alias = "default_language") {
        $this->db->select('*'); 
        if ($id != "") {
            $where = "UserID=$id AND Alias='$alias'";
        }
        $this->db->where($where, NULL, FALSE);
        $result = $this->db->get(self::_tablename)->row();
        return $result;
    }

    public function update($id,$data){
        $this->db->where("UserID", $id);
        return $this->db->update(self::_tablename, $data);
    }

}