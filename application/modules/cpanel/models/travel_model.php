<?php
class Travel_model extends CI_Model {

    const _tablename        = 'ttp_modules_travel_tours';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getTours()
    {
        $this->db->select('*');
        $res = $this->db->get(self::_tablename)->result();
        return $res;
    }
    
    public function getToursByID($id)
    {
        $this->db->where("ID",$id);
        $this->db->select('Details');
        $res = $this->db->get(self::_tablename)->row();
        return $res->Details;
    }
    
    public function update($id,$data){
        $this->db->where("ID", $id);
        return $this->db->update(self::_tablename, $data);
    }
    
    public function insert($data){
        $this->db->insert(self::_tablename,$data);
        return $this->db->insert_id();
    }

}