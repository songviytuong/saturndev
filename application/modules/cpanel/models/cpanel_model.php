<?php
class Cpanel_model extends CI_Model {

    const _tablename            = 'ttp_modules';
    const _tablename_menutop    = 'ttp_menutop';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllModules($position = 'main_left', $order = 'ASC'){
        $this->db->select('*');
        $this->db->where('ModPosition',$position);
        $this->db->order_by('ModOrder',$order);
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
    
    function getListMenuTop($MenuParent = 0, $order_by = 'DESC'){
        $this->db->select('*');
        if($MenuParent != 0){
            $this->db->where('MenuParent',$MenuParent);
        }else{
            $this->db->where('MenuParent',0);
        }
        $this->db->order_by('MenuPosition',$order_by);
        $result = $this->db->get(self::_tablename_menutop)->result();
        return $result;
    }
}