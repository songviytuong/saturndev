<?php
class Templates_model extends CI_Model {

    const _tablename        = 'ttp_templates';
    const _tablename_cate   = 'ttp_templates_category';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getTemplates()
    {
        $this->db->select('*');
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
    
    public function getTemplatesCate()
    {
        $this->db->select('*');
        $result = $this->db->get(self::_tablename_cate)->result();
        return $result;
    }
    
    public function getListTemplatesByCateID($cid)
    {
        $this->db->select('*');
        $this->db->where('CateID',$cid);
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
    
    public function getTemplatesActiveByUser($uid)
    {
        $this->db->select('t.ID as TempID');
        $this->db->from('ttp_templates t');
        $this->db->join('ttp_store s','s.TempID = t.ID');
        $this->db->where('s.UserID',$uid);
        $result = $this->db->get()->row();
        return $result;
    }
    
    public function update($id,$data){
        $this->db->where("ID", $id);
        return $this->db->update(self::_tablename, $data);
    }
    
    public function insert($data){
        $this->db->insert(self::_tablename,$data);
        return $this->db->insert_id();
    }

}