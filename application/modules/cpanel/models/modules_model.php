<?php
class Modules_model extends CI_Model {

    const _tablename        = 'ttp_modules';
    const _sub_tablename    = 'ttp_modules_sub';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getAllModules(){
        $this->db->select('*');
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
    
    function getAllSubModules($module_alias = null, $orderby = "ASC"){
//        var_dump($module_alias);
        if($module_alias != null){
            $this->db->select('m.*,s.*');
            $this->db->from('ttp_modules m');
            $this->db->join('ttp_modules_sub s', 'm.ModID = s.ModID', 'inner');
            $this->db->where('m.ModAlias',$module_alias); 
            $this->db->order_by('s.SubPosition', $orderby);
        }else{
            $this->db->select('*');
            $this->db->from(self::_sub_tablename);
        }
        $result=$this->db->get()->result();
        return $result;
    }
    
    function get_order_status($group,$type,$order = null,$by = null)
    {
        if($order == null){$order = "name";}
        if($by == null){$by = "asc";}
        
        $this->db->select('code, name');
        $this->db->where('group',$group); 
        $this->db->where('type',$type);
        $this->db->order_by($order, $by);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }
    
    function get_list_by_group($code = null){
        if($code){
            $this->db->select('*');
            $this->db->where('group',$code); 
            $this->db->order_by('name', 'asc'); 
            $result = $this->db->get($this->tablename)->result();
            if($result != null)
            return $result;
        }
        return false;
    }

}