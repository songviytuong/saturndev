<?php
class Travel extends CI_Controller {
    
    public $user;
    public $classname="cpanel";
    
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        
        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        $this->load->library('template');
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
//        $this->template->add_js("public/authen/6ede73fb6e204f0d1ba850a2db67eb65.js");
//        $this->template->add_js("public/cpanel/js/script.js");
        
        $this->template->add_css("public/cpanel/css/custom.css");
        $this->template->add_doctype();
        
        #BEGIN: VERIFY TOKEN
        if(isset($_GET["_token"]) != substr(sha1($this->session->userdata('_token')),TOKENF,TOKENT)){
            //redirect('/authen/logout');
        }
    }
    
    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/travel/travel_content";
        $alias = "module_travel";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "travel";
        $this->template->add_title($this->lang->line('settings_global'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_title($this->lang->line('home'));
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    public function tours(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/travel/default";
        $alias = "module_travel";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "tours";
        $this->template->add_title($this->lang->line('tours'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_travel.js");
        $this->template->add_title($this->lang->line('home'));
        $this->template->write_view('content',$view);
        $this->template->render();
    }

    #BEGIN: LOAD TOURS
    public function loadToursAjax(){
        $this->load->model("travel_model","tra");
        $listTour = $this->tra->getTours();
        $listTour = ($listTour) ? $listTour : array();
        $data["result"] = $listTour;
        $data["edithref"] = base_url().ADMINROOT."/travel/edit";
        
        
        $data["edittext"] = "<i class='fa fa-edit'></i> ".$this->lang->line('btn_edit');
        $data["deletetext"] = "<i class='fa fa-trash-o'></i> ".$this->lang->line('btn_remove');
        $data["disabled_edit"] = "";
        $data["disabled_delete"] = "";
        
        $view = "cpanel/page_ajax/load_tours";
        $this->load->view($view,$data);
    }
        
    public function hotels(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/travel/default_hotels";
        $alias = "module_travel";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "hotels";
        $this->template->add_title($this->lang->line('hotels'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_hotel.js");
        $this->template->add_title($this->lang->line('home'));
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    #BEGIN: LOAD TOURS
    public function loadHotelsAjax(){
        $this->load->model("hotel_model","hotel");
        $listHotels = $this->hotel->getHotels();
        $listHotels = ($listHotels) ? $listHotels : array();
        $data["result"] = $listHotels;
        $data["edithref"] = base_url().ADMINROOT."/travel/edit";
        $data["edittext"] = "<i class='fa fa-edit'></i> ".$this->lang->line('btn_edit');
        $data["deletetext"] = "<i class='fa fa-trash-o'></i> ".$this->lang->line('btn_remove');
        $data["disabled_edit"] = "";
        $data["disabled_delete"] = "";
        $view = "cpanel/page_ajax/load_hotels";
        $this->load->view($view,$data);
    }
    
    public function edit(){
        $id = $this->uri->segment(4);
        $this->load->model("travel_model","tra");
        $result1 = $this->tra->getToursByID($id);
        $result = unserialize($result1);
        $data["edit"] = true;
        $data["editid"] = $id;
        $data["result"] = isset($result) ? $result : "";
        $data["base_link"] = base_url().ADMINROOT."/travel/";
        
        $view = "cpanel/travel/edit_tours";
        $this->load->view($view,$data);
    }
    
    public function edittours(){
        $data = array();
        $id = $this->uri->segment(4);
        if($id){
            $this->template->add_title("Edit Row");
            $edit = true;
            $this->load->model("travel_model","tra");
            $result1 = $this->tra->getToursByID($id);
            $result = unserialize($result1);
            $data["editid"] = $id;
        }
        
        
        $alias = "module_travel";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "tours";
        
        $view = "cpanel/travel/edit_tours";
        $this->template->add_title($this->lang->line('tours'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_title($this->lang->line('home'));
        $this->template->write_view('content',$view,$data);
        $this->template->render();
        
    }
    
    public function addNames(){
        $cnt = count($this->input->post('name',''));
        for($i=0;$i<$cnt;$i++){
            $res[$i]["name"] = $this->input->post('name','')[$i];
            $res[$i]["phone"] = $this->input->post('phone','')[$i];
        }
        $this->load->model("Other","oth");
        $data = array(
            'Names' => serialize($res)
        );
        echo $this->oth->insert($data);
    }

    public function updateNames(){
        var_dump($_REQUEST);
        $id = $this->input->post('id',0);
        $cnt = count($this->input->post('name',''));
        for($i=0;$i<$cnt;$i++){
            $res[$i]["tourDay"] = $this->input->post('tourDay','')[$i];
            $res[$i]["name"] = $this->input->post('name','')[$i];
            $res[$i]["phone"] = $this->input->post('phone','')[$i];
            $res[$i]["type"] = $this->input->post('type','')[$i];
        }
        $this->load->model("travel_model","tra");
        $data = array(
            'Details' => serialize($res)
        );
        $this->tra->update($id,$data);
        echo "OK";
    }
//
//    public function hotels(){
//        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
//        $view = "cpanel/travel/travel_content";
//        $alias = "module_travel";
//        $this->load->model("modules_model","mod");
//        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
//        $data_sitebar['nav_sub_active'] = "hotels";
//        $this->template->add_title($this->lang->line('hotels'));
//        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
//        $this->template->add_title($this->lang->line('home'));
//        $this->template->write_view('content',$view);
//        $this->template->render();
//    }
//    
}