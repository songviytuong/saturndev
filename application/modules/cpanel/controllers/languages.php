<?php
class Languages extends CI_Controller {
    
    public $user;
    public $classname="cpanel";
    
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        
        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        $this->load->library('template');
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
        $this->template->add_js("public/authen/dataTables.fixedColumns.min.js");
        $this->template->add_js("public/cpanel/js/script_language.js");
        $this->template->add_css("public/cpanel/css/custom.css");
        $this->template->add_css("public/cpanel/css/fixed/fixedColumns.bootstrap.scss");
        $this->template->add_css("public/cpanel/css/fixed/fixedColumns.bootstrap4.scss");
        $this->template->add_css("public/cpanel/css/fixed/fixedColumns.dataTables.scss");
        $this->template->add_css("public/cpanel/css/fixed/fixedColumns.foundation.scss");
        $this->template->add_css("public/cpanel/css/fixed/fixedColumns.jqueryui.scss");
        $this->template->add_css("public/cpanel/css/fixed/fixedColumns.semanticui.scss");
        $this->template->add_doctype();
    }
    #BEGIN: INDEX LANGUAGE
    public function index()
    {
        $lang = $this->input->get('lang');
        $urlReturn = $this->input->get('return');
        $this->config->set_item('language', $lang);
        $this->session->set_userdata('languageTT', $lang);
        redirect($urlReturn);
    }
    #BEGIN: INSERT LANGUAGE
    public function insertLanguage(){
        $exitsKeyword = $this->db->query("SELECT count(*) as count FROM ttp_languages_translate WHERE Keyword = '".$_REQUEST["keyword"]."'")->row();
        $exitsKeyword = $exitsKeyword->count;
        
        $rows = count($_REQUEST["id"]);
        if($exitsKeyword > 0){
            echo $this->lang->line("keyword_exists");
        }else{
            $res = array();
            for($i=0;$i<$rows;$i++){
                $res["LangID"]      = $_REQUEST["id"][$i];
                $res["Translate"]   = $_REQUEST["language"][$i];
                $res["Keyword"]     = $_REQUEST["keyword"];
                $res["TypeID"]      = $_REQUEST["optionsRadio"];
                $this->db->insert("ttp_languages_translate",$res);
            }
            echo "OK";
        }
    }
    
    #BEGIN: UPDATE LANGUAGE
    public function updateLanguage(){
        $translate = $_POST["translate"];
        $id = $_POST["id"];
        $updateLang = array(
            'Translate' => $translate
        );
        $this->db->where('ID',$id);
        $this->db->update('ttp_languages_translate',$updateLang);
        echo "OK";
    }
    
    public function updateActiveLanguage(){
        $status = $_POST["status"];
        $id = $_POST["id"];
        if($status == 1){
            $LangActive = 0;
        }else{
            $LangActive = 1;
        }
        $updateActiveLang = array(
            'LangActive' => $LangActive
        );
        $this->db->where('LangID',$id);
        if($this->db->update('ttp_languages',$updateActiveLang)){
            echo "OK";
        }
    }
    
    #BEGIN: ADD MORE LANGUAGE
    public function add_more_language(){
        $view = "cpanel/languages/add_more_language";
        $alias = "languages";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "add_more_language";
        $this->template->add_title($this->lang->line('add_more_language'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        #ADD_JS: VALIDATE FORM
        $this->template->add_js("public/authen/926fa5705d7bbe688c5b54d9d559f339.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    #BEGIN: UPDATE INSERT MORE LANGUAGE
    public function updateInsertMoreLanguage(){
        $newid = $_POST["LangID"];
        $ext = $this->db->query("select DISTINCT (Keyword) from ttp_languages_translate")->result();
        foreach($ext as $row){
            $data_more = array(
                'Keyword' => $row->Keyword,
                'LangID' => $newid
            );
            $this->db->insert('ttp_languages_translate',$data_more);
        }
        echo "Done";
    }
    
    #BEGIN: LOAD LANGUAGE
    public function loadLanguages(){
        $view = "cpanel/page_ajax/load_languages";
        $this->load->view($view);
    }
    
    #BEGIN: LOAD LANGUAGE LIST
    public function loadLanguagesList(){
        #LOAD MODEL LANGUAGES
        $this->load->model("languages_model","lan");
        $ListLanguage = $this->lan->getLanguages();
        $data["result"] = $ListLanguage;
        $view = "cpanel/page_ajax/load_languages_list";
        $this->load->view($view,$data);
    }
    
    #BEGIN: ADD MORE LANGUAGE
    public function translate(){
        
        $view = "cpanel/languages/languages_translate";
        $alias = "languages";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "translate";
        $this->template->add_title($this->lang->line('translate'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
        $this->template->add_js("public/authen/0ecdc2a1f21af0053c8e1d78bc57a41a.js");
        #ADD_JS: VALIDATE FORM
        $this->template->add_js("public/authen/926fa5705d7bbe688c5b54d9d559f339.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
}