<?php 
class Cpanel extends CI_Controller { 
    public $user;
    public $classname="cpanel";
    
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        
        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        
        $this->load->library('template'); 
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
        $this->template->add_js("public/cpanel/js/script.js");
        $this->template->add_css("public/cpanel/css/custom.css");
        $this->template->add_doctype();
        
    }
    #BEGIN: INDEX
    public function index(){
        
        #echo "cpanel index";
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/main_content";
        $this->template->write_view('subsitebar','cpanel/main_sub_sidebar',array('user'=>$this->user));
        $this->template->add_title($this->lang->line('home'));
        $this->template->add_js("public/authen/6ede73fb6e204f0d1ba850a2db67eb65.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    #BEGIN: DASHBOARD
    public function dashboard(){
        #echo "cpanel index";
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/main_content";
        $this->template->write_view('subsitebar','cpanel/main_sub_sidebar',array('user'=>$this->user));
        $this->template->add_title($this->lang->line('home'));
        $this->template->add_js("public/authen/6ede73fb6e204f0d1ba850a2db67eb65.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    #BEGIN: RENDER POSTS FACEBOOK
    private function renderposts($facebookdata){
        $posts = array();
        foreach($facebookdata['posts']['data'] as $status)
        {
            $posts[]= $status;
        }
        return $posts;
    }
    
    public function posts($fbid){
        $this->load->library('facebook');
        $user = $this->facebook->request('get', '/'.$fbid.'?fields=posts{id,picture,full_picture,message}');
        try{
            $posts = $this->renderposts($user);
            $data["posts"] = $posts;
        }
        catch(FacebookApiException $e)
        {
            $login_url = $this->facebook->getLoginUrl(array(
                'scope' => 'read_stream'
            ));
            echo 'Please <a href="' . $login_url . '">login.</a>';
            error_log($e->getType());
            error_log($e->getMessage());
        }
    }
    
    #BEGIN: Hide SubMenu
    public function hideside(){
        if($this->session->userdata('submenu') == false){
            $this->session->set_userdata('submenu','hide-side-menu');
        }else{
            $this->session->unset_userdata('submenu');
        }
    }
}
?>