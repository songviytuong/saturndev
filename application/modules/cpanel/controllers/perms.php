<?php
class Perms extends CI_Controller {

    public $user;
    public $classname="cpanel";
    
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session, $this->classname);

        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        $this->load->library('template');
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
    }
    
    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/perms/perms_content";
        $alias = "perms";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "perms";
        $this->template->add_title($this->lang->line('settings_global'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_title($this->lang->line('home'));
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    public function groups(){
        die("a");
    }
    
    public function permission(){
        die("a");
    }

}
