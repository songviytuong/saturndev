<?php
class Settings extends CI_Controller {
    
    public $user;
    public $classname="cpanel";
    
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        
        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        
        $this->load->library('template');
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
        $this->template->add_js("public/cpanel/js/script_settings.js");
        $this->template->add_css("public/cpanel/css/custom.css");
        $this->template->add_doctype();
    }
    
    public function index(){
        $this->lib->check_permission($this->user->DetailRole,"setting",'r',$this->user->IsAdmin);
        #BEGIN: Load style
        $this->load->library('folder');
        $data['style'] = $this->folder->load('templates/store');
        #BEGIN: Display Currency
        $data['currency'] = $this->currency();
        
        $this->load->model("templates_model","temp");
        $data['TemplateCate'] = $this->temp->getTemplatesCate();
        #BEGIN: View
        $view = "cpanel/settings/settings_content";
        $this->template->add_title($this->lang->line('store_settings'));
        $this->template->write_view('subsitebar','cpanel/main_sub_sidebar',array('user'=>$this->user));
        $this->template->add_js("public/cpanel/js/script_shop.js");
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    public function globals(){
        $this->lib->check_permission($this->user->DetailRole,"setting",'r',$this->user->IsAdmin);
        #BEGIN: Load style
        $this->load->library('folder');
        $data['style'] = $this->folder->load('templates/store');
        #BEGIN: Display Currency
        $data['currency'] = $this->currency();
        #BEGIN: View
        $view = "cpanel/settings/settings_globals";
        $alias = "settings";
        $this->load->model("modules_model","mod");
        $data_sitebar['nav_sub_active'] = $this->uri->segment(3);
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $this->template->add_title($this->lang->line('settings_global'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/authen/54af62862bafb8d935ed7facd521918f.js");
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    public function color(){
        echo "Color";
    }
    
    public function size(){
        echo "Size";
    }
    
    #CURRENCY UPDATE
    public function currency(){
        
        #GET CURRENCY FROM DATA
        $setting = $this->db->query("SELECT value,updated FROM ttp_setting WHERE Alias = 'currency'")->row();
        $date = strtotime($setting->updated);
        $dateone = strtotime("+1 day", $date);
        $now = time();
        if(!($setting->value) || $now >= $dateone){
            $this->load->helper('dom');
            $link = "https://www.google.com/finance/converter?a=1&from=USD&to=VND";
            $html = file_get_html($link);
            $contents = $html->find("span.bld");
            $set_currency[] = "USD";
            $set_currency[] = "VND";
            foreach ($contents as $t){
                $str = $t->innertext;
                $str = substr($str,0,5);
                $set_currency[] = $str;
            }
            $updateData = array(
                'value' => serialize($set_currency),
                'updated' => date('Y-m-d h:i:s',time())
            );
            $this->db->where('Alias','currency');
            $this->db->update('ttp_setting',$updateData);
        }
        $get_currency = unserialize($setting->value);
        $get_currency = "1 ".$get_currency[0]." = ". number_format($get_currency[2]) . " " . $get_currency[1];
        $get_currency = $get_currency." <small><i>( ".$this->lang->line('updated')." ".$setting->updated." )</i></small>";
        return $get_currency;
        
    }
    
}