<?php
class Stores extends CI_Controller {
    
    public $user;
    public $classname="cpanel";
    
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session,$this->classname);

        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        
        $this->load->library('template');
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
        $this->template->add_js("public/cpanel/js/script_store.js");
        $this->template->add_css("public/cpanel/css/custom.css");
        $this->template->add_doctype();
    }
    
    public function index(){
        
        #DEFINE: /cpanel/stores
        
        $this->lib->check_permission($this->user->DetailRole,"setting",'r',$this->user->IsAdmin);
        #BEGIN: Load style
        $this->load->library('folder');
        $data['style'] = $this->folder->load('templates/store');
        #BEGIN: Display Currency
        $data['currency'] = $this->currency();
        
        #BEGIN: TemplateCate List
        $this->load->model("templates_model","temp");
        $data['TemplateCate'] = $this->temp->getTemplatesCate();
        
        #BEGIN: Language List
        $this->load->model("languages_model","_lang");
        $data['LanguageList'] = $this->_lang->getLanguages();
        
        #BEGIN: GET SETTING INFO
        $this->load->model("settings_model","settings");
        $default_language = $this->settings->getSettingInfo($this->user->ID);
        $default_language = isset($default_language) ? $default_language->Value : DEFAULT_LANGUAGE_ID;
        $data["default_language"] = $default_language;
        
        #BEGIN: Load Model Store
        $this->load->model("store_model","store");
        $StoreInfo = $this->store->getStoreInfo($this->user->ID);
        if($StoreInfo){
            $data["StoreName"] = unserialize($StoreInfo->StoreName);
            $data["Slogan"] = unserialize($StoreInfo->Slogan);
            $data["Intro"] = unserialize($StoreInfo->Intro);
            $data["SloganActive"] = ($StoreInfo->SloganActive == 1) ? "checked" : "";
        }else{
            redirect('/cpanel');
        }
        
        #BEGIN: Language Active User
        $Language = unserialize($StoreInfo->LanguageID);
        $data["LangSelected"] = $Language;
        $data['ListLanguageInSetting'] = $this->_lang->getLangIconByStore($Language);
        #BEGIN: View
        $view = "settings/settings_content_multilingual";
        $alias = "stores";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "stores";
        
        $this->template->add_title($this->lang->line('store_settings'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_store.js");
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    #BEGIN: CURRENCY UPDATE
    public function currency(){
        
        #GET CURRENCY FROM DATA
        $setting = $this->db->query("SELECT value,updated FROM ttp_setting WHERE Alias = 'currency'")->row();
        $date = strtotime($setting->updated);
        $dateone = strtotime("+1 day", $date);
        $now = time();
            
        if (!($setting->value) || $now >= $dateone) {
            $this->load->helper('dom');
            $link = "https://www.google.com/finance/converter?a=1&from=USD&to=VND";
            $html = file_get_html($link);
            $contents = $html->find("span.bld");
            $set_currency[] = "USD";
            $set_currency[] = "VND";
            foreach ($contents as $t) {
                $str = $t->innertext;
                $str = substr($str, 0, 5);
                $set_currency[] = $str;
            }
            $updateData = array(
                'value' => serialize($set_currency),
                'updated' => date('Y-m-d h:i:s', time())
            );
            $this->db->where('Alias', 'currency');
            $this->db->update('ttp_setting', $updateData);
        }
        $get_currency = unserialize($setting->value);
        $get_currency = "1 " . $get_currency[0] . " = ? " . $get_currency[1];
        $get_currency = $get_currency . " <small><i>( " . $this->lang->line('updated') . " " . $setting->updated . " )</i></small>";
        return $get_currency;
    }

    #BEGIN: CONFIG STORE UPDATE TEMPLATE LAYOUT
    public function saveLayout(){
        #BEGIN: GET SETTING INFO
        $this->load->model("settings_model","settings");
        $default_language = $this->settings->getSettingInfo($this->user->ID);
        $default_language = isset($default_language) ? $default_language->Value : DEFAULT_LANGUAGE_ID;
        
        #Update: Languages & Templates
        $arrLanguage = isset($_REQUEST["cboLanguage"]) ? $_REQUEST["cboLanguage"] : array("$default_language");
        $arr = array();
        if(!in_array(DEFAULT_LANGUAGE_ID, $arrLanguage)){
            $arr = array_merge($arrLanguage,array("$default_language"));
        }else{
            $arr = $arrLanguage;
        }
        
        $updateData = array(
            'TempID' => $_REQUEST["cboTemplate"],
            'LanguageID' => serialize($arr)
        );
        $res = array();
        $this->load->model("store_model","store");
        if($this->store->update($this->user->ID,$updateData)){
            $res["status"] =  "OK";
            $res["message"] = $this->lang->line("updated_successfully");
        }else{
            $res["message"] = $this->lang->line("updated_false");
        }
        echo json_encode($res);
    }
    
    #BEGIN: CONFIG STORE UPDATE STORE
    public function saveSettingStore(){
        
        $store_name = isset($_REQUEST["store_name"]) ? $_REQUEST["store_name"] : "";
        $slogan = isset($_REQUEST["slogan"]) ? $_REQUEST["slogan"] : "";
        $intro = isset($_REQUEST["intro"]) ? $_REQUEST["intro"] : "";
        $slogan_active = isset($_REQUEST["optActiveSlogan"]) ? $_REQUEST["optActiveSlogan"] : "off";
        
        $_active = ($slogan_active == "on") ? 1:0;
        
        $updateData = array(
            'StoreName' => serialize($store_name),
            'Slogan' => serialize($slogan),
            'Intro' => serialize($intro),
            'SloganActive' => $_active,
            
        );
        $res = array();
        $this->load->model("store_model","store");
        if($this->store->update($this->user->ID,$updateData)){
            $res["status"] =  "OK";
            $res["message"] = $this->lang->line("updated_successfully");
        }else{
            $res["message"] = $this->lang->line("updated_false");
        }
        echo json_encode($res);
    }
    
}