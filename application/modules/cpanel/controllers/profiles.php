<?php
class Profiles extends CI_Controller {
    
    public $user;
    public $classname="cpanel";
    
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        
        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        
        $this->load->library('template');
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
        $this->template->add_js("public/authen/6ede73fb6e204f0d1ba850a2db67eb65.js");
        $this->template->add_js("public/cpanel/js/script.js");
        $this->template->add_js("public/cpanel/js/script_profiles.js");
        $this->template->add_css("public/cpanel/css/custom.css");
        $this->template->add_doctype();
    }
    
    public function index(){
        if($this->user->FacebookID){
            $path = 'public/authen/avatar/';
            $file = $path . $this->user->FacebookID . '.jpg';
            if (file_exists($file)) {

            } else {
                $fileThumb = $this->user->Thumb;
                if (file_exists($fileThumb)) {

                }else{
                    $this->load->library('facebook');
                    $user = $this->facebook->request('get', '/me');
                    if ($user) {
                        $img = file_get_contents('https://graph.facebook.com/' . $user["id"] . '/picture?type=large');
                        $path = 'public/authen/avatar/';
                        $file = $path . $user["id"] . '.jpg';
                        file_put_contents($file, $img);

                        /* Update Avata Users */
                        $data = array(
                            'Thumb' => $file
                        );
                        $this->db->where('FacebookID', $user["id"]);
                        $this->db->update('ttp_user', $data);
                    } else {

                    }
                }
            }
        }
        $view = "cpanel/profiles";
        $alias = "profiles";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "profiles";
        $this->template->add_title($this->lang->line('profiles'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/authen/54af62862bafb8d935ed7facd521918f.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    public function contact(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/travel_content";
        $alias = "profiles";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "contact";
        $this->template->add_title($this->lang->line('contact'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_title($this->lang->line('home'));
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    public function change_password(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/travel_content";
        $alias = "profiles";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "change_password";
        $this->template->add_title($this->lang->line('change_password'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_title($this->lang->line('home'));
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
}