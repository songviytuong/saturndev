<?php
class Loans extends CI_Controller {
    
    public $user;
    public $classname="cpanel";
    
    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        
        #BEGIN: LOAD LANGUAGE
        $this->cur_lang = "english";
        $langs = $this->session->userdata('languageTT');
        if ($langs && $langs !== "") {
            $langsite = $langs;
        } else {
            $langsite = $this->config->item('language');
        }
        $this->cur_lang = $langsite;
        $this->lang->load('translates', $this->classname);
        #BEGIN: END LANGUAGE
        
        $this->load->model('cpanel_model','cpa');
        $dataSiteBar['sitebar'] = $this->cpa->getAllModules('main_left');
        $dataSiteBar['nav_main_active'] = $this->uri->segment(2);
        
        $this->load->library('template');
        $this->template->set_template('cpanel');
        $this->template->write_view('sitebar','cpanel/main_sidebar',$dataSiteBar);
        $this->template->write_view('header','cpanel/main_header',array('user'=>$this->user));
        $this->template->write_view('footer','cpanel/main_footer',array('user'=>$this->user));
        $this->template->add_js("public/authen/ad67372f4b8b70896e8a596720082ac6.js");
        $this->template->add_js("public/cpanel/js/script_settings.js");
        $this->template->add_css("public/cpanel/css/custom.css");
        $this->template->add_doctype();
    }
    
    public function index(){
        $this->lib->check_permission($this->user->DetailRole,"setting",'r',$this->user->IsAdmin);
        $alias = "module_loans";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "loans";
        #BEGIN: View
        $view = "cpanel/loans/loans_content";
        $this->template->add_title($this->lang->line('module_loans'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    #BEGIN: NEW CUSTOMER FORM
    public function new_customer(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/loans/new_customer";
        
        $alias = "module_loans";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "new_customer";
        
        $this->load->model("loans_model","loans");
        $dataMembers = $this->loans->getMembers();
        $data["old_customer"] = $this->session->userdata('old_customer');
        $data["dataMembers"] = $dataMembers;
        
        $this->template->add_title($this->lang->line('new_customer'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_loans.js");
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    #BEGIN: ADD CUSTOMER
    public function addCustomerInfo() {
        $dataAdd = array(
            'fullname' => $_REQUEST["fullname"],
            'address' => $_REQUEST["address"],
            'phone' => $_REQUEST["phone"],
            'cmnd' => $_REQUEST["cmnd"],
            'userID' => $this->user->ID,
        );
        $this->load->model("loans_model", "loans");
        $result = $this->loans->insertCustomerInfo($dataAdd);
        $res = array();
        if ($result > 0) {
            $res["status"] = "OK";
            $old_customer = $_REQUEST["phone"];
            $this->session->set_userdata('old_customer', $old_customer);
            $res["old_customer"] = $this->session->userdata('old_customer');
        } else {
            $res["status"] = "NO";
        }
        echo json_encode($res);
    }

    public function addNewMember(){
        $res = array();
        $this->load->model("loans_model","loans");
        $dataAddBranch = array(
            'name' => $_REQUEST["branch_name"],
            'cityID' => $_REQUEST["cityID"],
            'userID' => $this->user->ID,
            'isdefault' => 0,
        );
        $branchID = $this->loans->insertBranch($dataAddBranch);
        if($branchID > 0){
                $dataAddMember = array(
                'fullname' => $_REQUEST["fullname"],
                'phone' => $_REQUEST["phone"],
                'nickname' => $_REQUEST["nickname"],
                'address' => $_REQUEST["address"],
                'branchID' => $branchID,
                'ispartner' => $_REQUEST["branchID"],
            );
            $result = $this->loans->insertMember($dataAddMember);
            $res["status"] = "OK";
            $res["message"] = "Thêm mới thành công";
        }else{
            $res["error"] = "Không thêm mới được";
        }
        echo json_encode($res);
    }
    
    public function addNewMoney(){
        $res = array();
        $this->load->model("loans_model","loans");
        $add_money = (int)str_replace(",", "", $_REQUEST["add_money"]);
        $dataAddMoney = array(
            'add_money' => $add_money,
            'add_source' => $_REQUEST["add_source"],
            'add_date' => date('Y-m-d h:i:s',time()),
            'add_type' => $_REQUEST["add_type"],
            'add_package' => $_REQUEST["add_package"],
            'userID' => $this->user->ID,
            'del_flag' => 0,
        );
        $MoneyID = $this->loans->insertMoney($dataAddMoney);
        if($MoneyID > 0){
            $res["status"] = "OK";
            $res["message"] = "Thêm mới thành công";
        }else{
            $res["error"] = "Không thêm mới được";
        }
        echo json_encode($res);
    }
    
    public function editMoneyAction(){
        $res = array();
        $this->load->model("loans_model","loans");
        $add_money = (int)str_replace(",", "", $_REQUEST["add_money"]);
        $id = $_REQUEST["id"];
        $dataEditMoney = array(
            'add_money' => $add_money,
            'add_source' => $_REQUEST["add_source"],
            'add_date_updated' => date('Y-m-d h:i:s',time()),
            'add_type' => $_REQUEST["add_type"],
            'add_package' => $_REQUEST["add_package"],
            'userID' => $this->user->ID,
            'del_flag' => 0,
        );
        $token = $_REQUEST["token"];
        $mytoken = substr(md5($_REQUEST["id"]),TOKENF,TOKENT);
        if($token == $mytoken){
            $Edited = $this->loans->updateMoney($id,$dataEditMoney);
            if($Edited){
                $res["status"] = "OK";
                $res["message"] = "Cập nhật thành công";
            }else{
                $res["error"] = "Không thực hiện được";
            }
        }else{
            $res["error"] = "Hacker !!!";
        }
        echo json_encode($res);
    }
    
    public function onOffBranch(){
        $status = ($_POST["status"] == 1) ? 0 : 1;
        $id = $_POST["id"];
        $token = $_POST["token"];
        $mytoken = substr(md5($id . "onOffBranch"), TOKENF, TOKENT);
        if ($mytoken == $token) {
            $dataBranchUpdate = array(
                'active' => $status
            );
            $this->load->model("loans_model","loans");
            $this->loans->updateBranch($id,$dataBranchUpdate);
        }
        echo $status;
    }
    
    public function addMoreTimer(){
        $value = $_POST["value"];
        $this->load->model("conf_define","define");
        $result = $this->define->get_config_define("loans","timer","","",$value);
        if(count($result) == 0){
            $dataTimerAdd = array(
            'code' => $value,
            'value' => $value,
            'name' => $value." ngày",
            'group' => "loans",
            'type' => "timer",
            'created' => date('Y-m-d h:i:s',time())
            );
            if($this->define->insert_config_define($dataTimerAdd) > 0){
                echo "OK";
            }
        }
    }
    
    public function loadTimer(){
        $this->load->model("conf_define","define");
        $timer = $this->define->get_config_define("loans","timer","value","asc");
        $html = "";
        foreach($timer as $item){
            $html .= "<option value=".$item->code.">".$item->name."</option>";
        }
        echo $html;
    }
    
    public function verifyMoneyEdit(){
        $id = $_POST["id"];
        $token = $_POST["token"];
        $mytoken = substr(md5($id . "verifyMoneyEdit"), TOKENF, TOKENT);
        $res = array();
        if ($mytoken == $token) {
            $dataMoneyUpdate = array(
                'status' => 1
            );
            $this->load->model("loans_model","loans");
            $this->loans->updateMoneyStatus($id,$dataMoneyUpdate);
            $res["status"] = "OK";
        }
        echo json_encode($res);
    }
    
    public function deleteMoneyAdd(){
        $id = $_POST["id"];
        $token = $_POST["token"];
        $mytoken = substr(md5($id . "deleteMoneyAdd"), TOKENF, TOKENT);
        $res = array();
        if ($mytoken == $token) {
            $this->load->model("loans_model","loans");
            $this->loans->deleteMoney($id);
            $res["status"] = "OK";
        }
        echo json_encode($res);
    }
    
    public function verifyMoney(){
        $package_id = $_POST["id"];
        $userID = $this->user->ID;
        $money_data = $this->db->query("SELECT sum(add_money) as total FROM m_money_details WHERE add_package = $package_id and userID = $userID and del_flag = 0 and status = 1")->row();
        $money_data = isset($money_data) ? $money_data->total : 0;
        $res = array();
        if ($money_data > 0) {
            $after_money = $this->db->query("SELECT sum(loan_money) as total_loan FROM m_loans WHERE branchID = $package_id and userID = $userID")->row();
            $after_money = isset($after_money) ? $after_money->total_loan : 0;
            $money_data = $money_data - $after_money;
            if ($money_data > 0) {
                $res["status"] = "OK";
                $res["total"] = $money_data;
                $branch = $this->db->query("SELECT name FROM m_branch WHERE id = $package_id")->row();
                $branch_name = ($branch) ? $branch->name : "";
                $res["branchname"] = $branch_name;
            }
        } else {
            $res["total"] = "";
        }
        echo json_encode($res);
    }
    
    public function loadHistory(){
        $loan_id = $_GET["loan"];
        $customer_id = $_GET["customer"];
        $this->load->model("loans_model","loans");
        $History = $this->loans->getHistory($loan_id,$customer_id);
        $data["result"] = $History;
        $view = "cpanel/page_ajax/load_history";
        $this->load->view($view,$data);
    }
    public function saveLoansDetails(){
        $res = array();
        $fee_money = (int)str_replace(",", "", $_REQUEST["fee_money"]);
        $loan_money = (int)str_replace(",", "", $_REQUEST["loan_money"]);
        $return_money = ($fee_money * (int)$_REQUEST["after_date"]) + $loan_money;
        $dataAdd = array(
            'title' => $_REQUEST["title"],
            'notes' => $_REQUEST["notes"],
            'loan_money' => $loan_money,
            'return_money' => $return_money,
            'after_date' => (int)$_REQUEST["after_date"],
            'fee_money' => $fee_money,
            'verify_by' => (int)$_REQUEST["branchID"],
            'branchID' => (int)$_REQUEST["branchID"],
            'verify_notes' => $_REQUEST["verify_notes"],
            'cID' => (int)$_REQUEST["cID"],
            'created' => date('Y-m-d h:i:s',time()),
            'modified' => date('Y-m-d h:i:s',time()),
            'userID' => $this->user->ID,
        );
        $this->load->model("loans_model", "loans");
        $return_id = $this->loans->insertLoans($dataAdd);
        if($return_id > 0){
            /* Insert default value m_loans_details
             * loanID = $return_id
             * cID = $_REQUEST["cID"]
             */
            $defaultInsert = array(
                'loanID' => $return_id,
                'cID' => $_REQUEST["cID"],
                'return_date' => date('Y-m-d h:i:s',time()),
                'return_money' => 0,
                'return_notes' => 'Lấy tiền',
                'verify_by' => 1,
                'userID' => $this->user->ID
            );
            if($this->loans->insertReturn($defaultInsert)){
                $res["status"] = "OK";
                $res["message"] = "Thêm mới thành công";
            }
        }else{
            $res["status"] = "NO";
            $res["message"] = "Không thêm mới được";
        }
        echo json_encode($res);
    }
    
    #BEGIN: LIST CUSTOMER
    public function list_customer(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/loans/list_customer";
        $alias = "module_loans";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "list_customer";
        
        $this->load->model("loans_model","loans");
        $dataMembers = $this->loans->getMembers();
        $data["dataMembers"] = $dataMembers;
        
        $this->template->add_title($this->lang->line('list_customer'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_loans.js");
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    #BEGIN: RETURN MONEY
    public function return_money(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/loans/return_money";
        $alias = "module_loans";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "return_money";
        $this->template->add_title($this->lang->line('return_money'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_loans.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    public function loadReturnMoneyList(){
        $this->load->model("loans_model","loans");
        $ListMoneyReturn = $this->loans->getReturnMoneyList($this->user->ID);
        $data["result"] = $ListMoneyReturn;
        $data["cur_link"] = "cpanel/loans/return_money/";
        $view = "cpanel/page_ajax/load_return_money_list";
        $this->load->view($view,$data);
    }
    
    #BEGIN: LOAD CUSTOMERS
    public function loadCustomerList(){
        $this->load->model("loans_model","loans");
        $ListCustomers = $this->loans->getCustomers();
        $data["result"] = $ListCustomers;
        $data["cur_link"] = "/cpanel/loans/list_customer/";
        $data["new_customer_link"] = "/cpanel/loans/new_customer/";
        $view = "cpanel/page_ajax/load_customer_list";
        $this->load->view($view,$data);
    }
    
    #BEGIN: LIST CUSTOMER
    public function list_member(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/loans/list_member";
        $alias = "module_loans";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "list_member";
        $this->template->add_title($this->lang->line('list_member'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_loans.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    #BEGIN: LIST MONEY
    public function list_money(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $view = "cpanel/loans/list_money";
        $alias = "module_loans";
        $this->load->model("modules_model","mod");
        $data_sitebar['data_sub_sitebar'] = $this->mod->getAllSubModules($alias);
        $data_sitebar['nav_sub_active'] = "list_money";
        $this->template->add_title($this->lang->line('list_money'));
        $this->template->write_view('subsitebar','cpanel/modules_sub_sidebar',$data_sitebar);
        $this->template->add_js("public/cpanel/js/script_loans.js");
        $this->template->write_view('content',$view);
        $this->template->render();
    }
    
    public function loadDetailsMoneyOfMonth(){
        $year = isset($_POST["year"]) ? $_POST["year"] : date('Y');
        $data = array(
            "year" => $year
        );
        $view = "cpanel/page_ajax/load_money_of_month";
        $this->load->view($view,$data);
    }
    
    #BEGIN: LOAD MEMBERS
    public function loadMemberList(){
        $this->load->model("loans_model","loans");
        $ListMembers = $this->loans->getMembersDetail();
        $data["result"] = $ListMembers;
        $data["cur_link"] = "cpanel/loans/list_member/";
        $view = "cpanel/page_ajax/load_member_list";
        $this->load->view($view,$data);
    }
    
    public function loadPersonalList(){
        $mID = $_POST["id"];
        $this->load->model("loans_model","loans");
        $ListMembersNS = $this->loans->getPersonalList($mID);
        $data["resultNS"] = $ListMembersNS;
        $data["cur_link"] = "cpanel/loans/list_member/";
        $view = "cpanel/page_ajax/load_member_ns_list";
        $this->load->view($view,$data);
    }
    
    public function loadMoneyList(){
        $this->load->model("loans_model","loans");
        $ListMoney = $this->loans->getMoneyList($this->user->ID);
        $data["result"] = $ListMoney;
        $data["cur_link"] = "cpanel/loans/list_money/";
        $view = "cpanel/page_ajax/load_money_list";
        $this->load->view($view,$data);
    }
    
    public function loadEditMoney(){
        $id = $_GET["id"];
        $token = $_GET["token"];
        $mytoken = substr(md5($id),TOKENF,TOKENT);
        $this->load->model("loans_model","loans");
        $result = $this->loans->getMoneyList($this->user->ID,$id);
        if(count($result) > 0 && $token == $mytoken){
            $dataEdit = array(
                'data' => $result
            );
            $view = "cpanel/page_ajax/load_edit_money";
            $this->load->view($view,$dataEdit);
        }
    }
    
    public function setSessionRedirectTo(){
        $mytoken = substr(sha1($this->session->userdata('_token')),TOKENF,TOKENT);
        $phone = $_POST["phone"];
        $token = $_POST["_token"];
        $res = array();
        if($mytoken == $token){
            $this->session->set_userdata('old_customer', $phone);
            $res["status"] = "OK";
        }
        echo json_encode($res);
    }
    
    public function resetSessionOldCustomer(){
        $this->session->unset_userdata('old_customer');
        echo "OK";
    }
    
    public function doUploadCMND(){
        $this->load->model('loans_model','loans');
        
        $config['upload_path'] = 'public/loans/cmnd/';
        $config['allowed_types'] = 'jpg|JPG|jpeg|png|gif|PNG';
        $config['max_size'] = '3000';
        $config['max_width'] = '3000';
        $config['max_height'] = '3000';
        $config['overwrite'] = 'TRUE';
        $config['encrypt_name'] = 'TRUE';
        $new_name = time().$_FILES["userfile"]['name'];
        $config['file_name'] = $new_name;
        $this->load->library('upload', $config);
        if($this->upload->do_upload('userfile')){
            unlink('public/loans/cmnd/'.$this->loans->getCMNDCustByID($_REQUEST['cID']));
            $dataUpdate = array(
                'cmnd_src' => $new_name
            );
            $this->loans->updateCustomer($_REQUEST['cID'],$dataUpdate);
        }
        $token = substr(sha1($this->session->userdata('_token')),TOKENF,TOKENT);
        redirect('/cpanel/loans/new_customer/?_token='.$token);
    }
    
    public function setToday(){
        
    }
    
    public function loadLoansDetails(){
        $cID = $_POST["cID"];
        $token = $_POST["token"];
        $mytoken = substr(md5($cID),TOKENF,TOKENT);
        if($token == $mytoken){
            $this->load->model("loans_model","loans");
            $ListLoans = $this->loans->getLoansBycID($cID);
            $data["result"] = $ListLoans;
        }else{
            $data["result"] = array();
        }
        $view = "cpanel/page_ajax/load_loans_details";
        $this->load->view($view,$data);
    }
    
    public function addReturn(){
        $return_money = (int)str_replace(",", "", $_REQUEST["return_money"]);
        $dataid = $_REQUEST["dataid"];
        $dataidArr = explode("|", $dataid);
        
        $dataInsert = array(
            'loanID' => (int)$dataidArr[1],
            'cID' => (int)$dataidArr[0],
            'return_date' => date('Y-m-d h:i:s',time()),
            'return_money' => $return_money,
            'return_notes' => $_REQUEST["return_notes"],
            'verify_by' => $_REQUEST["verify_by"],         
            'userID' => $this->user->ID        
        );
        
        $res = array();
        $this->load->model("loans_model","loans");
        if($this->loans->insertReturn($dataInsert)){
            $res["status"] = "OK";
            $res["cID"] = (int)$dataidArr[0];
            $res["token"] = substr(md5((int)$dataidArr[0]),TOKENF,TOKENT);
        }
        echo json_encode($res);
    }
}