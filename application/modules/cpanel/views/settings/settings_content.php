<?php
    $this->load->view('common/breadcrumb');
?>
<div class="alert alert-warning alert-dismissable bottom-margin">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-exclamation-circle"></i> <?php echo $currency; ?>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="widget widget-green">
            <div class="widget-title">
                <div class="widget-controls">
                    <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                    <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
                    <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
                </div>
                <h3><i class="fa fa-cogs"></i><?php echo $this->lang->line('shop_boxes'); ?></h3>
            </div>
            <div class="widget-content">
                <form name="frmUpdateStore" id="frmUpdateStore" role="form">
                    <div class="row hidden">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Column</label>
                                <input type="text" class="form-control" placeholder="First Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Second Column</label>
                                <input type="text" class="form-control" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?php echo $this->lang->line('shop_name'); ?></label>
                        <input type="text" name="store_name" class="form-control" value="<?=$StoreName;?>" placeholder="<?php echo $this->lang->line('shop_name_input'); ?>...">
                    </div>
                    <div class="form-group">
                        <label><?php echo $this->lang->line('shop_slogan'); ?></label>
                        <label class="checkbox-inline pull-right">
                            <input type="checkbox" name="optActiveSlogan" id="optActiveSlogan" <?=$SloganActive;?>> <?php echo $this->lang->line('shop_slogan_active'); ?>
                        </label>
                        <input type="text" name="slogan" value="<?=$Slogan;?>"  class="form-control" placeholder="<?php echo $this->lang->line('shop_slogan_input'); ?>..."/>
                    </div>
                    
                    <div class="btn-group">
                        <a class="btn btn-primary btn-xs saveSettingStore"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_save'); ?></a>
                    </div>
                    <span class="showMessages_store"></span>
                </form>
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <form name="frmTemplate" id="frmTemplate">
            <div class="widget widget-red">
                <div class="widget-title">
                    <div class="widget-controls">
                        <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
                        <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
                        <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                        <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
                        <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-cog"></i><?php echo $this->lang->line('template_boxes'); ?></h3>
                </div>
                <div class="widget-content">
                    <div class="form-group">
                        <label for="language_list"><?php echo $this->lang->line('language_list'); ?></label>
                        <select data-placeholder="Your Favorite Language" name="cboLanguage[]" class="form-control chosen-select" multiple>
                            <?php 
                                foreach($LanguageList as $lang){
                            ?>
                            <option value="<?=$lang->LangID;?>" <?=(in_array($lang->LangID, $LangSelected)) ? 'selected':'';?>><?=$lang->LangName?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="templates_list"><?php echo $this->lang->line('templates_list'); ?></label>
                        <select data-placeholder="Your Favorite Layout" class="form-control chosen-select" name="cboTemplate">
                            <option value="-1"></option>
                            <?php
                            foreach ($TemplateCate as $cate) {
                                ?>
                                <optgroup label="<?php echo $this->lang->line($cate->alias); ?>">
                                    <?php
                                    $this->load->model("templates_model","temp");
                                    $TemplateList = $this->temp->getListTemplatesByCateID($cate->id);
                                    foreach ($TemplateList as $list) {
                                        $temp = $this->temp->getTemplatesActiveByUser($this->user->ID);
                                        ?>
                                        <option value="<?php echo $list->ID; ?>" <?= ($temp->TempID == $list->ID) ? 'selected' : '' ?>><?php echo $list->TempName; ?></option>
                                    <?php } ?>
                                </optgroup>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="btn-group">
                        <a class="btn btn-danger btn-xs"><i class="fa fa-search"></i><?php echo $this->lang->line('btn_view'); ?></a>
                        <a class="btn btn-primary btn-xs saveSettingLayout"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_save'); ?></a>
                    </div>
                    <span class="showMessages"></span>
                </div>
            </div>
        </form>
      </div>
</div>