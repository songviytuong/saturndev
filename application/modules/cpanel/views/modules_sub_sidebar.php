<div class="sub-sidebar-wrapper">
    <ul class="nav">
        <?php
        foreach ($data_sub_sitebar as $nav) {
            ?>
            <li <?= ($nav_sub_active == $nav->SubClassActive) ? "class='active'" : ""; ?>><a href="<?= base_url() . ADMINROOT . $nav->SubLink . "/?_token=" . substr(md5($this->session->userdata('_token')),TOKENF,TOKENT); ?>" title="<?= $nav->SubName; ?>" <?= ($nav_sub_active == $nav->SubClassActive) ? "class='current'" : ""; ?>><?= ($this->lang->line($nav->SubAlias)) ? $this->lang->line($nav->SubAlias) : $nav->SubName; ?></a></li>
        <?php } ?>
    </ul>
</div>