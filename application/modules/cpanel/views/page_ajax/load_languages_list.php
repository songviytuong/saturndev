<table class="table table-bordered table-hover datatable">
    <thead>
        <tr>
            <th class="col-lg-1 text-center">ID</th>
            <th class="col-lg-1"></th>
            <th>Lang Name</th>
            <th class="hidden-xs">Lang Code</th>
            <th class="text-center">Active</th>
            <th class="text-center">Sync <span id="load"></span></th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i = 0;
            foreach($result as $row){
                $q = $this->db->query("SELECT count(*) as cnt FROM ttp_languages_translate WHERE LangID = $row->LangID")->row();
                $sync = $q->cnt;
        ?>
        <tr>
            <td class="text-center"><?=$i+1;?></td>
            <td class="text-center"><img src="<?=base_url().$row->LangFlag;?>" width="20px;"/></td>
            <td><?=$row->LangName;?></td>
            <td class="hidden-xs"><?=$row->LangAlias;?></td>
            <td class="text-center" style="cursor: pointer;"><?=($row->LangActive == 1)? "<i class='ActiveLanguage fa fa-toggle-on fa-lg' rel='$row->LangActive' data-id='$row->LangID'></i>":"<i class='ActiveLanguage fa fa-toggle-off fa-lg' rel='$row->LangActive' data-id='$row->LangID'></i>";?></td>
            <td class="text-center" style="cursor: pointer;"><?=($sync == 0)? '<i class="fa fa-refresh" onclick="sync('.$row->LangID.')"></i>':'';?></td>
        </tr>
        <?php $i++;} ?>
    </tbody>
</table>

<script>
$(".datatable").dataTable( {
    "oLanguage": {
        "oPaginate": {
            "sNext": "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>",
        },
        sSearch: "",
        sLengthMenu: "_MENU_",
        sEmptyTable: "<?php echo $this->lang->line('sEmptyTable'); ?>",
        sInfo: "<?php echo $this->lang->line('sInfo'); ?>",
        sInfoEmpty: "",
        sInfoFiltered: "(<?php echo $this->lang->line('sInfoFiltered'); ?>)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sZeroRecords: "<?php echo $this->lang->line('sZeroRecords'); ?>"
    },
});


$('.ActiveLanguage').click(function(){
    var status = $(this).attr("rel");
    var id = $(this).attr("data-id");
    if($(this).hasClass("fa-toggle-on")){
        $(this).removeClass("fa-toggle-on").addClass("fa-toggle-off");
    }else{
        $(this).removeClass("fa-toggle-off").addClass("fa-toggle-on");
    }
    
    $.ajax({
        url: "/cpanel/languages/updateActiveLanguage",
        dataType: "html",
        type: "POST",
        data: "status="+status+"&id="+id,
        context: $(this),
        success: function(result){
            if(result == "OK"){
                $('.widget-control-refresh').click();
            }
        }
    });
    
});

function sync(LangID){
    $('.fa-refresh').addClass("fa-spin");
    $.ajax({
        url: "/cpanel/languages/updateInsertMoreLanguage",
        dataType: "html",
        type: "POST",
        data: "LangID="+LangID,
        context: $(this),
        success: function(result){
            if(result == "Done"){
                $('.fa-refresh').removeClass("fa-spin");
                location.reload();
            }
        }
    });
}
$('.btnSync').click(function(){
    $(this).addClass("fa-spin");
    var rel = $(this).attr("rel");
    var langid = $(this).attr("data-id");
    if(rel == 1){
        $.ajax({
            url: "/cpanel/languages/updateInsertMoreLanguage",
            dataType: "html",
            type: "POST",
            data: "LangID="+langid,
            context: $(this),
            success: function(result){
                console.log(result);
                if(result == "Done"){
                    $('.btnSync').attr("rel","2");
                    $('.btnSync').removeClass("fa-spin");
                    $('.btnSync').removeClass("btnSync");
                }
            }
        });
    }else if(rel == 2){
        location.reload(true);
    }
});
</script>