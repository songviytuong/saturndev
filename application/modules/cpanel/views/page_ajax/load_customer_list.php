<table class="table table-bordered table-hover datatable">
    <thead>
        <tr>
            <th class="col-lg-1 text-center"><?php echo $this->lang->line('autoid'); ?></th>
            <th class="col-lg-1"></th>
            <th><?php echo $this->lang->line('customer'); ?></th>
            <th class="hidden-xs"><?php echo $this->lang->line('address'); ?></th>
            <th class="text-center"><?php echo $this->lang->line('phone'); ?></th>
            <th class="text-center"><?php echo $this->lang->line('identitycard'); ?></th>
            <th class="text-center">Vay tiền</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i = 0;
            foreach($result as $row){
                $isLoans = $this->db->query("SELECT count(*) as cnt FROM m_loans WHERE cID = $row->id ")->row();
                $isLoans = ($isLoans) ? $isLoans->cnt : 0;
        ?>
        <tr>
            <td class="text-center"><?=$i+1;?></td>
            <td class="text-center" style="cursor: pointer;"><i class="fa fa-edit"></i></td>
            <td class="<?=($isLoans == 0) ? 'nonedetails':'loansdetails text-primary';?>" data-token="<?=substr(md5($row->id),TOKENF,TOKENT);?>" data-id="<?=$row->id;?>" style="cursor: pointer;"><?=$row->fullname;?></td>
            <td class="hidden-xs"><?=$row->address;?></td>
            <td class="text-center"><?=$row->phone;?></td>
            <td class="text-center"><?=$row->cmnd;?></td>
            <td class="text-center text-danger addLoans" style="cursor: pointer;" data-phone="<?=$row->phone;?>"><i class="fa fa-plus"></i></td>
        </tr>
        <?php $i++;} ?>
    </tbody>
</table>
<script>
$(".datatable").dataTable( {
    "oLanguage": {
        "oPaginate": {
            "sNext": "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>",
        },
        sSearch: "",
        sLengthMenu: "_MENU_",
        sEmptyTable: "<?php echo $this->lang->line('sEmptyTable'); ?>",
        sInfo: "<?php echo $this->lang->line('sInfo'); ?>",
        sInfoEmpty: "",
        sInfoFiltered: "(<?php echo $this->lang->line('sInfoFiltered'); ?>)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sZeroRecords: "<?php echo $this->lang->line('sZeroRecords'); ?>"
    },
});

$('.loansdetails').click(function(){
    $('.widget-detail').removeClass("hidden");
    var id = $(this).attr("data-id");
    var token = $(this).attr("data-token");
    loadLoansDetails(id,token);
});

$('.nonedetails').click(function(){
    $('.widget-detail').addClass("hidden");
});

$('.addLoans').click(function(){
    var token = "<?=substr(sha1($this->session->userdata('_token')),TOKENF,TOKENT);?>";
    $.ajax({
        url: "/cpanel/loans/setSessionRedirectTo",
        dataType: "JSON",
        type: "POST",
        data: "phone="+ $(this).attr("data-phone")+"&_token=" + token,
        context: $(this),
        success: function(result){
            if(result.status == "OK"){
                window.location = "<?= $new_customer_link . "?_token=" . substr(sha1($this->session->userdata('_token')), TOKENF, TOKENT); ?>";
            }else{
                console.log(result);
            }
        }
    });
});
</script>