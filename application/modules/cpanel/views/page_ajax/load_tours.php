<table class="table table-bordered table-hover datatable">
    <thead>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th class="text-center">Date-Time</th>
            <th>Price</th>
            <th class="hidden-xs"></th>
            <th class="hidden-xs  text-center col-lg-2">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($result as $row){
        ?>
        <tr>
            <td>1</td> 
            <td><?=$row->Title;?></td> 
            <td class="text-center">4 ngày 3 đêm</td> 
            <td><?=number_format($row->Price);?></td> 
            <td class="hidden-xs"></td> 
            <td class="hidden-xs text-center"><a class="btn btn-default btn-xs <?=$disabled_edit;?>" onclick="showEditTourModal(this)" rel="edittour" data-bind="<?=$row->ID;?>"><?=$edittext;?></a> <a class="btn btn-danger btn-xs <?=$disabled_delete;?>" onclick="showModal(this)" rel="btn_add"><?=$deletetext;?></a></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<script>
$(".datatable").dataTable( {
    "oLanguage": {
        "oPaginate": {
            "sNext": "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>",
        },
        sSearch: "",
        sLengthMenu: "_MENU_",
        sEmptyTable: "<?php echo $this->lang->line('sEmptyTable'); ?>",
        sInfo: "<?php echo $this->lang->line('sInfo'); ?>",
        sInfoEmpty: "",
        sInfoFiltered: "(<?php echo $this->lang->line('sInfoFiltered'); ?>)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sZeroRecords: "<?php echo $this->lang->line('sZeroRecords'); ?>"
    },
});

function showEditTourModal(ob){
    var id = $(ob).attr("data-bind");
    $("#modalFormDefine").modal({show: 'true'});
    $("#modalFormDefine .modal-dialog").css({'width':'80%'});
    $("#modalFormDefine .modal-dialog .modal-content .widget").removeClass("widget-blue").addClass("widget-red");
    $("#modalFormDefine .modal-dialog .modal-content h3").html("<i class='fa fa-edit'></i>EDIT TOUR");
    $("#modalFormDefine .modal-dialog .modal-content .modal-body").load("<?=$edithref;?>/"+id);
}

</script>
