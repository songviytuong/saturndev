<table class="table table-bordered table-hover datatable">
    <thead>
        <tr>
            <th class="text-center"><?php echo $this->lang->line('autoid'); ?></th>
            <th class=""></th>
            <th class="text-center">Số tiền</th>
            <th class="text-center">Két tiền</th>
            <th class="text-center">Ghi chú</th>
            <th class="text-center">Ngày tháng</th>
            <th class="text-center"></th>
            <th class="text-center"></th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i = 0;
            foreach($result as $row){
                /* Lấy tên của Branch qua package
                 * $row->add_package
                 */
                $package_name = $this->db->query("SELECT name FROM m_branch WHERE id = $row->add_package")->row();
                $package_name = ($package_name) ? $package_name->name : "--";
        ?>
        <tr>
            <td class="text-center"><?=$i+1;?></td>
            <td class="text-center" data-id="<?=$row->id;?>" data-token="<?=substr(md5($row->id),TOKENF,TOKENT);?>" <?php if($row->status == 0): ?> onclick="editMoney(this)" style="cursor: pointer;"<?php endif;?>><?=($row->status == 1) ? '':'<i class="fa fa-edit"></i>';?></td>
            <td class="text-right"><?=number_format($row->add_money);?> <?=($row->add_type == 0) ? '<font color="green"><i class="fa fa-caret-up"></i></font>':'<font color="red"><i class="fa fa-caret-down"></i></font>';?></td>
            <td class="text-center"><?=$package_name;?></td>
            <td class=""><?=$row->add_source;?></td>
            <td class="text-center"><?=date(DATEFORMAT,strtotime($row->add_date));?></td>
            <td class="text-center <?=($row->status == 0) ? 'text-default':'text-success';?>" <?php if($row->status == 0):?>onclick="verifyMoneyEdit(this);"<?php endif;?> data-id="<?=$row->id;?>" data-token="<?=substr(md5($row->id."verifyMoneyEdit"),TOKENF,TOKENT);?>"><i class="fa fa-check-square fa-lg" style="cursor: pointer"></i></td>
            <td class="text-center" <?php if($row->status == 0):?>onclick="deleteMoneyAdd(this);"<?php endif; ?> data-id="<?=$row->id;?>" data-token="<?=substr(md5($row->id."deleteMoneyAdd"),TOKENF,TOKENT);?>"><?php if($row->status == 0):?><i class="fa fa-trash-o" style="cursor: pointer"></i><?php endif;?></td> 
        </tr>
        <?php $i++;} ?>
    </tbody>
</table>
<script>
$(".datatable").dataTable( {
    "oLanguage": {
        "oPaginate": {
            "sNext": "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>",
        },
        sSearch: "",
        sLengthMenu: "_MENU_",
        sEmptyTable: "<?php echo $this->lang->line('sEmptyTable'); ?>",
        sInfo: "<?php echo $this->lang->line('sInfo'); ?>",
        sInfoEmpty: "",
        sInfoFiltered: "(<?php echo $this->lang->line('sInfoFiltered'); ?>)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sZeroRecords: "<?php echo $this->lang->line('sZeroRecords'); ?>"
    },
    "iDisplayLength": 50
});

function verifyMoneyEdit(ob){
    var id = $(ob).attr("data-id");
    var token = $(ob).attr("data-token");
    if (confirm('Bạn chắc chắn số tiền này là đúng?\nSẽ không cho phép chỉnh sửa nữa nếu bạn đồng ý.')) {
        $.ajax({
            url: "/cpanel/loans/verifyMoneyEdit",
            dataType: "JSON",
            type: "POST",
            data: "id="+id+"&token="+token,
            context: $(ob),
            beforeSend: function(){
                
            },
            success: function(result){
                if(result.status == "OK"){
                    loadMoneyList();
                }else{
                    
                }
            }
        });
    } else {
        // Do nothing!
    }
}

function deleteMoneyAdd(ob){
    var id = $(ob).attr("data-id");
    var token = $(ob).attr("data-token");
    if (confirm('Bạn chắc chắn xóa khoản tiền này?')) {
        $.ajax({
            url: "/cpanel/loans/deleteMoneyAdd",
            dataType: "JSON",
            type: "POST",
            data: "id="+id+"&token="+token,
            context: $(ob),
            beforeSend: function(){
                
            },
            success: function(result){
                if(result.status == "OK"){
                    loadMoneyList();
                }else{
                    alert('Không thể xóa được!!!');
                }
            }
        });
    } else {
        // Do nothing!
    }
}
</script>