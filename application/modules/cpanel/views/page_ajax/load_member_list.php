<table class="table table-bordered table-hover datatable">
    <thead>
        <tr>
            <th class="text-center"><?php echo $this->lang->line('autoid'); ?></th>
            <th class=""></th>
            <th class=""></th>
            <th class="text-center"><?php echo $this->lang->line('branch'); ?></th>
            <th class="text-center hidden-xs"><?php echo $this->lang->line('address'); ?></th>
            <th class="text-center">Tiền có</th>
            <th class="text-center"></th>
            <th class="text-center"></th>
            <th class="text-center"></th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i = 0;
            $userID = $this->user->ID;
            foreach($result as $row){
                $getMoneyInStore = $this->db->query("SELECT sum(add_money) as total_money FROM m_money_details WHERE add_package = $row->id and userID = $userID and del_flag = 0 and add_type = 0")->row();
                $getTotalMoney = ($getMoneyInStore) ? $getMoneyInStore->total_money : 0;
        ?>
        <tr>
            <td class="text-center"><?=$i+1;?></td>
            <td class="text-center" style="cursor: pointer;"><?php if($row->isdefault == 0):?><i class="fa fa-edit"></i><?php endif; ?></td>
            <td class="text-center text-primary" data-id="<?=$row->id;?>" data-token="<?=substr(md5($row->id."onOffBranch"),TOKENF,TOKENT);?>" style="cursor: pointer;" <?php if($row->isdefault == 0):?>onclick="onOffBranch(this);"<?php endif; ?> data-rel="<?=($row->active == 1)? 1:0;?>"><i class="fa <?=($row->active == 1)? 'fa-toggle-on':'fa-toggle-off';?> fa-lg"></i></td>
            <td class="text-left"><?=$row->branchname;?></td>
            <td class="hidden-xs"><?=$row->address;?></td>
            <td class="text-right"><?=number_format($getTotalMoney);?></td>
            <td class="text-center text-primary" onclick="viewPersonal(this);" data-id="<?=$row->id;?>"><i class="fa fa-search-plus" style="cursor: pointer" title="Danh sách NS"></i></td>
            <td class="text-center text-danger addPersonal"><i class="fa fa-plus" style="cursor: pointer" title="Thêm mới NS"></i></td>
            <td class="text-center text-danger removePersonal"><?php if(($row->isdefault == 0) && ($this->user->ID == $row->userID)) { ?><i class="fa fa-trash-o" style="cursor: pointer" title="Xoá CH"></i><?php } ?></td>
        </tr>
        <?php $i++;} ?>
    </tbody>
</table>
<script>
$(".datatable").dataTable( {
    "oLanguage": {
        "oPaginate": {
            "sNext": "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>",
        },
        sSearch: "",
        sLengthMenu: "_MENU_",
        sEmptyTable: "<?php echo $this->lang->line('sEmptyTable'); ?>",
        sInfo: "<?php echo $this->lang->line('sInfo'); ?>",
        sInfoEmpty: "",
        sInfoFiltered: "(<?php echo $this->lang->line('sInfoFiltered'); ?>)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sZeroRecords: "<?php echo $this->lang->line('sZeroRecords'); ?>"
    },
});

function viewPersonal(ob){
    $('.onPersonalList').removeClass("hidden");
    var id = $(ob).attr("data-id");
    $.ajax({
        url: "/cpanel/loans/loadPersonalList",
        dataType: "html",
        type: "POST",
        data: "id="+id,
        context: $(ob),
        success: function(result){
            $('.loadPersonalList').html(result);
            $.getScript("public/authen/d7dfc13379a397356e42ab8bd98901a0.js");
        }
    });
}

function onOffBranch(ob){
    var status = $(ob).attr("data-rel");
    var id = $(ob).attr("data-id");
    var token = $(ob).attr("data-token");
    if(status == 1){
        $(ob).html("<i class='fa fa-toggle-off'></i>");
        $(ob).attr("data-rel",0);
    }else{
        $(ob).html("<i class='fa fa-toggle-on'></i>");
        $(ob).attr("data-rel",1);
    }
    $.ajax({
        url: "/cpanel/loans/onOffBranch",
        dataType: "html",
        type: "POST",
        data: "id="+id+"&token="+token+"&status="+status,
        context: $(ob),
        success: function(result){
            //Done
            console.log(result);
        }
    });
    
}
</script>