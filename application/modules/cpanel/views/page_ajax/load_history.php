<div class="">
    <table class="table table-bordered" width="100%">
        <?php
            $i = 1;
            $loanID = 0;
            $cntReturn = count($result);
            foreach($result as $row){
                if($i<10){
                    $i = str_pad($i,2,"0",STR_PAD_LEFT);
                }
                $loanID = $row->loanID;
        ?>
        <tr>
            <td><i class="fa fa-plus-circle"></i> Lần <?=$i;?>:</td>
            <td class="text-center"><?=date(DATEFORMAT,strtotime($row->return_date));?></td>
            <td class="text-center"><?=number_format($row->return_money);?></td>
            <td class="text-center"><?=$row->return_notes;?></td>
        </tr>
        <?php $i++; }
            $exday = $this->db->query("SELECT after_date FROM m_loans WHERE id = $loanID")->row();
            $exday = ($exday) ? $exday->after_date : 0 ;
        ?>
    </table>
    <div class="hidden">Còn: <span class="text-danger"><?php
        $day = $exday-$cntReturn;
        echo $day ;?></span> (ngày)</div>
</div>