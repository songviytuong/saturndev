<?php
    for ($m=1; $m<=12; $m++) {
        $month = date('m-'.$year, mktime(0,0,0,$m, 1, date('Y')));
        $month_search = date($year.'-m', mktime(0,0,0,$m, 1, date('Y')));
        
        $userID = $this->user->ID;
        $getDataAdd = $this->db->query("SELECT sum(add_money) as total_add FROM m_money_details WHERE add_type = 0 and add_date LIKE '%$month_search%' and userID = $userID")->row();
        $getDataAdd = ($getDataAdd) ? $getDataAdd->total_add : '';

        $getDataSub = $this->db->query("SELECT sum(add_money) as total_sub FROM m_money_details WHERE add_type = 1 and add_date LIKE '%$month_search%' and userID = $userID")->row();
        $getDataSub = ($getDataSub) ? $getDataSub->total_sub : '';
    ?>
    <tr>
        <td><?="T".$month;?></td>
        <td class="text-success"><?=number_format($getDataAdd);?></td>
        <td class="text-danger"><?=number_format($getDataSub);?></td>
        <td class="text-center text-primary" style="cursor: pointer;"><i class="fa fa-plus-circle"></i></td>
    </tr>
<?php } ?>