<table class="table table-bordered table-hover datatable">
    <thead>
        <tr>
            <th class="text-center"><?php echo $this->lang->line('autoid'); ?></th>
            <th class=""></th>
            <th><?php echo $this->lang->line('fullname'); ?></th>
            <th class="hidden-xs"><?php echo $this->lang->line('address'); ?></th>
            <th class="text-center"><?php echo $this->lang->line('phone'); ?></th>
            <th class="text-center">Lương</th>
            <th class="text-center">Vị trí</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i = 0;
            foreach($resultNS as $row){
        ?>
        <tr>
            <td class="text-center"><?=$i+1;?></td>
            <td class="text-center" style="cursor: pointer;"><i class="fa fa-edit"></i></td>
            <td class=""><?=$row->fullname;?> <?=!empty($row->nickname) ? "(".$row->nickname.")" : '';?></td>
            <td class="hidden-xs"><?=$row->address;?></td>
            <td class="text-center"><?=$row->phone;?></td>
            <td class="text-center"><?=number_format($row->salary);?></td>
            <td class="text-center"><?=($row->position);?></td>
        </tr>
        <?php $i++;} ?>
    </tbody>
</table>
<script>
$(".datatable").dataTable( {
    "oLanguage": {
        "oPaginate": {
            "sNext": "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>",
        },
        sSearch: "",
        sLengthMenu: "_MENU_",
        sEmptyTable: "<?php echo $this->lang->line('sEmptyTable'); ?>",
        sInfo: "<?php echo $this->lang->line('sInfo'); ?>",
        sInfoEmpty: "",
        sInfoFiltered: "(<?php echo $this->lang->line('sInfoFiltered'); ?>)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sZeroRecords: "<?php echo $this->lang->line('sZeroRecords'); ?>"
    },
});
</script>