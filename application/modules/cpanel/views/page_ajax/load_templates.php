<label for="">Templates List</label>
                <select data-placeholder="Your Favorite Layout" class="form-control chosen-select">
                    <option value="-1"></option>
                    <?php 
                        $langs = $this->session->userdata('languageTT');
                        $arrLang = $this->db->query("SELECT * FROM ttp_languages")->result();
                        foreach($arrLang as $keyLang=>$lg){
                            if($langs == $lg->LangName){
                                $aLang = $keyLang;
                            }
                        }
                        $TemplateCate = $this->db->query("SELECT * FROM ttp_templates_category")->result();
                        foreach($TemplateCate as $keyCate=>$cate){
                            $value = unserialize($cate->CateValue);
                            foreach($value as $key=>$val){
                                $arr[$key][] = $val;
                            }

                    ?>
                    <optgroup label="<?php echo strtoupper($arr[$aLang][$keyCate]); ?>">
                        <?php
                            $TemplateList = $this->db->query("SELECT * FROM ttp_templates WHERE CateID = $cate->CateID")->result();
                            foreach ($TemplateList as $list){
                        ?>
                        <option value="<?php echo $list->ID;?>"><?php echo $list->TempName;?></option>
                        <?php } ?>
                    </optgroup>
                    <?php } ?>
                    </select>