<table class="table table-bordered table-hover datatable">
    <thead>
        <tr>
            <th class="text-center">- ID -</th>
            <th>Keywords</th>
            <?php
            $arr_Languages = $this->db->query("SELECT * FROM ttp_languages WHERE LangActive = 1")->result();
            foreach ($arr_Languages as $item) {
                ?>
                <th><?= $item->LangName; ?></th>
            <?php } ?>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        $Translate = $this->db->query("SELECT DISTINCT (Keyword) FROM ttp_languages_translate ORDER BY Keyword ASC")->result();
        foreach ($Translate as $items) {
            ?>
            <tr class="has-success has-iconed">
                <td class="text-center"><?= $i; ?></td>
                <td><?= $items->Keyword ?></td>
                <?php
                $arr_Languages = $this->db->query("SELECT * FROM ttp_languages WHERE LangActive = 1")->result();
                foreach ($arr_Languages as $item) {
                    $TranText = $this->db->query("SELECT Translate,ID,Keyword FROM ttp_languages_translate WHERE LangID = $item->LangID and Keyword = '$items->Keyword'")->row();
                    ?>
                <td class="td_<?=isset($TranText->ID) ? $TranText->ID : 0;?>"><input type="text" value="<?= isset($TranText->Translate) ? $TranText->Translate : ""; ?>" class="form-control" onblur="updateRow(this)" rel="<?=isset($TranText->ID) ? $TranText->ID : $TranText->Keyword;?>" style="border:0px; height:22px;"/></td>
                <?php } ?>
                <td class="text-center">
                    <a class="btn btn-default btn-xs edit-tr disabled" rel="<?=$items->Keyword?>"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_save'); ?></a>
                    <a href="#" class="btn btn-danger btn-xs remove-tr"><i class="fa fa-times"></i><?php echo $this->lang->line('btn_delete'); ?></a>
                </td>
            </tr>
            <?php $i++;
        } ?>
    </tbody>
</table>
<script>
var oTable = $(".datatable").dataTable( {
    "sScrollX": "100%",
    "sScrollXInner": "150%",
    "bScrollCollapse": true,
    "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0]}
       ],
    "oLanguage": {
        "oPaginate": {
            "sNext": "<?php echo $this->lang->line('sNext'); ?>",
            "sPrevious": "<?php echo $this->lang->line('sPrevious'); ?>",
        },
        sSearch: "",
        sLengthMenu: "_MENU_",
        sEmptyTable: "<?php echo $this->lang->line('sEmptyTable'); ?>",
        sInfo: "<?php echo $this->lang->line('sInfo'); ?>",
        sInfoEmpty: "",
        sInfoFiltered: "(<?php echo $this->lang->line('sInfoFiltered'); ?>)",
        sInfoPostFix: "",
        sInfoThousands: ",",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sZeroRecords: "<?php echo $this->lang->line('sZeroRecords'); ?>"
    },
});
new FixedColumns(oTable);
</script>