<table class="table table-bordered table-hover datatable1">
    <thead>
        <tr>
            <th></th>
            <th class="col-lg-2 text-center">Thời gian</th>
            <th></th>
            <th class="text-center">Tiền vay</th>
            <th class="text-center">Phí/ngày</th>
            <th class="text-center">Thời lượng</th>
            <th class="text-center">Đóng ngày</th>
            <th class="text-center">Phải trả</th>
            <th class="text-center">Đã đóng</th>
            <th class="text-center">Đóng tiền</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach($result as $row){
                $dataverify = $this->db->query("SELECT * FROM m_members WHERE id = $row->verify_by")->row();
                $verify_data = $dataverify->fullname." | ".$dataverify->phone." | ".$dataverify->address;
                
                $datacust = $this->db->query("SELECT * FROM m_customers WHERE id = $row->cID")->row();
                $customer_id = $datacust->id;
                $customer_src = $datacust->cmnd_src;
                $customer_data = $datacust->fullname." | ".$datacust->phone;
                
                /* loanID = $row->id;
                 * cID = $row->cID;
                 * Xem KH cID này đã đóng được mấy lần tương đương với mấy ngày trong m_loans_details
                 */
                $d = $this->db->query("SELECT count(*) as cnt FROM m_loans_details WHERE cID = $row->cID and loanID = $row->id and return_money != 0")->row();
                $d = (count($d) > 0) ? $d->cnt : 0;
                $now = time();
                $details = $this->db->query("SELECT * FROM m_loans_details WHERE cID = $row->cID and loanID = $row->id ORDER BY id DESC LIMIT 0,1")->row();
                $date = (count($details) > 0) ? strtotime($details->return_date) : $now;
                $dateone = strtotime("+1 day", $date);
                
                if($d > 0 && $now <= $dateone){
                    $btn = "<i class='fa fa-check text-primary'></i>";
                    $propeties = "";
                }else{
                    $btn = "<i class='fa fa-plus-square text-danger'></i>";
                    $propeties = " onclick='addReturnMoney(this)'";
                }
                
                $branch_data = $this->db->query("SELECT * FROM m_branch WHERE id = $row->branchID")->row();
                $branch_data = ($branch_data) ? $branch_data->name : "";
        ?>
        <tr>
            <td><?php if($d == 0) { ?><span class="text-danger" loan-id="<?=$row->id;?>"><i class="fa fa-trash-o"></i></span><?php } ?></td>
            <td class="text-center"><?= date(DATEFORMAT,strtotime($row->created));?></td>
            <td class="text-center text-danger" data-title="<?=$row->title;?>" data-notes="<?=$row->notes;?>" data-src="<?=$customer_src;?>" data-customer="<?=$customer_data;?>" data-verify-by="<?=$verify_data;?>" data-branch="<?=$branch_data;?>" data-verify-notes="<?=$row->verify_notes;?>" data-id="<?=substr(md5($row->id),TOKENF,TOKENT);?>" rel="<?=$row->id;?>" style="cursor: pointer;" onclick="loansDetails(this)">Chi tiết <i class="fa fa-arrow-right"></i></td>
            <td class="text-center"><?=number_format($row->loan_money);?></td>
            <td class="text-center"><?=number_format($row->fee_money);?></td>
            <td class="text-center"><?=$row->after_date;?> (ngày)</td>
            <td class="text-center"><?=number_format($row->return_money / $row->after_date);?></td>
            <td class="text-center"><?=number_format($row->return_money);?></td>
            <td class="text-center" style="cursor: pointer"><?php if($d > 0) { ?><span class="text-primary" customer-id="<?=$customer_id;?>" loan-id="<?=$row->id;?>" onclick="loansHistory(this);"><?=($d <10)? str_pad($d,2,"0",STR_PAD_LEFT)."/".$row->after_date : $d."/".$row->after_date;?></span><?php } ?></td>
            <td class="text-center" <?=$propeties;?> class="text-center text-primary" data-customer="<?=$customer_data;?>" data-id="<?=$customer_id.'|'.$row->id;?>" data-return="<?=number_format($row->return_money / $row->after_date);?>" style="cursor: pointer;"><?=$btn;?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<script>
$(".datatable1").dataTable( {
    "bPaginate": false,
    "bFilter": false,
    "bInfo": false
});

function loansDetails(ob){
    $(".modal-body .source").attr("src","public/loans/cmnd/" + $(ob).attr("data-src"));
    $(".modal-body .content").text($(ob).attr("data-title"));
    $(".modal-body .notes").text($(ob).attr("data-notes"));
    $(".modal-body .verify_by").text($(ob).attr("data-verify-by"));
    $(".modal-body .branchID").text($(ob).attr("data-branch"));
    $(".modal-body .verify_notes").text($(ob).attr("data-verify-notes"));
    $(".details h3").html("<i class='fa fa-info-circle'></i> "+$(ob).attr("data-customer"));
    $('#modalLoansDetails').modal(true);
}

function addReturnMoney(ob){
    $(".addReturn h3").html("<i class='fa fa-plus-circle'></i> "+$(ob).attr("data-customer"));
    $(".addReturnContent input#dataid").val($(ob).attr("data-id"));
    $(".addReturnContent input#return_money").attr("placeholder",$(ob).attr("data-return"));
    $('#modalAddLoans').modal(true);
}

function loansHistory(ob){
    var loanid = $(ob).attr("loan-id");
    var customerid = $(ob).attr("customer-id");
    var link = "/cpanel/loans/loadHistory/?loan="+loanid+"&customer="+customerid;
    $("#modalHistory").modal({show: 'true'});
    $("#modalHistory .modal-dialog .modal-content .modal-body").load(link);
}
</script>