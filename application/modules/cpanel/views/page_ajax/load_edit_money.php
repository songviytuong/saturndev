<form id="frmEditMoney">
    <input type="hidden" name="id" value="<?=$data->id?>"/>
    <input type="hidden" name="token" value="<?=substr(md5($data->id),TOKENF,TOKENT)?>"/>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Hành động</label>
                <select name="add_type" id="type" class="form-control">
                    <option value="0" <?=($data->add_type == 0) ? "selected":"";?>>Thu (vào)</option>
                    <option value="1" <?=($data->add_type == 1) ? "selected":"";?>>Chi (ra)</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Số tiền</label>
                <input type="number" name="add_money" id="add_money" class="form-control" placeholder="Nhập tiền" value="<?=$data->add_money;?>">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Từ két</label>
                <select name="add_package" id="package" class="form-control">
                    <?php
                    $this->load->model("loans_model", "loans");
                    $ListBranch = $this->loans->getListBranchByUserID($this->user->ID);
                    foreach ($ListBranch as $item) {
                        ?>
                        <option value="<?= $item->id; ?>" <?=($data->add_package == $item->id) ? "selected":"";?>><?= $item->name; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Ghi chú</label>
                <input type="text" name="add_source" class="form-control" placeholder="<?php echo $this->lang->line('ph_address'); ?>" value="<?=$data->add_source;?>">
            </div>
        </div>
    </div>
    <div class="btn-group">
        <a class="btn btn-primary btn-xs editMoneyAction"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_save'); ?></a>
    </div>
    <span class="showMessages_saveEditMoney"></span>
</form>

<script type="text/javascript">
$('.editMoneyAction').click(function(){
    $(this).addClass("disabled");
    $('.showMessages_saveEditMoney').html("<i class='fa fa-spin fa-cog'></i>");
    $.ajax({
        url: "/cpanel/loans/editMoneyAction",
        dataType: "JSON",
        type: "POST",
        data: $('#frmEditMoney').serialize(),
        context: $(this),
        success: function(e){
            setTimeout(function(){
                $('.editMoneyAction').removeClass("disabled");
                if(e.status == "OK"){
                    $('.showMessages_saveEditMoney').addClass("text-success");
                    $('.showMessages_saveEditMoney').html("<i class='fa fa-check-circle'></i> " + e.message + " !");
                }else{
                    $('.showMessages_saveEditMoney').addClass("text-danger");
                    $('.showMessages_saveEditMoney').html("<i class='fa fa-minus-circle'></i> " + e.error + " !");
                } 
            },100);
            loadMoneyList();
        }
    });
});
</script>