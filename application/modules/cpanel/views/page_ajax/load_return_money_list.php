<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th class="text-center">Order</th>
            <th class="text-center">Ngày</th>
            <th class="text-center">Tại CH</th>
            <th class="text-center">Họ tên</th>
            <th class="text-center">Điện thoại</th>
            <th class="text-center">Chưa đóng</th>
            <th class="text-center">Số tiền</th>
            <th class="text-center"></th>
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach($result as $row){
                $need_return = $row->loan_money + $row->fee_money * $row->after_date;
                $need_return = number_format($need_return / $row->after_date);
        ?>
        <tr>
            <td class="text-center"><?="ORD".str_pad($row->ordercode,5,'0',STR_PAD_LEFT);?></td>
            <td class="text-center"><?=date(DATEFORMAT,strtotime($row->created));?></td>
            <td class="text-center"><?=$row->branchname;?></td>
            <td class="text-left"><?=$row->fullname;?></td>
            <td class="text-center"><?=$row->phone;?></td>
            <td class="text-center">? (ngày)</td>
            <td class="text-center"><?=$need_return;?></td>
            <td class="text-center text-danger"><i class="fa fa-plus-circle" style="cursor: pointer"></i></td>
        </tr>
                <?php } ?>
    </tbody>
</table>