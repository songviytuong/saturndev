<div class="row">
  <div class="col-md-12">
    <span class="offset_anchor" id="widget_tabs"></span>
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_pie_chart" data-toggle="tab"><i class="fa fa-bullseye"></i> INFO</a></li>
      <li><a href="#tab_bar_chart" data-toggle="tab"><i class="fa fa-bar-chart-o"></i> DATE</a></li>
      <li class="hidden-md hidden-xs"><a href="#tab_table" data-toggle="tab"><i class="fa fa-th"></i> Table</a></li>
    </ul>
    <div class="tab-content bottom-margin">
      <div class="tab-pane active" id="tab_pie_chart">
        <div class="shadowed-bottom">
            <div class="row">
                <div class="container">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description:</label>
                            <textarea class="summernote" name="editor" rows="6"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="padded">
                <a class="btn btn-primary btn-xs"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_save'); ?></a>
              </div>
      </div>
        <div class="tab-pane" id="tab_bar_chart">
            <div class="shadowed-bottom divTour">
                <form name="frmTour" id="frmTour">
                <div class="row">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea class="summernote" name="tourDay[]" rows="6"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                
                    <table id="tbl" rules="all" style="background:#fff;">
                        <tr>
                            <td style="font-size:14px;" >Name</td>
                            <td><span style="cursor:pointer;" onclick="addMoreRows(this);"> Add More </span> | 
                                <?php
                                if (isset($edit) != true) {
                                    ?>
                                    <span style="cursor:pointer;" onclick="saveNames(this);">Save</span> | 
                                <?php } else { ?>
                                    <span style="cursor:pointer;" onclick="updateNames(this);">Update</span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                        if (isset($edit) == true) {
                            echo "<input type='hidden' name='id' value='" . $editid . "'>";
                            $i = 0;
                            foreach ($result as $key => $item) {
                                ?>
                                <tr id="rowCount<?= $i; ?>">
                                    <td><input name="name[]" type="text"  value="<?= $item["name"] ?>" size="17%"/></td>
                                    <td><input name="phone[]" type="text"  value="<?= $item["phone"] ?>" size="17%"/></td>
                                    <td>
                                        <select name="type[]">
                                            <option value="1" <?= ($item["type"] == 1) ? "selected" : ""; ?>>Type 1</option>
                                            <option value="2" <?= ($item["type"] == 2) ? "selected" : ""; ?>>Type 2</option>
                                            <option value="3" <?= ($item["type"] == 3) ? "selected" : ""; ?>>Type 3</option>
                                            <option value="4" <?= ($item["type"] == 4) ? "selected" : ""; ?>>Type 4</option>
                                        </select>
                                    </td>
                                    <td><a href="javascript:void(0);" onclick="removeRow('<?= $i; ?>');">Delete</a></td>
                                </tr>
                                <?php $i++;
                            }
                        } else { ?>
                            <tr>
                                <td><input name="name[]" type="text"  value="" size="17%"/></td>
                                <td><input name="phone[]" type="text"  value="" size="17%"/></td>
                                <td>
                                    <select name="type[]">
                                        <option value="1">Type 1</option>
                                        <option value="2">Type 2</option>
                                        <option value="3">Type 3</option>
                                        <option value="4">Type 4</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                    <input type="hidden" id="baselink" value="<?php echo $base_link; ?>"/>
                </form>
            </div>
            <div class="padded">
                <a class="btn btn-danger btn-xs"><i class="fa fa-plus-square-o"></i><?php echo $this->lang->line('btn_add'); ?></a> <a class="btn btn-primary btn-xs"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_save'); ?></a>
              </div>
            
        </div>
      <div class="tab-pane" id="tab_table">
        <div class="padded">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Product</th>
              <th>Qty</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Floor Lamp</td>
              <td>2</td>
              <td>3</td>
            </tr>
            <tr>
              <td>Coffee Mug</td>
              <td>4</td>
              <td>7</td>
            </tr>
            <tr>
              <td>Television</td>
              <td>1</td>
              <td>3</td>
            </tr>
            <tr>
              <td>Red Carpet</td>
              <td>6</td>
              <td>5</td>
            </tr>
            <tr>
              <td>Laptop</td>
              <td>3</td>
              <td>6</td>
            </tr>
          </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="/public/authen/ad67372f4b8b70896e8a596720082ac6.js"></script>