<?php
    $this->load->view('common/breadcrumb');
?>
<div class="alert alert-warning alert-dismissable bottom-margin">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-exclamation-circle"></i> <strong>Welcome!</strong> This is a dashboard of the powerful admin template.
</div>
