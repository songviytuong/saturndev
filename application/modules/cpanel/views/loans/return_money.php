<?php
    $this->load->view('common/breadcrumb');
    $now = date('d-m-Y');
?>
<div class="widget widget-red" style="margin-bottom: 10px;">
    <div class="widget-title">
        <div class="widget-controls">
            <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $this->lang->line('fullscreen'); ?>"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo $this->lang->line('exit_fullscreen'); ?>"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $this->lang->line('refresh'); ?>"><i class="fa fa-refresh"></i></a>
            <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $this->lang->line('minimize'); ?>"><i class="fa fa-minus-circle"></i></a>
            <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $this->lang->line('remove'); ?>"><i class="fa fa-times-circle"></i></a>
        </div>
        <h3><i class="fa fa-table"></i><?php echo $this->lang->line('return_money'); ?> ngày hôm nay <?=$now;?></h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive loadReturnMoneyList">
            <i class="fa fa-spinner fa-spin"></i> <?php echo $this->lang->line('loading'); ?>...
        </div>
    </div>
</div>