<?php
    $this->load->view('common/breadcrumb');
?>
<div class="widget widget-green hidden">
    <span class="offset_anchor" id="stat_charts_anchor"></span>
    <div class="widget-title">
        <div class="widget-controls">
            <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
            <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
            <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
        </div>
        <h3><i class="fa fa-bar-chart-o"></i> Statistics</h3>
    </div>
    <div class="widget-content">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="widget-content-blue-wrapper changed-up">
                    <div class="widget-content-blue-inner padded">
                        <div class="pre-value-block"><i class="fa fa-dashboard"></i> Total Visits</div>
                        <div class="value-block">
                            <div class="value-self">10,520</div>
                            <div class="value-sub">This Week</div>
                        </div>
                        <span class="dynamicsparkline">Loading..</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="widget-content-blue-wrapper changed-up">
                    <div class="widget-content-blue-inner padded">
                        <div class="pre-value-block"><i class="fa fa-user"></i> New Users</div>
                        <div class="value-block">
                            <div class="value-self">1,120</div>
                            <div class="value-sub">This Month</div>
                        </div>
                        <span class="dynamicsparkline">Loading..</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center hidden-md">
                <div class="widget-content-blue-wrapper changed-up">
                    <div class="widget-content-blue-inner padded">
                        <div class="pre-value-block"><i class="fa fa-sign-in"></i> Sold Items</div>
                        <div class="value-block">
                            <div class="value-self">275</div>
                            <div class="value-sub">This Week</div>
                        </div>
                        <span class="dynamicsparkline">Loading..</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 text-center">
                <div class="widget-content-blue-wrapper changed-up">
                    <div class="widget-content-blue-inner padded">
                        <div class="pre-value-block"><i class="fa fa-money"></i> Net Profit</div>
                        <div class="value-block">
                            <div class="value-self">$9,240</div>
                            <div class="value-sub">Yesterday</div>
                        </div>
                        <span class="dynamicbars">Loading..</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
      
<div class="row">
    
    <div class="col-md-6">
        <div class="widget widget-red">
            <div class="widget-title">
                <div class="widget-controls">
                    <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                    <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
                </div>
                <h3><i class="fa fa-bullseye"></i>THỐNG KÊ NĂM</h3>
            </div>
            <div class="widget-content">
                <div id="piechart" style=""></div>
                <table class="table table-bordered" id="topsellers_table">
                    <thead>
                        <tr>
                            <th>Năm</th>
                            <th>Thu</th>
                            <th>Chi</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            for($i=-2;$i<1;$i++){
                            $year = date('Y');
                            $year = $year + $i;
                            $userID = $this->user->ID;
                            $getDataAdd = $this->db->query("SELECT sum(add_money) as total_add FROM m_money_details WHERE add_type = 0 and add_date LIKE '%$year%' and userID = $userID")->row();
                            $getDataAdd = ($getDataAdd) ? $getDataAdd->total_add : '';
                            
                            $getDataSub = $this->db->query("SELECT sum(add_money) as total_sub FROM m_money_details WHERE add_type = 1 and add_date LIKE '%$year%' and userID = $userID")->row();
                            $getDataSub = ($getDataSub) ? $getDataSub->total_sub : '';
                        ?>
                        <tr>
                            <td>Năm <?=$year;?></td>
                            <td class="text-success"><?=number_format($getDataAdd);?></td>
                            <td class="text-danger"><?=number_format($getDataSub);?></td>
                            <td class="text-center text-primary" style="cursor: pointer;" data-year="<?=$year;?>" onclick="detailsMoneyOfMonth(this)"><i class="fa fa-search-plus"></i></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="widget widget-blue">
            <div class="widget-title details-year">
                <div class="widget-controls">
                    <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                    <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
                </div>
                <h3><i class="fa fa-list-alt"></i>NĂM</h3>
            </div>
            <div class="widget-content">
                <div id="piechart" style=""></div>
                <table class="table table-bordered table-details-month" id="topsellers_table">
                    <thead>
                        <tr>
                            <th>Năm</th>
                            <th>Chi</th>
                            <th>Thu</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function detailsMoneyOfMonth(ob){
    var year = $(ob).attr("data-year");
    $('.details-year h3').html("<i class='fa fa-list-alt'></i>NĂM "+year);
    $.ajax({
        url: "/cpanel/loans/loadDetailsMoneyOfMonth",
        dataType: "html",
        type: "POST",
        data: "year="+year,
        context: $(this),
        success: function(result){
            $('table.table-details-month tbody').html(result);
        }
    });
}
</script>