<?php
    $this->load->view('common/breadcrumb');
    if(isset($old_customer)){
        $data_customer = $this->db->query("SELECT * FROM m_customers WHERE phone = '$old_customer'")->row();
    }
?>
<?php if($old_customer) { ?>
<div class="alert alert-warning alert-dismissable bottom-margin">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-exclamation-circle"></i> Thêm khách hàng mới vui lòng <span class="newCustomerAction text-danger" style="cursor: pointer">Click</span> vào đây !!!  
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
        <div class="widget widget-red" style="margin-bottom:10px;">
            <div class="widget-title">
                <div class="widget-controls">
                    <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                    <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
                    <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
                </div>
                <?php if($old_customer) { ?>
                <h3><i class="fa fa-info-circle"></i><?php echo $this->lang->line('customer'); ?></h3>
                <?php } else { ?>
                <h3><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('add')." ".$this->lang->line('customer'); ?></h3>
                <?php } ?>
            </div>
            <div class="widget-content">
                <form name="frmCustomerInfo" id="frmCustomerInfo" role="form">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label><?php echo $this->lang->line('fullname');?></label>
                                <input type="text" name="fullname" class="form-control" value="<?=($old_customer) ? $data_customer->fullname : ''?>" <?=($old_customer) ? 'readonly' : ''?> placeholder="Nguyễn Văn A">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><?php echo $this->lang->line('address');?></label>
                                <input type="text" name="address" class="form-control" value="<?=($old_customer) ? $data_customer->address : ''?>" <?=($old_customer) ? 'readonly' : ''?> placeholder="303 Trường Chinh, P.2, Tân Bình">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label><?php echo $this->lang->line('phone');?></label>
                                <input type="number" name="phone" class="form-control" value="<?=($old_customer) ? $data_customer->phone : ''?>" <?=($old_customer) ? 'readonly' : ''?> placeholder="0989000000">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><?php echo $this->lang->line('identitycard'); ?></label> <i class="fa fa-camera-retro viewCMND <?=(isset($data_customer->cmnd_src) == "") ? 'hidden':'';?>" style="cursor: pointer" title="viewCMND"></i>
                                <div class="input-group">
                                    <input type="number" name="cmnd" class="form-control" value="<?=($old_customer) ? $data_customer->cmnd : ''?>" <?=($old_customer) ? 'readonly' : ''?> placeholder="012636123">
                                    <span class="input-group-addon <?=($old_customer) ? 'uploadCMND' : ''?>" data-id="<?=($old_customer) ? $data_customer->id : ''?>" data-src="<?=($old_customer) ? $data_customer->cmnd_src : ''?>" style="cursor: pointer"><i class="fa <?=($old_customer) ? 'fa-camera' : 'fa-barcode'?>" title="Upload CMND"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label class="clearfix">Vay tiền</label>
                                <span class="btn <?=($old_customer) ? '' : 'addCustomerInfo'?>"><i class="fa <?=($old_customer) ? 'fa-plus-circle addLoansDetails' : 'fa-save'?> fa-lg"></i></span>
                            </div>
                        </div>
                    </div>
                    <span class="showMessages"></span>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row rowLoansDetails hidden">
    <div class="col-md-12">
        <div class="widget widget-green">
            <div class="widget-title loans">
                <div class="widget-controls">
                    <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
                    <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                    <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
                    <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
                </div>
                <h3><i class="fa fa-money"></i></h3>
            </div>
            <div class="widget-content">
                <form name="frmLoansDetails" id="frmLoansDetails" role="form">
                    <input type="hidden" name="cID" value="<?=($old_customer) ? $data_customer->id : 0?>"/>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nội dung vay</label>
                                <div class="">
                                <input type="text" name="title" class="form-control" placeholder="Vay tiền trả lãi 30 ngày">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tạm giữ</label>
                                <input type="text" name="notes" class="form-control" placeholder="Để lại CMND và Giấy phép lái xe">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Ghi chú</label>
                                <input type="text" name="verify_notes" class="form-control" placeholder="Đã xác minh">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nguồn tiền từ CH</label>
                                <div class="">
                                <select name="branchID" class="form-control">
                                    <option value="-1" selected="selected">-------</option>
                                    <?php
                                        $userID = $this->user->ID;
                                        $dataBranch = $this->db->query("SELECT * FROM m_branch WHERE userID = $userID")->result();
                                        foreach($dataBranch as $items){
                                    ?>
                                    <option value="<?=$items->id;?>"><?=($items) ? $items->name : "";?></option>
                                    <?php } ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Số tiền vay</label>
                                <input type="text" name="loan_money" id="loan_money" class="form-control" placeholder="10.000.000">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Thời lượng <i class="fa fa-plus-circle addMoreTimer" style="cursor: pointer" title="Thêm"></i></label>
                                <select name="after_date" id="after_date" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Phí/ngày</label>
                                <input type="text" name="fee_money" id="fee_money" class="form-control" placeholder="400.000">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Thu về</label>
                                <input type="text" id="return_money" class="form-control" placeholder="12.000.000" disabled="">
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="btn-group">
                                <a class="btn btn-success btn-xs saveLoansDetails disabled"><i class="fa fa-plus-square"></i>Thực hiện</a>
                            </div>
                            <span class="showMessages_saveLoansDetails"></span>
                        </div>
                        <div class="col-md-2"><div class="showMoneyInPackage text-danger"></div></div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>