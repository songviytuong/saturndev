<?php
    $this->load->view('common/breadcrumb');
?>
<div class="row bottom-margin hidden">
    <div class="col-md-4">
        <div class="btn btn-warning btnToday" data-toggle="modal" data-target="#modalLoansToday"><i class="fa fa-calendar-o"></i><?php echo $this->lang->line('today'); ?></div>
    </div>
</div>
<div class="widget widget-blue" style="margin-bottom: 10px;">
    <div class="widget-title">
        <div class="widget-controls">
            <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
            <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
            <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
        </div>
        <h3><i class="fa fa-table"></i> <?php echo $this->lang->line('list_customer'); ?></h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive loadCustomerList">
            <i class="fa fa-spinner fa-spin"></i> <?php echo $this->lang->line('loading'); ?>...
        </div>
    </div>
</div>
<div class="widget widget-red widget-detail hidden">
    <div class="widget-title">
        <div class="widget-controls">
            <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
            <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
            <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
        </div>
        <h3><i class="fa fa-table"></i> <?php echo $this->lang->line('loans_details');?></h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive loadLoansDetails">
            <i class="fa fa-spinner fa-spin"></i> <?php echo $this->lang->line('loading'); ?>...
        </div>
    </div>
</div>