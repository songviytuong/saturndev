<!--Modal-->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-blue">
                <div class="widget-title">
                    <div class="widget-controls">
                        <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('languages_translate_boxes'); ?></h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body">
                        <form class="form-group has-iconed" id="validateForm" name="frmAddLang" id="frmAddLang" role="form">
                            <?php
                            $arr_Languages = $this->db->query("SELECT * FROM ttp_languages")->result();
                            foreach ($arr_Languages as $item) {
                                ?>
                                <div class="form-group">
                                    <div class="<?= $item->LangName; ?> move-top">
                                        <div class="input-group has-iconed">
                                            <input type="hidden" name="id[]" value="<?= $item->LangID; ?>"/>
                                            <input type="text" class="form-control move-top-input txtLanguage iconed-input" name="language[]" id="language" placeholder=""  minlength="2" required autocomplete="off"/>
                                            <label for="<?= $item->LangName; ?>" class="move-top-label lblLanguage"><?= $item->LangName; ?></label>
                                            <span class="input-group-addon"><span class="badge"><?= $item->LangID; ?></span></span>
                                        </div>

                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-inline">
                                <a onclick="insertLanguages(this);" class="btn btn-primary"><i class="fa fa-arrow-down"></i><?php echo $this->lang->line('btn_insert'); ?></a> 
                                <div class="form-group hidden">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadio" id="optionsRadios1" value="0" checked>
                                            <?php echo $this->lang->line('default'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadio" id="optionsRadios2" value="1">
                                            <?php echo $this->lang->line('shop'); ?>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadio" id="optionsRadios2" value="2">
                                            <?php echo $this->lang->line('modules'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="Keyword iconed-input move-top">
                                    <input type="text" class="form-control move-top-input txtKeyword" name="keyword" id="txtKeyword" required placeholder="" autocomplete="off"/>
                                    <label for="Keyword" class="move-top-label lblKeyword">Keyword</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Define Modal-->
<div class="modal fade" id="modalFormDefine" tabindex="-1" role="dialog" aria-labelledby="modalFormLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-blue">
                <div class="widget-title">
                    <div class="widget-controls">
                        <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3></h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    if($this->uri->segment(2) == "loans"){
?>
<div class="modal fade" id="modalUploadCMND" tabindex="-1" role="dialog" aria-labelledby="modalUploadCMNDLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-blue">
                <div class="widget-title">
                    <div class="widget-controls">
                        <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('upload_boxes'); ?></h3>
                </div>
                <form id="frmU" action="/cpanel/loans/doUploadCMND" method="post" enctype="multipart/form-data">
                    <div class="widget-content">
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group addOn">
                                        <input type="hidden" name="cID" id="cID" value=""/>
                                        <input type="file" name="userfile" id="choosefile" class="form-control">
                                        <span class="input-group-addon doUploadCMND" style="cursor: pointer"><i class="fa fa-upload" title="Upload CMND"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center dvPreview"><img src="public/authen/images/noimage.png" width="100px"/></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddLoans" tabindex="-1" role="dialog" aria-labelledby="modalAddLoansLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-blue">
                <div class="widget-title addReturn">
                    <div class="widget-controls">
                        <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle closeAddReturn"></i></a>
                    </div>
                    <h3><i class="fa fa-plus-circle"></i><?php echo $this->lang->line('addReturnMoney_boxes'); ?></h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body addReturnContent">
                        <form id="frmAddReturn">
                        <input type="hidden" name="dataid" id="dataid" value=""/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon"><?= date('d/m/Y H:i', time()) ?></span>
                                    <input type="text" name="return_money" id="return_money" data-money="" class="form-control" placeholder="100,000">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" name="return_notes" id="return_notes" class="form-control" placeholder="Ghi chú">
                            </div>
                            <div class="col-lg-4 hidden">
                                 <select name="verify_by" id="verify_by" class="form-control">
                                    <?php
                                        foreach($dataMembers as $items){
                                    ?>
                                    <option value="<?=$items->id;?>"><?=($items) ? $items->fullname : "";?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-2"><a class="btn btn-primary addReturnAction"><i class="fa fa-save"></i></a></div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalLoansDetails" tabindex="-1" role="dialog" aria-labelledby="modalLoansDetailsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-red">
                <div class="widget-title details">
                    <div class="widget-controls">
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-info-circle"></i></h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <img class="source" src="public/authen/images/user.png" alt="" width="100%" style="border:1px solid #ccc; padding: 3px;"/>
                            </div>
                            <div class="col-lg-9">
                                <table class="table table-bordered" width="100%">
                                <tr>
                                    <td width="auto" style="word-wrap: initial;">Nội dung:</td>
                                    <td class="content"></td>
                                </tr>
                                <tr>
                                    <td>Tạm giữ:</td>
                                    <td class="notes"></td>
                                </tr>
                                <tr>
                                    <td>Xác nhận:</td>
                                    <td class="verify_by"></td>
                                </tr>
                                <tr>
                                    <td>Tiền từ CH:</td>
                                    <td class="branchID"></td>
                                </tr>
                                <tr>
                                    <td>Ghi chú:</td>
                                    <td class="verify_notes"></td>
                                </tr>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalHistory" tabindex="-1" role="dialog" aria-labelledby="modalHistoryLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-red">
                <div class="widget-title History">
                    <div class="widget-controls">
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-info-circle"></i>LỊCH SỬ ĐÓNG TIỀN</h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddMember" tabindex="-1" role="dialog" aria-labelledby="modalAddMemberLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-blue">
                <div class="widget-title addMemberHeader">
                    <div class="widget-controls">
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-info-circle"></i></h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body addMemberContent">
                        <form id="frmAddMember">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('fullname'); ?> (Đại diện)</label>
                                    <input type="text" name="fullname" class="form-control" placeholder="<?php echo $this->lang->line('ph_fullname'); ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('nickname'); ?></label>
                                    <input type="text" name="nickname" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('phone'); ?></label>
                                    <input type="number" min="0" name="phone" class="form-control" placeholder="<?php echo $this->lang->line('ph_phone'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('address'); ?></label>
                                    <input type="text" name="address" class="form-control" placeholder="<?php echo $this->lang->line('ph_address'); ?>">
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('branch_name'); ?></label>
                                    <input type="text" name="branch_name" class="form-control" placeholder="<?php echo $this->lang->line('ph_branch_name'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('area'); ?></label>
                                    <select name="cityID" id="cityID" class="form-control">
                                        <?php
                                            $this->load->model("city_model","city");
                                            $ListCity = $this->city->getAllCity();
                                            foreach($ListCity as $item){
                                        ?>
                                        <option value="<?=$item->id;?>" <?=($item->id == 29)? 'selected' : ''?>><?=$item->name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('branch'); ?></label>
                                    <select name="branchID" id="branchID" class="form-control">
                                        <?php
                                            $this->load->model("loans_model","loans");
                                            $ListBranch = $this->loans->getBranchDefaultByUserID($this->user->ID);
                                            foreach($ListBranch as $item){
                                        ?>
                                            <option value="<?=$item->id;?>"><?=$item->name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group">
                            <a class="btn btn-danger btn-sm addMemberAction"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_add'); ?></a>
                        </div>
                        <span class="showMessages_saveNewMember"></span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddMoney" tabindex="-1" role="dialog" aria-labelledby="modalAddMoneyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-blue">
                <div class="widget-title addMoneyHeader">
                    <div class="widget-controls">
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-info-circle"></i></h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body addMoneyContent">
                        <form id="frmAddMoney">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Hành động</label>
                                    <select name="add_type" id="type" class="form-control">
                                        <option value="0">Thu (vào)</option>
                                        <option value="1">Chi (ra)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Số tiền</label>
                                    <input type="text" name="add_money" id="add_money" class="form-control" placeholder="Nhập tiền">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Từ két</label>
                                    <select name="add_package" id="package" class="form-control">
                                        <?php
                                            $this->load->model("loans_model","loans");
                                            $ListBranch = $this->loans->getListBranchByUserID($this->user->ID);
                                            foreach($ListBranch as $item){
                                        ?>
                                            <option value="<?=$item->id;?>"><?=$item->name;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Ghi chú</label>
                                    <input type="text" name="add_source" class="form-control" placeholder="<?php echo $this->lang->line('ph_address'); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="btn-group">
                            <a class="btn btn-danger btn-xs addMoneyAction"><i class="fa fa-save"></i><?php echo $this->lang->line('btn_add'); ?></a>
                        </div>
                        <span class="showMessages_saveNewMoney"></span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEditMoney" tabindex="-1" role="dialog" aria-labelledby="modalEditMoneyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="widget widget-blue">
                <div class="widget-title editMoneyHeader">
                    <div class="widget-controls">
                        <a class="widget-control" data-dismiss="modal"><i class="fa fa-times-circle"></i></a>
                    </div>
                    <h3><i class="fa fa-info-circle"></i></h3>
                </div>
                <div class="widget-content">
                    <div class="modal-body editMoneyContent">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>
