<?php
    $_token = substr(sha1($this->session->userdata('_token')),TOKENF,TOKENT);
    $_token = "/?_token=".$_token;
?>
<ol class="breadcrumb">
    <li><a href="<?=$this->uri->segment(1).$_token;?>"><?=$this->uri->segment(1);?></a></li>
    <?php if($this->uri->segment(2)) { ?>
    <li><a href="<?=$this->uri->segment(1)."/".$this->uri->segment(2).$_token;?>"><?=$this->lang->line($this->uri->segment(2));?></a></li>
    <?php } ?>
    <li class="active"><?=($this->lang->line($this->uri->segment(3))) ? $this->lang->line($this->uri->segment(3)) : $this->uri->segment(3);?></li>
</ol>
