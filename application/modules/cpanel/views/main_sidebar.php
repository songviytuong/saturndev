<div class="sidebar-wrapper">
    <ul>
        <?php
        $OnMenuLeftArr = $this->db->query("SELECT OnMenuLeft FROM ttp_user WHERE ID = " . $this->user->ID)->row();
        $OnMenuLeftArr = ($OnMenuLeftArr) ? json_decode($OnMenuLeftArr->OnMenuLeft) : array();
        foreach ($sitebar as $nav) {
            if (in_array($nav->ModID, $OnMenuLeftArr)) {
                ?>
                <li<?= ($nav_main_active == $nav->ModClassActive) ? " class='current'" : ""; ?>>
                    <a<?= ($nav_main_active == $nav->ModClassActive) ? " class='current'" : ""; ?> href="<?= base_url() . ADMINROOT . $nav->ModLink . "/?_token=" . substr(md5($this->session->userdata('_token')), TOKENF, TOKENT); ?>" data-toggle="tooltip" data-placement="right" title="<?= $this->lang->line($nav->ModAlias); ?>" data-original-title="<?= $this->lang->line($nav->ModAlias); ?>">
                        <i class="<?= $nav->ModClass; ?>"></i>
                    </a>
                </li>
            <?php }
        } ?>
    </ul>
</div>