<?php
    $this->load->view('common/breadcrumb');
?>
<div class="row bottom-margin">
    <div class="col-md-4">
        <div class="btn btn-primary" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus"></i><?php echo $this->lang->line('btn_add'); ?></div>
    </div>
</div>
<div class="widget widget-blue">
    <div class="widget-title">
        <div class="widget-controls">
            <a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
            <a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
            <a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
            <a href="#" class="widget-control widget-control-remove" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove"><i class="fa fa-times-circle"></i></a>
        </div>
        <h3><i class="fa fa-table"></i> DataTable</h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive loadLanguages">
            <?php echo $this->lang->line('loading'); ?>...
        </div>
    </div>
</div>

