<?php
#LOAD LANGUAGE FROM DATABASE
$this->load->model("languages_model","_lang");
$lang_constants = unserialize(ACTIVELANG);
$dataLanguages = $this->_lang->getLanguagesIn($lang_constants["ids"]);
$langsite = $this->config->item('language');
$src = "<i class='flag-icon flag-icon-vn'></i>";
foreach($dataLanguages as $active){
    if($active->LangName == $this->cur_lang){
        $src = "<i class='flag-icon flag-icon-".$active->LangIcon."'></i>";
    }
}
#BEGIN: END LOAD LANGUAGE
$_token = substr(sha1($this->session->userdata('_token')),TOKENF,TOKENT);
$_token = "/?_token=".$_token;
?>
<div class="header-links hidden-xs">
    <?php
        $this->load->model('Cpanel_model','cpanel');
        $result = $this->cpanel->getListMenuTop();
        foreach($result as $row){
            $OnMenuTopArr = $this->db->query("SELECT OnMenuTop FROM ttp_user WHERE ID = " . $this->user->ID)->row();
            $OnMenuTopArr = ($OnMenuTopArr) ? json_decode($OnMenuTopArr->OnMenuTop) : array();
            if (in_array($row->MenuID, $OnMenuTopArr)) {
                switch ($row->MenuName){
                    case 'Search':
                        ?>
                        <div class="top-search-w pull-right">
                            <input type="text" class="top-search" placeholder="<?php echo $this->lang->line('search'); ?>"/>
                        </div>
                    <?php
                        break;
                    case 'Language':
                        ?>
                        <div class="dropdown hidden-sm hidden-xs">
                            <a href="#" data-toggle="dropdown" class="header-link"><?=$src;?> <?php echo $this->lang->line('language_title'); ?></a>
                            <ul class="dropdown-menu dropdown-inbar">
                                <?php
                                foreach($dataLanguages as $item){
                                    $name = $item->LangName;
                                    $url = $this->uri->uri_string();
                                ?>
                                <li<?=($this->cur_lang == $name) ? ' class="active"' : ''?>><a href="<?php echo base_url() . ADMINROOT. "/languages?lang=$name&return=$url" ?>"><i class="fa fa-angle-right"></i> <?=$name;?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php
                        break;
                    case 'Profiles':
                        ?>
                        <div class="dropdown">
                            <a href="#" class="header-link clearfix" data-toggle="dropdown">
                                <div class="avatar">
                                    <img src="<?=($this->user->Thumb) ? $this->user->Thumb :'public/authen/images/user.png'?>" />
                                </div>
                                <div class="user-name-w">
                                    <?= $this->user->FirstName . ' ' . $this->user->LastName; ?>  <i class="fa fa-caret-down"></i>
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-inbar dropdown-wide">
                                <?php
                                $this->load->model('Cpanel_model','cpanel');
                                $dataChild = $this->cpanel->getListMenuTop($row->MenuID,'ASC');
                                foreach($dataChild as $child){
                                     $alias = $child->MenuAlias;
                                     if($alias == 'mnu_logout'){
                                        $link = base_url() . ADMINPATH . $child->MenuLink . $_token;
                                     }else{
                                        $link = base_url() . ADMINROOT . $child->MenuLink . $_token;
                                     }
                                ?>
                                <li><a href="<?=$link;?>"><span class="label label-warning hidden">2</span><i class="<?=$child->MenuClassIcon;?>"></i> <?php echo $this->lang->line($alias); ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php
                        break;
                    case 'Stores':
                        ?>
                        <div class="dropdown hidden-sm hidden-xs">
                            <a href="#" data-toggle="dropdown" class="header-link"><i class="fa fa-briefcase"></i><?php echo $this->lang->line('store_settings'); ?></a>
                            <ul class="dropdown-menu dropdown-inbar">
                                <li><a href="<?php echo base_url() . ADMINROOT . "/stores" ?>"><i class="fa fa-shopping-cart"></i><?php echo $this->lang->line('site_settings'); ?></a></li>
                            </ul>
                        </div>
                    <?php
                        break;
                    case 'Alerts':
                        ?>
                        <div class="dropdown hidden-sm hidden-xs">
                            <a href="#" data-toggle="dropdown" class="header-link"><i class="fa fa-bolt"></i>User Alerts <span class="badge alert-animated">5</span></a>

                            <ul class="dropdown-menu dropdown-inbar dropdown-wide">
                                <li><a href="#"><span class="label label-warning">1 min</span> <i class="fa fa-bell"></i> Menu 1</a></li>
                                <li><a href="#"><span class="label label-warning">4 min</span> <i class="fa fa-fire"></i> Menu 1</a></li>
                                <li><a href="#"><span class="label label-warning">12 min</span> <i class="fa fa-flag-o"></i> Menu 1</a></li>
                                <li><a href="#"><span class="label label-warning">15 min</span> <i class="fa fa-smile-o"></i> Menu 1</a></li>
                            </ul>
                        </div>
                    <?php
                        break;
                }
            } /* End MenuTop */
        }
    ?>
</div>
<?php
#BEGIN: Get Domain where UserID
$this->load->model("store_model","store");
$info = $this->store->getStoreInfo($this->user->ID);
$info = ($info) ? $info->Domain : "";
$LinkToStore = "http://".$info;
?>
<a class="current logo hidden-xs" href="<?php echo $LinkToStore; ?>" target="_blank"><i class="fa fa-rocket"></i></a>
<a class="menu-toggler" href="#"><i class="fa fa-bars"></i></a>
<h1>Dashboard</h1>