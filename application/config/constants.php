<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); 
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); 
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('HACKER',    'saturn.local');
define('SITENAME',  ' .:: Cpanel Administrator ::.');
define('ADMINPATH', 'authen');
define('ADMINROOT', 'cpanel');
define('REGISTER',  'false');
define('PUBLISHED', 1); //Kích hoạt người dùng |0

define('MYKEY',     'LP');
define('TOKENF',    5);
define('TOKENT',    10);
define('DATEFORMAT','d/m/Y - H:i');

define('PAGE_EXTENSION',    '.html');
define('DEFAULT_LANGUAGE_ID',    2); // Vietnamese

define('CMS_VERSION', "2.0.b1");
define('CMS_VERSION_NAME', "Lee-Peace");
define('CMS_SCHEMA_VERSION', "220");

define('ACTIVELANG', serialize(array(
    'ids'        =>  array("1","2"),
    ))
);

define('PUBLIC_MEDIA_SRC',	'public/media/');
define('DYNAMIC_TEMPLATE_SRC',	'public/templates/');
define('RETURN_IP',	'http://www.idev.com.vn');

define('GHNDEV', serialize(array(
    'apiUrl'        =>  'https://testapipds.ghn.vn:9999/external/b2c/',
    'clientID'      =>  53813,
    'password'      =>  '1234567890',
    'apiKey'        =>  'dSLjeJstcjwcbcLe',
    'apiSecretKey'  =>  'F7BB3C08E9BCA7E8D880B3D9D57FB4DF',
    ))
);

define('GHNPUB', serialize(array(
    'apiUrl'        =>  'https://apipds.ghn.vn/external/b2c/',
    'clientID'      =>  42597,
    'password'      =>  'bbxpjXRKTTnSNsuqN',
    'apiKey'        =>  'dSLjeJstcjwcbcLe',
    'apiSecretKey'  =>  'F7BB3C08E9BCA7E8D880B3D9D57FB4DF',
    ))
);

define('GHNHNA', serialize(array(
    'apiUrl'        =>  'https://apipds.ghn.vn/External/B2C',
    'clientID'      =>  26684,
    'password'      =>  'a5p6zawrPyqWRGpsS',
    'apiKey'        =>  'caquEdYxJU15C2N0',
    'apiSecretKey'  =>  'E731CA7CEA7F0585C35B185DCD77ACEB',
    ))
);

/* End of file constants.php */
/* Location: ./application/config/constants.php */