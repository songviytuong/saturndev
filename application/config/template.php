<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// section default
$template['default']['template'] = 'default/default_template';
$template['default']['regions'] = array(
    'top',
    'menu',
    'left',
    'content',
    'right',
    'bottom'
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

/*Administrator Template*/
$template['cpanel']['template'] = 'cpanel/template_cpanel';
$template['cpanel']['regions'] = array(
    'header',
    'footer',
    'sitebar',
    'subsitebar',
    'content'
);
$template['cpanel']['parser'] = 'parser';
$template['cpanel']['parser_method'] = 'parse';
$template['cpanel']['parse_template'] = FALSE;

/*Authenticate Template*/
$template['authen']['template'] = 'authen/template_authen';
$template['authen']['regions'] = array(
    'content'
);
$template['authen']['parser'] = 'parser';
$template['authen']['parser_method'] = 'parse';
$template['authen']['parse_template'] = FALSE;