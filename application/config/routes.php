<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$route[ADMINPATH] = "authentication";
$route[ADMINPATH . '/check-login'] = "authentication/login_now";
$route[ADMINPATH . '/login'] = "authentication/login";
$route[ADMINPATH . '/register'] = "authentication/register";
$route[ADMINPATH . '/login/(:any)'] = "authentication/login";
$route[ADMINPATH . '/logout'] = "authentication/logout";
$route[ADMINPATH . '/([a-zA-Z_-]+)/(:any)'] = "$1/admin/$2";
$route[ADMINPATH . '/([a-zA-Z_-]+)'] = "$1/admin/$1";

$domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '' ;
$subdomain = explode('.',$domain);
if(count($subdomain)>= 3 || $domain != HACKER){
    $subdomain = isset($subdomain[0]) ? $subdomain[0] : '' ;
    $route['default_controller'] = "store";
    $route['404_override'] = 'store/my404';
    $route['([a-zA-Z_-]+)-(:num)'.PAGE_EXTENSION] = "store/detail/$1/$2";
    $route['([a-zA-Z_-]+)'.PAGE_EXTENSION] = "store/index/$1";
    $route['(:any)'] = "store/$1";
}
else{
    $route['default_controller'] = "home";
    $route['cpanel/decline/(:any)'] = "home/decline/$1";
    $route['404_override'] = 'home/my404';
    
    $route['api'] = "api";
    
    $route['cpanel'] = "cpanel";
    $route['cpanel/(:any)'] = "cpanel/$1";
    
//    $route['language'] = "cpanel/language";
//    $route['language/(:any)'] = "cpanel/language/$1";
    
    //$route['travel'] = "cpanel/travel";
    //$route['travel/(:any)'] = "cpanel/travel/$1";
    
    $route['(:any)'] = "home/$1";
}
/* End of file routes.php */
/* Location: ./application/config/routes.php */