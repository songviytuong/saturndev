<?php
class Ipaccept extends CI_Model {
    const _tablename    = 'ttp_ip';
    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getIPs($type = 0) {
        $this->db->select('ip');
        $this->db->where('type', $type);
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
}