<?php
class conf_define extends CI_Model {

    const _tablename = 'ttp_define';
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_config_define($group, $type, $order = null, $by = null, $value = null) {
        if ($order == null) { $order = "name"; }
        if ($by == null) { $by = "asc"; }
        $this->db->select('code, name, description');
        if ($value) {
            $this->db->where('value', $value);
        }
        $this->db->where('group', $group);
        $this->db->where('type', $type);
        $this->db->order_by($order, $by);
        $result = $this->db->get(self::_tablename)->result();
        return $result;
    }
    
    function insert_config_define($data){
        $this->db->insert(self::_tablename,$data);
        return $this->db->insert_id();
    }
    
    function update_config_define($id,$data){
        $this->db->where("id",$id);
        return $this->db->update(self::_tablename,$data);
    }
}