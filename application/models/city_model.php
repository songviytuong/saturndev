<?php
class City_model extends CI_Model {

    const _tablename_city      = 'tbl_city';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function getAllCity()
    {
        $this->db->select('*');
        $result = $this->db->get(self::_tablename_city)->result();
        return $result;
    }
}